import os, sys, re

import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwText/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall(r"\d+\.\d+\.\d+", version_line)[0]

def download_nltk_stopwords():
    print("About to download nltk stopwords...")
    import nltk
    nltk.download("stopwords")
    nltk.download("punkt")

            
class custom_install(install):
    def run(self):
        print("This is a custom installation of FwText")
        install.run(self)
        download_nltk_stopwords()

setuptools.setup(
    name="FwText",
    version=__version__,
    url="https://bitbucket.org/FindWatt/fwtext",

    author="FindWatt",

    description="Text utilities",
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    package_data={'': ["*.pyx","*.txt"]},
    py_modules=['FwText'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
        "nltk",
        "numpy"
    ],
    cmdclass={'install': custom_install}
)


