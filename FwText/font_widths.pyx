__all__ = ["get_text_pixel_width"]

cdef dict e_ROBOT_CHAR_WIDTHS = {
    "0": 10, "1": 10, "2": 10, "3": 10, "4": 10,
    "5": 10, "6": 10, "7": 10, "8": 10, "9": 10,
    "'": 4, "-": 6, '"': 7, "#": 10, "$": 10, "&": 12,
    "(": 6, ")": 6, ",": 5, ".": 5, ":": 5, ";": 5, " ": 4,
    "?": 10, "\\": 5, "|": 5, "®": 13, "™": 18,
    "A": 12, "a": 10, "B": 12, "b": 10, "C": 13, "c": 9,
    "D": 13, "d": 10, "E": 12, "e": 10, "F": 11, "f": 5,
    "G": 14, "g": 10, "H": 13, "h": 10, "I": 5, "i": 4,
    "J": 9, "j": 4, "K": 12, "k": 9, "L": 10, "l": 4,
    "M": 15, "m": 15, "N": 13, "n": 10, "O": 14, "o": 10,
    "P": 12, "p": 10, "Q": 14, "q": 10, "R": 13, "r": 6,
    "S": 12, "s": 9, "T": 11, "t": 5, "U": 13, "u": 10,
    "V": 12, "v": 9, "W": 17, "w": 13, "X": 12, "x": 9,
    "Y": 12, "y": 9, "Z": 11, "z": 9,}
    
    
cpdef int get_text_pixel_width(str text, dict e_font_widths=e_ROBOT_CHAR_WIDTHS, int i_default_width=10):
    ''' estimate the pixel length of a string using a font width dictionary
        Ignores kerning, which means it's a conservative estimate when
        it comes to comparing to a pixel limit.
        Arguments:
            text: {str} of the text to estimate
            e_font_widths: {dict} in the format of {char_str: char_pixel_width...}
            i_default_width: {int} the default width for any character missing from e_font_widths
        Returns:
            {int} of pixel width estimate
    '''
    cdef str char
    return sum([
        e_font_widths.get(char, i_default_width)
        for char in text])
