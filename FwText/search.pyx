﻿from __future__ import absolute_import
import re
from operator import itemgetter
import itertools
from .cleaning import *
from .tokenize import *
from .plural import *

cdef e_ALTERNATE_SPANS = {
    'and': ['&', "'n",],
    '&': ['and', 'AND', "'n", "'N",],
    'AND': ['&', "'N",],
    'with': ['w/', 'w'],
    'w/': ['with', 'w'],
    'w': ['with', 'w/'],
    'without': ['w/o', 'wo'],
    'w/o': ['without', 'wo'],
    'wo': ['without', 'w/o'],
    'WITH': ['W/', 'W'],
    'W/': ['WITH', 'W'],
    'W': ['WITH', 'W/'],
    'WITHOUT': ['W/O', 'WO'],
    'W/O': ['WITHOUT', 'WO'],
    'WO': ['WITHOUT', 'W/O'],
}

c_WORD_SEP_CHARS = set("'`~-_\"/\:@+")   #"#$%^&*()=,.<>?:|{}[]")


cpdef list word_indexes(list a_text, str s_text, bint b_case_insensitive=False):
    ''' Turn word list into a list of tuples of start-stop character indexes in a string.
        This assumes the word list is in the exact same order as the string,
        but that punctuation may have been added or removed.
        Arguments:
            a_text: {list} tokenized words
            s_text: {str} text that words were tokenized from
            b_case_insensitive: {bool} whether to do case insensitive alignment between a_text and s_text
        Returns:
            {list} of (word_first_char_pos, word_last_char_pos)
    '''
    if not s_text or not a_text: return []
    if b_case_insensitive:
        s_text = s_text.lower()
        a_text = [word.lower() for word in a_text]
    
    cdef int i_pos = 0
    cdef list a_pos = []
    cdef str s_word
    cdef int i_start, i_end
    
    for s_word in a_text:
        i_start = s_text.find(s_word, i_pos)
        if i_start == -1:
            a_pos.append(None)
        else:
            i_end = i_start + len(s_word)
            a_pos.append((i_start, i_end))
            i_pos = i_end
    return a_pos




cpdef bint pos_contained_in_spans(tuple t_pos, list a_spans):
    ''' check each span in the list to see if the pos span
        is completely contained inside of any one of them
        Arguments:
            t_pos: {tuple} of (start_pos, stop_pos)
            a_spans: {list} of [(start_pos1, stop_pos1), (start_pos2, stop_pos2)...]
        Returns:
            {bool} if t_pos is completely inside one of the positions in a_spans
    '''
    cdef tuple t_span
    if not t_pos or not a_spans: return False
    for t_span in a_spans:
        if t_pos[0] >= t_span[0] and t_pos[1] <= t_span[1]:
            return True
    return False


cpdef bint pos_overlaps_in_spans(tuple t_pos, list a_spans):
    ''' check each span in the list to see
        if the pos span overlaps it
        Arguments:
            t_pos: {tuple} of (start_pos, stop_pos)
            a_spans: {list} of [(start_pos1, stop_pos1), (start_pos2, stop_pos2)...]
        Returns:
            {bool} if t_pos is overlapped with at least one of the positions in a_spans
    '''
    cdef tuple t_span
    if not t_pos or not a_spans: return False
    for t_span in a_spans:
        if t_pos[0] >= t_span[0] and t_pos[0] < t_span[1]:
            return True
        elif t_pos[1] > t_span[0] and t_pos[1] <= t_span[1]:
            return True
    return False


cpdef list merge_overlapped_spans(list a_spans):
    ''' take a list of spans/positions, sort them in order, then merge spans with any overlap
        return list of unique and distinct spans
        Arguments:
            a_spans: {list} of [(start_pos1, stop_pos1), (start_pos2, stop_pos2)...]
        Returns:
            {list} of non-overlapped [(start_pos1, stop_pos1), (start_pos2, stop_pos2)...]
    '''
    cdef list a_distinct = []
    cdef tuple t_span
    if not a_spans: return a_spans
    for t_span in sorted(a_spans, key=itemgetter(0)):
        if not a_distinct:
            a_distinct.append(t_span)
        elif a_distinct[-1][1] <= t_span[0]:
            a_distinct.append(t_span)
        elif t_span[1] > a_distinct[-1][1]:
            a_distinct[-1] = (a_distinct[-1][0], t_span[1])
    return a_distinct


cpdef list merge_adjacent_spans(
    str s_text, list a_spans, int i_max_separation=-1, set c_allowed_chars=None, set c_not_allowed_chars=None):
    ''' Merge the character spans of adjacent words that are separate only by punctuation
        Arguments:
            s_text: {str} the text that the character spans apply to
            a_spans: {list} of spans as tuples of (i_start_char, i_end_char)
            i_max_separation: {int} the max # of any characters between the two spans. -1 is unlimited
            c_allowed_chars: optional {set} of the allowed characters between spans
            c_not_allowed_chars: optional {set} of not allowed characters between spans. overrides c_allowed_chars
        Returns:
            {list} of merged spans as tuples of (i_start_char, i_end_char)
    '''
    cdef int i_span
    cdef tuple t_span
    
    if not a_spans or len(a_spans) <= 1: return a_spans
    
    if c_allowed_chars is None:
        c_allowed_chars = c_WORD_SEP_CHARS
    
    # TODO: should this work on a copy of the list instead of sorting the passed in list?
    a_spans.sort(key=itemgetter(0))
    cdef list a_merged = [a_spans[0]]
    cdef int i_end = a_merged[-1][1]
    
    for i_span, t_span in enumerate(a_spans):
        if i_span == 0:
            continue
        elif t_span[0] <= i_end:
            i_end = t_span[1]
            a_merged[-1] = (a_merged[-1][0], i_end)
            
        elif i_max_separation < 0 or (i_max_separation >= 0 and t_span[0] - i_end <= i_max_separation):
            s_between = s_text[i_end: t_span[0]]
            if all([s_char in c_allowed_chars for s_char in s_between]):
                if not c_not_allowed_chars or not any([s_char in c_not_allowed_chars for s_char in s_between]):
                    i_end = t_span[1]
                    a_merged[-1] = (a_merged[-1][0], i_end)
                else:
                    i_end = t_span[1]
                    a_merged.append(t_span)
            else:
                i_end = t_span[1]
                a_merged.append(t_span)
        else:
            i_end = t_span[1]
            a_merged.append(t_span)
    return a_merged


cpdef word_in_text(str s_text, str s_word, position_function=None,
                   bint b_try_plural=True, bint b_try_singular=True):
    ''' Look for word in text, checking exact, plural, singular, and stem.
        If a custom function is passed in for position_function, it needs to
        return tuple of start and end position of word in s_text when called
        with (s_text, s_word)
        word_instr is exact word match
        find_word_sequence will match between words parts and whole word in text, but not vice versa
        find_char_sequence will fuzzy match in both directions
    '''
    cdef tuple t_pos
    cdef str s_plural, s_singular
    
    if position_function is None:
        f_position = word_instr
    elif position_function == 'exact':
        f_position = word_instr
    elif position_function == 'word':
        f_position = find_word_sequence
    elif position_function == 'character':
        f_position = find_char_sequence
    else:
        f_position = position_function
    
    t_pos = f_position(s_text, s_word)
    if t_pos: return t_pos
    
    if b_try_plural:
        s_plural = pluralize(s_word)
        t_pos = f_position(s_text, s_plural)
        if t_pos: return t_pos
        
    if b_try_singular:
        s_singular = singularize(s_word)
        t_pos = f_position(s_text, s_singular)
        if t_pos: return t_pos


cpdef tuple word_instr(str s_text, str s_search):
    ''' Check for word/phrase in text, requiring word boundaries before and after.
        Example: "cat" is word in "black cat", but not word in "category" '''
    o_match = re.search(r"(^|\b|\W){}(\W|\b|$)".format(re.escape(s_search)), s_text)
    if o_match:
        return o_match.span()
    else:
        return None

re_default_match_chars = re.compile("[A-Z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚()/+]+", flags=re.I)
re_default_punct_ignore = re.compile("[^ '-]+", flags=re.I)


cpdef tuple find_char_sequence(str s_text, str s_substring, int i_max_insert_len=2,
                               str s_insert_chars=" _:;+/()-", int i_start=0):
    ''' Another way of finding sequences of characters in another string,
        disregarding certain punctuation
        characters that may appear between the letters.
        Return the start and end positions of the first match if it is found. '''
    cdef set c_insert_chars = set(list(s_insert_chars))
    cdef list a_sub_chars = [
        "({})?".format(re.escape(s_char))
        if s_char in c_insert_chars
        else re.escape(s_char)
        for s_char in s_substring
    ]
    cdef str s_sub_regex = str("[%s]{0,%i}" % (s_insert_chars, i_max_insert_len)).join(a_sub_chars)
    o_match = re.search(s_sub_regex, s_text[i_start:], flags=re.I)
    if o_match:
        t_pos = o_match.span()
        return (t_pos[0] + i_start, t_pos[1] + i_start)
    else:
        return None
    

cpdef tuple find_word_sequence(str s_text, str s_substring,
                               bint b_try_plural_singular=False, str s_match_regex="[^ '-/]+",
                               int i_start_pos=0, bint b_word_boundaries=True):
    ''' Look for substring in text, ignoring spaces and other punctuation.
        Can optionally look for plural and singular variants of each word.
        Return a tuple with the start and end positions of the substring within the text. '''
    if s_match_regex == "[A-Z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚()/+]+":
        re_match = re_default_match_chars
    elif s_match_regex == "[^ '-]+":
        re_match = re_default_punct_ignore
    else:
        re_match = re.compile(s_match_regex, flags=re.I)
    
    # start by dividing sub into the alphanumeric pieces to match on
    cdef list a_alpha = re_match.findall(s_substring.lower())
    if not a_alpha: return None
    
    s_text = s_text.lower()
    cdef int i_pos, i_len, i_total_len
    cdef tuple t_sub_pos
    cdef str s_plural, s_singular
    i_total_len = len(s_text)
    
    while i_start_pos < i_total_len and i_start_pos != -1:
        i_pos = s_text.find(a_alpha[0], i_start_pos)  # find the position of the first element
        
        if i_pos != -1:
            i_len = len(a_alpha[0])
            i_start_pos = i_pos
            
        elif b_try_plural_singular:
            s_plural = pluralize(a_alpha[0])
            if not s_plural: return None
            i_pos = s_text.find(s_plural, i_start_pos)
            if i_pos != -1:
                i_len = len(s_plural)
                i_start_pos = i_pos
            else:
                s_singular = (a_alpha[0])
                if not s_singular: return None
                i_pos = s_text.find(s_singular, i_start_pos)
                if i_pos != -1:
                    i_len = len(s_singular)
                    i_start_pos = i_pos
                else:
                    return None  # if the first element isn't found, then the whole substring can't be found
        else:
            return None  # if the first element isn't found, then the whole substring can't be found
        
        if not b_word_boundaries or i_start_pos == 0 or not re_ALPHANUM.search(s_text[i_start_pos - 1]):
            if len(a_alpha) == 1:
                if not b_word_boundaries or i_start_pos + i_len == len(s_text) or \
                   not re_ALPHANUM.search(s_text[i_start_pos + i_len]):
                    # if first element is found and there's only 1, then return its start and end
                    return (i_start_pos, i_start_pos + i_len)
            
            # search for the rest of the substring pieces in order in the string
            t_sub_pos = _find_other_pieces(s_text, a_alpha, i_start_pos, i_len, re_match, b_try_plural_singular)

            if t_sub_pos:
                if not b_word_boundaries or t_sub_pos[1] == len(s_text) or \
                   not re_ALPHANUM.search(s_text[t_sub_pos[1]]):
                    return t_sub_pos  # if they were all found
        
        i_start_pos += 1  # else, increment the start pos and try again
    return None


cpdef bint word_in_set(set c_words, str word, i_partial_minimum=-1):
    ''' check if a word is in the set in any of its standard variant forms
        Arguments:
            c_words: {set} of words to look in
            word: {str} word to look for
            i_partial_minimum: {int} the mimimum len of the input word
                               to consider for a start/end partial match
                               -1 (default) means don't allow any partial matches
    '''
    cdef str s_word
    cdef int i_len
    
    if not c_words: return False
    if word in c_words: return True
    if word[-1:] == "'":
        word = word[:-1]
    elif word[-2:] == "'s":
        if "{}s".format(word[:-2]) in c_words: return True
        if "{}".format(word[:-2]) in c_words: return True
        word = "{}s".format(word[:-2])
    else:
        if "{}'".format(word) in c_words: return True
        if "{}'s".format(word) in c_words: return True
        if "{}s'".format(word) in c_words: return True
    if pluralize(word) in c_words: return True
    if singularize(word) in c_words: return True
    
    if i_partial_minimum != -1 and len(word) >= i_partial_minimum:
        i_len = len(word)
        for s_word in c_words:
            if len(s_word) > i_len:
                if s_word[:i_len] == word or s_word[-i_len:] == word:
                    return True
    
    return False


cpdef tuple _find_other_pieces(str s_string, list a_pieces, int i_start,
                               int i_first_len, re_match, bint b_try_plural_singular=False):
    ''' Find the remainder of the words in the sequence to search for starting from the end
        of the matched first word. Can optionally try plural and singular variants of each word. '''
    cdef int i_pos, i_end, i_len
    cdef str s_piece, s_between, s_plural, s_singular
    
    i_end = i_start + i_first_len
    for s_piece in a_pieces[1:]:
        i_pos = s_string.find(s_piece, i_end)
        
        if i_pos != -1:
            i_len = len(s_piece)
        elif b_try_plural_singular:
            s_plural = pluralize(s_piece)
            i_pos = s_string.find(s_plural, i_end)
            if i_pos != -1:
                i_len = len(s_plural)
            else:
                s_singular = (s_piece)
                i_pos = s_string.find(s_singular, i_end)
                if i_pos != -1:
                    i_len = len(s_singular)
                else:
                    return None
        else:
            return None
        
        s_between = s_string[i_end:i_pos]
        if re_match.search(s_between): return None
        i_end = i_pos + i_len
    return (i_start, i_end)


cpdef list get_token_spans(str text, list tokens, int i_max_missing=10, bint b_case_insensitive=False,
                           int i_start=0, b_accent_insensitive=False):
    ''' @description: Get the spans for the tokens as characters within the text
                      Allowing for some punctuation that has been removed from the tokens
        @arg text: {str} The original unsplit text
        @arg tokens: {list} The tokenized form of the text
        @arg i_max_missing: {int} The maximum number of characters to possibly skip over in finding next token
        @arg b_case_insensitive: {bint} Optionally ignore case
    '''
    cdef int i_search, i_end, i_begin, i_alt_len
    cdef list a_alts
    cdef list a_spans = []
    cdef str tok, s_alt
    cdef bint b_alt_found
    
    i_search, i_end = i_start, len(text)
    
    if b_case_insensitive:
        text = text.lower()
        tokens = [tok.lower() for tok in tokens]
    
    if b_accent_insensitive:
        text = strip_unicode_accents(text, b_maintain_length=True)
        tokens = [strip_unicode_accents(tok, b_maintain_length=True) for tok in tokens]
        #print('b_accent_insensitive, "{}"'.format(text), tokens)
        
    i_len = 0
    for tok in tokens:
        i_prior_len = i_len
        i_len = len(tok)
        
        i_begin = text.find(tok, i_search, i_search + i_len + i_max_missing + i_prior_len)
        if i_begin != -1:
            a_spans.append((i_begin, i_begin + i_len))
            i_search = i_begin + i_len
            i_len = 0
            continue
            
        a_alts = e_ALTERNATE_SPANS.get(tok)
        if a_alts:
            b_alt_found = False
            for s_alt in a_alts:
                i_alt_len = len(s_alt)
                i_begin = text.find(s_alt, i_search, i_search + i_alt_len + i_max_missing + i_prior_len)
                if i_begin != -1:
                    a_spans.append((i_begin, i_begin + i_alt_len))
                    i_search = i_begin + i_alt_len
                    b_alt_found = True
                    break
            
            if b_alt_found:
                i_len = 0
                continue
        
        a_spans.append(None)
    return a_spans


cpdef list get_consecutive_spans(
    str text, list token_lists,
    int i_max_missing=10, bint b_case_insensitive=False, int i_start=0):
    ''' Get the spans of a list of consecutive tokenized texts against the text they appear in. '''
    cdef list a_span_lists = []
    cdef list a_tokens, a_spans
    cdef tuple t_span
    
    for a_tokens in token_lists:
        a_spans = get_token_spans(
            text, a_tokens,
            i_max_missing=i_max_missing, b_case_insensitive=b_case_insensitive, i_start=i_start)
        a_span_lists.append(a_spans)
        for t_span in reversed(a_spans):
            if t_span is not None:
                i_start = t_span[1]
                break
    return a_span_lists


cpdef tuple get_phrase_position(v_sent, v_phrase,
                                bint b_from_beginning=True,
                                bint b_case_insensitive=False,
                                bint b_punctuation_insensitive=True,
                                int skipgram_length=0,
                                bint b_plural_last_insensitive=False,
                                bint b_plural_insensitive=False,
                               ):
    ''' Look for a phrase of multiple sequential items in a larger set of items.
    Return a tuple of position. '''
    if not v_sent or not v_phrase: return None
    cdef int i_len_phrase = len(v_phrase)
    cdef int i_len_sent = len(v_sent)
    cdef int i_index, i_phrase_index, i_sent_index, i_adjusted_length
    cdef str s_word
    cdef list a_phrase, a_sent, a_phrase_orig, a_sent_orig
    # if the phrase is larger than the sent, it can't exist in the sent. Add fuzziness to account for punctuation diffs
    if i_len_phrase > i_len_sent + 20: return None
    
    #if isinstance(v_phrase, (spSpan.Span, spTok.Token, spDoc.Doc) ):
    #    a_phrase = [x.text for x in v_phrase]
    if isinstance(v_phrase, str):
        a_phrase = word_tokenize(v_phrase, stop_words=None, remove_punct=False)
    elif isinstance(v_phrase, tuple):
        a_phrase = list(v_phrase)
    else:
        a_phrase = v_phrase
        
    #if isinstance(v_sent, (spSpan.Span, spTok.Token, spDoc.Doc) ):
    #    a_sent = [x.text for x in v_sent]
    if isinstance(v_sent, str):
        a_sent = word_tokenize(v_sent, stop_words=None, remove_punct=False)
    elif isinstance(v_sent, tuple):
        a_sent = list(v_sent)
    else:
        a_sent = v_sent
        
    if b_case_insensitive:
        a_phrase = [s_word.lower() for s_word in a_phrase]
        a_sent = [s_word.lower() for s_word in a_sent]
        
    a_phrase_orig = a_phrase
    a_sent_orig = a_sent
    if b_punctuation_insensitive:
        a_phrase = [remove_all_punctuation(s_word, b_keepspaces=False) for s_word in a_phrase]
        a_sent = [remove_all_punctuation(s_word, b_keepspaces=False) for s_word in a_sent]
    
    i_len_phrase = len(a_phrase)
    i_len_sent = len(a_sent)
    if i_len_phrase > i_len_sent: return None  # if the phrase is larger than the set, it can't exist in the set
    
    # find the indexes of the first non-blank, and last non-blank pieces of the phrase
    i_first_phrase, i_last_phrase = -1, -1
    for i_index, s_word in enumerate(a_phrase):
        if s_word:
            if i_first_phrase < 0: i_first_phrase = i_index
            i_last_phrase = i_index
    if i_last_phrase == -1:
        # phrase was only punctuation, might as well try matching with it instead of just returning none
        a_phrase, a_sent = a_phrase_orig, a_sent_orig
        i_first_phrase, i_last_phrase = 0, i_len_sent - 1

    i_adjusted_length = (i_last_phrase - i_first_phrase) + 1
    if b_from_beginning:
        r_order = range(i_len_sent - i_adjusted_length + 1)
    else:
        r_order = reversed(range(i_len_sent - i_adjusted_length + 1))
    
    for i_index in r_order:
        # find first occurence of the first word of the phrase in the larger set
        if (
            a_phrase[i_first_phrase] == a_sent[i_index] or
            (a_phrase[i_first_phrase] in {'and', '&', 'And', 'AND'} and a_sent[i_index] in {'and', '&', 'And', 'AND'}) or
            (b_plural_insensitive and
             (pluralize(a_phrase[i_first_phrase]) == a_sent[i_index] or
              singularize(a_phrase[i_first_phrase]) == a_sent[i_index]
             )
            )
        ):
            b_skip_position = False
            i_skipped = 0
            i_phrase_index, i_sent_index = i_first_phrase + 1, i_index + 1
            
            if i_phrase_index == i_len_phrase:  # if there are no trailing words to check
                # extend to capture leading/trailing punctuation
                if i_first_phrase != 0 and i_index > 0:
                    if not a_sent[i_index - 1]: i_index -= 1
                if i_last_phrase < i_len_phrase - 1 and i_sent_index < i_len_sent - 1:
                    if not a_sent[i_sent_index + 1]: i_sent_index += 1
                return (i_index, i_sent_index)
                
            else:  # make sure rest of phrase matches
                while i_phrase_index <= i_last_phrase and i_sent_index < i_len_sent:
                    if (
                        a_phrase[i_phrase_index] == a_sent[i_sent_index] or
                        (a_phrase[i_phrase_index] in {'and', '&', 'And', 'AND'} and a_sent[i_sent_index] in {'and', '&', 'And', 'AND'}) or
                        ((b_plural_insensitive or 
                          (b_plural_last_insensitive and
                           i_phrase_index == i_last_phrase)
                         ) and
                         (pluralize(a_phrase[i_phrase_index]) == a_sent[i_sent_index] or
                          singularize(a_phrase[i_phrase_index]) == a_sent[i_sent_index]
                         )
                        )
                    ):
                        if i_phrase_index == i_last_phrase:
                            # extend to capture leading/trailing punctuation
                            if i_first_phrase != 0 and i_index > 0:
                                if not a_sent[i_index - 1]: i_index -= 1
                            if i_last_phrase < i_len_phrase - 1 and i_sent_index < i_len_sent - 1:
                                if not a_sent[i_sent_index + 1]: i_sent_index += 1
                            return (i_index, i_sent_index + 1)
                        else:
                            i_phrase_index += 1
                            i_sent_index += 1
                    else:
                        while not a_phrase[i_phrase_index]:
                            i_phrase_index += 1  # skip blank phrase pieces
                            if i_phrase_index > i_last_phrase:
                                b_skip_position = True
                                break

                        while not a_sent[i_sent_index]:
                            i_sent_index += 1  # skip blank sentence pieces
                            if i_sent_index >= i_len_sent:
                                b_skip_position = True
                                break
                        
                        if i_phrase_index > i_last_phrase or i_sent_index >= i_len_sent:
                            pass
                        elif a_phrase[i_phrase_index] == a_sent[i_sent_index]:
                            if b_skip_position or i_phrase_index < i_last_phrase:
                                i_phrase_index += 1
                                i_sent_index += 1
                            else:
                                #extend to capture leading/trailing punctuation and return position
                                if i_first_phrase != 0 and i_index > 0:
                                    if not a_sent[i_index - 1]: i_index -= 1
                                if i_last_phrase < i_len_phrase - 1 and i_sent_index < i_len_sent - 1:
                                    if not a_sent[i_sent_index + 1]: i_sent_index += 1
                                return (i_index, i_sent_index)
                        else:
                            i_skipped += 1
                            if i_skipped > skipgram_length:
                                i_phrase_index += 1
                                i_sent_index += 1
                                continue
                            else:
                                i_sent_index += 1
                
    return None  # "Phrase not found"


cpdef list get_all_phrase_positions(v_sent, v_phrase,
                                    b_case_insensitive=False,
                                    b_punctuation_insensitive=True):
    ''' Get a list of all the positions as tuples
        where a word or phrase appears in a document. '''

    cdef int i_len_phrase = len(v_phrase)
    cdef int i_len_sent = len(v_sent)
    cdef int i_index, i_phrase_index, i_sent_index, i_adjusted_length
    cdef str s_word
    cdef list a_phrase, a_sent, a_phrase_orig, a_sent_orig
    cdef list a_found_positions = []
    
    if not v_sent or not v_phrase: return a_found_positions
    
    # if the phrase is larger than the set, it can't exist in the set
    if i_len_phrase > i_len_sent: return a_found_positions
    
    if isinstance(v_phrase, str):
        a_phrase = word_tokenize(v_phrase, stop_words=False, remove_punct=False)
    elif isinstance(v_phrase, tuple):
        a_phrase = list(v_phrase)
    else:
        a_phrase = v_phrase
    
    if isinstance(v_sent, str):
        a_sent = word_tokenize(v_sent, stop_words=False, remove_punct=False)
    elif isinstance(v_sent, tuple):
        a_sent = list(v_sent)
    else:
        a_sent = v_sent
        
    if b_case_insensitive:
        a_phrase = [s_word.lower() for s_word in a_phrase]
        a_sent = [s_word.lower() for s_word in a_sent]
        
    a_phrase_orig = a_phrase
    a_sent_orig = a_sent
    if b_punctuation_insensitive:
        a_phrase = [remove_all_punctuation(s_word, b_keepspaces=False) for s_word in a_phrase]
        a_sent = [remove_all_punctuation(s_word, b_keepspaces=False) for s_word in a_sent]
        
    i_len_phrase = len(a_phrase)
    i_len_sent = len(a_sent)
    
    # if the phrase is larger than the set, it can't exist in the set
    if i_len_phrase > i_len_sent: return a_found_positions
    
    # find the indexes of the first non-blank, and last non-blank pieces of the phrase
    i_first_phrase, i_last_phrase = -1, -1
    for i_index, s_word in enumerate(a_phrase):
        if s_word:
            if i_first_phrase < 0: i_first_phrase = i_index
            i_last_phrase = i_index
    
    if i_last_phrase == -1:
        # phrase was only punctuation, might as well try matching with it instead of just returning none
        a_phrase, a_sent = a_phrase_orig, a_sent_orig
        i_first_phrase, i_last_phrase = 0, i_len_sent - 1

    i_adjusted_length = (i_last_phrase - i_first_phrase) + 1
    r_order = range(i_len_sent - i_adjusted_length + 1)
    
    for i_index in r_order:
        # find first occurence of the first word of the phrase in the larger set
        if a_phrase[i_first_phrase] == a_sent[i_index]:
            b_skip_position = False
            i_phrase_index, i_sent_index = i_first_phrase + 1, i_index + 1
            
            if i_len_phrase == 1:
                a_found_positions.append((i_index, i_sent_index))
                
            elif i_phrase_index > i_last_phrase and i_phrase_index < i_len_phrase:  # if there are no trailing words to check
                # extend to capture leading/trailing punctuation
                if i_first_phrase != 0 and i_index > 0:
                    if not a_sent[i_index - 1]: i_index -= 1
                if i_last_phrase < i_len_phrase - 1 and i_sent_index < i_len_sent - 1:
                    if not a_sent[i_sent_index + 1]: i_sent_index += 1
                        
                a_found_positions.append((i_index, i_sent_index + 1))
                i_phrase_index += i_len_phrase
                i_sent_index += i_len_phrase
                
            else:  # make sure rest of phrase matches
                while i_phrase_index <= i_last_phrase and i_sent_index < i_len_sent:
                    if a_phrase[i_phrase_index] == a_sent[i_sent_index]:
                        if i_phrase_index == i_last_phrase:
                            # extend to capture leading/trailing punctuation
                            if i_first_phrase != 0 and i_index > 0:
                                if not a_sent[i_index - 1]: i_index -= 1
                            if i_last_phrase < i_len_phrase - 1 and i_sent_index < i_len_sent - 1:
                                if not a_sent[i_sent_index + 1]: i_sent_index += 1
                            a_found_positions.append((i_index, i_sent_index + 1))
                            i_phrase_index += i_len_phrase
                            i_sent_index += i_len_phrase
                        else:
                            i_phrase_index += 1
                            i_sent_index += 1
                    else:
                        while not a_phrase[i_phrase_index]:
                            i_phrase_index += 1  # skip blank phrase pieces
                            if i_phrase_index > i_last_phrase:
                                b_skip_position = True
                                break

                        while not a_sent[i_sent_index]:
                            i_sent_index += 1  # skip blank sentence pieces
                            if i_sent_index >= i_len_sent:
                                b_skip_position = True
                                break

                        if i_phrase_index > i_last_phrase or i_sent_index >= i_len_sent:
                            pass
                        elif a_phrase[i_phrase_index] == a_sent[i_sent_index]:
                            if b_skip_position or i_phrase_index < i_last_phrase:
                                i_phrase_index += 1
                                i_sent_index += 1
                            else:
                                #extend to capture leading/trailing punctuation
                                if i_first_phrase != 0 and i_index > 0:
                                    if not a_sent[i_index - 1]: i_index -= 1
                                if i_last_phrase < i_len_phrase - 1 and i_sent_index < i_len_sent - 1:
                                    if not a_sent[i_sent_index + 1]: i_sent_index += 1
                                a_found_positions.append((i_index, i_sent_index))
                                i_phrase_index += i_len_phrase
                                i_sent_index += i_len_phrase
                        else:
                            i_phrase_index += 1
                            i_sent_index += 1
                
    return a_found_positions




cpdef list get_proximities(list a_target, list a_source):
    ''' Measure the nearest occurence for each target item to the nearest source item.
        Choose in absolute distance, but return in relative distance.
        Negative distance means target is before nearest source, positive is after nearest source. 
    '''
    if not a_target: return []
    if not a_source: return [99999] * len(a_target)
    
    #sort the lists so it can jump out of the loop after finding the first source after target
    a_target.sort()
    a_source.sort()
    
    cpdef list a_min_dist = []
    cpdef tuple t_tg, t_sc
    cdef int i_min, i_before, i_after
    
    for t_tg in a_target:
        i_min = 99999
        for t_sc in a_source:
            if t_tg[0] >= t_sc[1]:  # target is after source
                i_before = t_tg[0] - t_sc[1]
                if abs(i_before) + 1 < abs(i_min):
                    i_min = i_before - 1 if i_before < 0 else i_before + 1
            elif t_tg[1] <= t_sc[0]:  # target is before source
                i_after = t_tg[1] - t_sc[0]
                if abs(i_after) + 1 < abs(i_min):
                    i_min = i_after - 1 if i_after <= 0 else i_after + 1
                break
            else:  # target and source overlap
                i_min = 0
        a_min_dist.append(i_min)
    return a_min_dist


cdef int abs_min_list(list values):
    return min(values, key=lambda x: abs(x))


cpdef int proximity(list a_target, list a_source):
    ''' returns the single nearest proximity '''
    cdef list a_proximities = get_proximities(a_target, a_source)
    if a_proximities:
        return abs_min_list(a_proximities)
    else:
        return 99999


cpdef tuple get_nearest_proximity(list a_target, list a_source):
    ''' returns a tuple of nearest proximity, and which target item it belongs to '''
    cdef list a_proximities = get_proximities(a_target, a_source)
    if a_proximities:
        i_min, i_min_pos = a_proximities[0], 0
        for i_pos, i_dist in enumerate(a_proximities):
            if abs(i_dist) < abs(i_min):
                i_min, i_min_pos = i_dist, i_pos
        if i_min == 99999:
            return None
        else:
            return (i_min, i_min_pos)
    else:
        return None


cpdef list get_sentence_proximity(
    tuple t_search_text, tuple t_anchor_text, list a_sentence, list a_anchor_pos=None, int i_max_distance=10,
    bint b_try_plural=True, b_try_singular=True
):
    ''' Find the closest distances between the occurences of the search text and
        the anchor text inside the description
        Arguments:
            t_search_text: {tuple} of the text to look for
            t_anchor_test: {tuple} of the text to use as the reference anchor
            a_sentence: {list} the tokenized sentence
            a_anchor_pos: optional {list} of already discovered positions for the anchor
            i_max_distance: {int} ignore instances farther than this from an anchor
            b_try_plural: {bool} whether to try the plural version of the search text
            b_try_singular: {bool} whether to try the singular version of the search text
    '''    
    cdef list a_text_pos, a_sent_prox
    cdef tuple t_plural, t_singular
    cdef int x
    cdef set c_sent
    
    c_sent = set(a_sentence)
    #print('c_sent', c_sent)
    
    if a_anchor_pos is None:
        a_anchor_pos = get_all_phrase_positions(a_sentence, t_anchor_text, b_case_insensitive=True)
        if b_try_plural:
            t_plural = pluralize_tuple(t_anchor_text)
            if t_plural != t_anchor_text and t_plural[-1] in c_sent:
                a_anchor_pos.extend(get_all_phrase_positions(a_sentence, t_plural, b_case_insensitive=True))
        if b_try_singular:
            t_singular = singularize_tuple(t_anchor_text)
            if t_singular != t_anchor_text and t_singular[-1] in c_sent:
                a_anchor_pos.extend(get_all_phrase_positions(a_sentence, t_singular, b_case_insensitive=True))
    
    if not a_anchor_pos: return []
    a_text_pos = get_all_phrase_positions(a_sentence, t_search_text, b_case_insensitive=True)
    
    if b_try_plural:
        t_plural = pluralize_tuple(t_search_text)
        if t_plural != t_search_text and t_plural[-1] in c_sent:
            a_text_pos.extend(get_all_phrase_positions(a_sentence, t_plural, b_case_insensitive=True))
    
    if b_try_singular:
        t_singular = singularize_tuple(t_search_text)
        if t_singular != t_search_text and t_singular[-1] in c_sent:
            a_text_pos.extend(get_all_phrase_positions(a_sentence, t_singular, b_case_insensitive=True))

    a_sent_prox = get_proximities(a_text_pos, a_anchor_pos)
    
    return [x for x in a_sent_prox if x <= i_max_distance]


cpdef list get_tokens_between(list a_text, tuple t_span1, tuple t_span2):
    ''' returns the words that are in a_text between t_span1 and t_span2 '''
    if not a_text or t_span1 == t_span2:
        return []
    elif t_span1[1] <  t_span2[0]:
        return a_text[t_span1[1]:t_span2[0]]
    elif t_span2[1] <  t_span1[0]:
        return a_text[t_span2[1]:t_span1[0]]
    else:
        return []


cpdef bint are_two_substrings_consecutive(str outter_string, list substrings,
                                          bint case_sensitive=False) except -1:
    '''
    @description: checks whether two substrings are consecutive, in a given order,
    within a longer string.
    @arg outter_string: {str} The outter, longer string.
    @arg substrings: {list} An ordered list of two strings.
    @return: {bool} Whether the substrings are consecutive, within outter_string,
    in an ordered fashion.
    '''
    if len(substrings) != 2:
        raise Exception("This function only takes 2 substrings")
    cdef re_consecutive, match

    if case_sensitive:
        re_consecutive = re.compile(
            r"\b{}\b[^a-z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚]*\b{}\b".format(re.escape(substrings[0]), re.escape(substrings[1]))
        )
    else:
        re_consecutive = re.compile(
            r"\b{}\b[^a-z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚]*\b{}\b".format(re.escape(substrings[0]), re.escape(substrings[1])),
            flags=re.I
        )
    match = re_consecutive.search(outter_string)
    if match:
        return True
    else:
        return False


cpdef bint are_consecutive_ordered(str outter_string, list substrings,
                                   bint case_sensitive=False):
    '''
    @description: Validates whether a list of strings, are present in a consecutive
    and ordered fashion, within a longer outter string.
    @arg outter_string: {str}
    @arg substrings: {list} The ordered list of substrings.
    @return: {bool} Whether the substring are present in a consecutive and ordered
    fashion, within the outter string.
    '''
    cdef int ind
    cdef str substring
    for ind, substring in enumerate(substrings[:-1]):
        if not are_two_substrings_consecutive(outter_string,
                                              [substrings[ind], substrings[ind + 1]],
                                              case_sensitive=case_sensitive):
            return False
    return True


cpdef bint are_substrings_consecutive(str s_string, list a_substrings,
                                      bint case_sensitive=False):
    ''' Check if list of strings are consecutive in any order in a larger string
    (not separated by other letters or numbers. '''
    cdef tuple permutation
    for permutation in itertools.permutations(a_substrings):
        if are_consecutive_ordered(s_string, list(permutation),
                                   case_sensitive=case_sensitive):
            return True
    return False
