import pyximport; pyximport.install()

try:
    from cleaning import *
except:
    from .cleaning import *
try:
    from font_widths import *
except:
    from .font_widths import *
try:
    from plural import *
except:
    from .plural import *
try:
    from tokenize import *
except:
    from .tokenize import *
try:
    from ngrams import *
except:
    from .ngrams import *
try:
    from search import *
except:
    from .search import *
try:
    from text import *
except:
    from .text import *
try:
    from dimensions import *
except:
    from .dimensions import *

__version__ = "1.2.2"