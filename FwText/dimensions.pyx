﻿import re
from collections import defaultdict, Counter
from .dimension_abbreviations import e_ABBR_NAMES, e_CONTEXTS

__all__ = [
    "make_unit_regex_name", "make_unit_regex_str",
    "make_metric_regex_name", "make_metric_regex_str",
    "make_count_regex_str", "make_pack_regex_str",
    "get_abbr_names", "get_abbr_name",
    "TokenizationResult", "Dimensionizer",
    "a_count_digits", "a_count_words", "c_dimension_names",
]

cdef dict e_ABBREVIATION_NAMES = <dict> e_ABBR_NAMES
cdef dict e_ABBREVIATION_CONTEXTS = <dict> e_CONTEXTS

a_count_digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
a_count_words = [
    'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten',
    'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen',
    'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety',
    'hundred', 'thousand', 'million', 'billion', 'trillion',
]
cdef set c_COUNT_WORDS = set(a_count_words)
e_count_words = c_COUNT_WORDS
cdef set c_COUNT_DIGITS = set(a_count_digits)
cdef set c_COUNTS = c_COUNT_DIGITS.union(c_COUNT_WORDS)

a_PACK_WORDS = [
    'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten',
    'eleven', 'twelve', 'fifteen', 'sixteen', 'eighteen', 'twenty',
]
PACK = r"({})".format('|'.join(a_PACK_WORDS))

BEFORE = r"(^|(?<![.0-9]))"
BETWEEN = r"[_ ]*(-|x|by|~)[_ ]*"  # separator between 2 numbers indication multiple-dimension
BETWEEN2 = r"[_ ]*(-|/|x|by|~)[_ ]*"  # separator between 2 numbers indication multiple-dimension
BETWEEN3 = r"[_ ]*(-|/|x|to|thru|~)[_ ]*"  # separator between 2 numbers indicating a range
FRAC_SEP = r"( | ?- ?)"  # separator between whole number and attached fraction like "1 - 1/2"
UNIT_SEP = r"( *| ?- ?)?"

# "([^ a-z0-9-]|\)|$)"
BR = r"(\b|$)"
ABBR_BR = r"(\.|\b|$)"
ABBR_END = r"(\.|\.?$)"
STRICT_ABBR_BR = ABBR_END  #r"\."

P_ABBR_BR = r"\.?"

WHOLE = r"(\d+,)*\d+"
DECIMAL = f"({WHOLE}(\.\d+)?|\.\d+)"
FRACTION = f"(\d+{FRAC_SEP})?\d+/\d+"
NUMBER = f"({DECIMAL}|{FRACTION})"
SQUARE = f"(sq\.?|square)[- ]?"
CUBIC = f"(cu\.?|cubic)[- ]?"

re_UOM_ABBR = re.compile(r"((?<=\d)['\"°]+|(?<=_)[a-z0-9°μΩ]*[a-z°μΩ]+[_a-z0-9°μΩ²³]*\.?)", flags=re.I)
#re_UOM_ABBR_REVERSED = re.compile(r"^([a-z]+_)*[a-z]+(?=_\d)", flags=re.I)
re_UOM_ABBR_REVERSED = re.compile(f"^(([a-z]+_)*[a-z]+)_({WHOLE})", flags=re.I)

c_dimension_names = set(
    ["inch", "inches", "in.", "in",
     "foot", "feet", "ft.", "ft",
     "yard", "yards", "yd.", "yds.", "yd", "yds",
     "millimeter", "millimeters", "mm.", "mm",
     "centimeter", "centimeters", "cm.", "cm",
     "meter", "meters", "m.", "m",
     "pound", "pounds", "lb.", "lbs.", "lb", "lbs",
     "gram", "grams", "g.", "gs.", "g", "gs", "gm.", "gms.", "gm", "gms",
     "kilogram", "kilograms", "kg.", "kgs.", "kg", "kgs",
     "ounce", "ounces", "oz.", "oz",
     "fluid ounce", "fluid ounces", "fluid oz.", "fluid oz",
     "fl. ounce", "fl. ounces", "fl oz.", "fl oz",
     "fl. ounce", "fl. ounces", "fl ounce", "fl ounces",
     "cup", "cups", "c.", "c",
     "quart", "quarts", "qt.", "qt",
     "gallon", "gallons", "gal.", "gal", "gall.", "gall", "gl.", "gl",
     "liter", "liters", "litre", "litres", "ltr.", "ltr","l.","l",
     "milliliter", "milliliters", "millilitre", "millilitres", "ml", "ml",
     "cubic cm", "cubic cms", "cubic centimeter", "cubic centimeters", "cc.", "cc", "ccs.", "ccs",
     "mil", "mils", "mil.", "mils.",
     "amp", "amps", "a.", "a",
     "milliamp", "milliamps", "mah.", "mah",
     "volt", "volts", "v.", "v",
     "millivolt", "millivolts", "mv.", "mv",
     "watt", "watts", "w.", "w",
     "milliwatt", "milliwatts", "mw.", "mw",
     "gauge", "gauges", "ga.", "ga",
     "hertz", "hz.", "hz",
     "kilohertz", "khz.", "khz",
     "megahertz", "mhz.", "mhz",
     "gigahertz", "ghz.", "ghz",
     "terrahertz", "thz.", "thz",
     "lumen", "lumens", "lm.", "lm",
     "bytes", "b.", "b",
     "kilobytes", "kb.", "kb",
     "megabytes", "mb.", "mb",
     "gigabytes", "gb.", "gb",
     "terrabytes", "tb.", "tb",
     "pack", "packs", "pk.", "pk", "pck.", "pck", "pks.", "pks", "pcks.", "pcks",
     "pair", "pairs", "pr.", "pr",
     "dozen", "dozens", "dz.", "dz",
     "gross",
     "piece", "pieces", "pc.", "pc", "pcs.", "pcs",
     "year", "years", "yr.", "yr", "yrs.", "yrs",
     "month", "months", "mon.", "mon", "mons.", "mons",
     "day", "days", "dy.", "dy", "dys.", "dys",
     "hour", "hours", "hr.", "hr", "hrs.", "hrs",
     "minute", "minutes", "min.", "min", "mins.", "mins",
     "second", "seconds", "sec.", "sec", "secs.", "secs",
     "karat", "karats", "k.", "k", "ks.", "ks", "kt.", "kt", "kts.", "kts",
     "carat", "carats", "c.", "c", "ct.", "ct", "cw.", "cw", "tw.", "tw", "ctw.",
     "ctw", "cwt.", "cwt", "cttw.", "cttw", "dw.", "dw", "dtw.", "dtw", "tdw.", "tdw",
     "horsepower", "hp.", "hp",
     "joules", "j.", "j",
     "pounds per square inch", "pounds per square in", "pounds per square in.",
     "lbs per square inch", "lbs per square in", "lbs per square in.",
     "lbs. per square inch", "lbs. per square in", "lbs. per square in.",
     "lbs. per sq inch", "lbs. per sq in", "lbs. per sq in.",
     "lbs. per sq. inch", "lbs. per sq. in", "lbs. per sq. in.",
     "psi", "psi.", "p.s.i.",
     "pounds per square inch absolute", "psia", "psia.", "p.s.i.a.",
     "pascal", "pa", "pa.",
     "decibel", "db.", "db",
     "degree", "d.", "d",
     "fahrenheit", "f.", "f",
     "celcius", "c.", "c",
     "kelvin", "k.", "k",
     ]
)


def make_unit_regex_name(
    name,
    abbr,
    exp='',
    b_in_parenthesis=True,
    b_strict_single_char=True,
    b_optional_break=False,
):
    ''' turn a name and/or abbr into a regular expression string matching whole, decimal and fraction numbers
        Arguments:
            name: {str} the UOM name as plain text or regex formula text
            abbr: {str} the UOM name as plain text, regex formula text, or {list} of separate abbr pieces
            exp: {str} exponent to add between the name/abbr and potential pluralization
        Returns:
            {str} of text ready to be put into re.compile
    '''
    assert name or abbr
    
    if b_optional_break:
        s_br = ''
        s_abbr_br = P_ABBR_BR
        s_strict_br = P_ABBR_BR
    else:
        s_br = BR
        s_abbr_br = ABBR_BR
        s_strict_br = STRICT_ABBR_BR
    
    a_pieces = [] 
    if name:
        a_pieces.append(f"{name}s?{exp}{s_br}")
        a_pieces.append(f"\({name}\)")
    
    if not exp:
        if isinstance(abbr, (list, tuple)):
            s_both = "|".join(abbr)
            if b_strict_single_char:
                s_short = "|".join([x for x in abbr if len(x) == 1])
                s_long = "|".join([x for x in abbr if len(x) > 1])
                a_pieces.append(f"({s_short})s?{s_br}")
                a_pieces.append(f"({s_long})s?{s_abbr_br}")
            else:
                a_pieces.append(f"({s_both})s?{s_abbr_br}")
            
            if b_in_parenthesis:
                a_pieces.append(f"\(({s_both})s?{P_ABBR_BR}\)")

        elif len(abbr) == 1 and b_strict_single_char:
            a_pieces.append(f"({abbr})s?{s_br}".format(abbr))
            if b_in_parenthesis:
                a_pieces.append(f"\(({abbr})s?{P_ABBR_BR}\)".format( "|".join(abbr)))
        elif abbr:
            a_pieces.append(f"({abbr})s?{s_abbr_br}")
            if b_in_parenthesis:
                a_pieces.append(f"\(({abbr})s?{P_ABBR_BR}\)")
        
    elif abbr:
        if isinstance(abbr, (list, tuple)): abbr = "|".join(abbr)
        a_pieces.append(f"({abbr})s?{exp}{s_abbr_br}")
        if b_in_parenthesis:
            a_pieces.append(f"\(({abbr})s?{exp}{P_ABBR_BR}\)")
        
    unit = r"({})".format("|".join(a_pieces))
    return unit


def make_unit_regex_str(
    name,
    abbr,
    exp='',
    b_in_parenthesis=True,
    b_strict_single_char=True,
    b_range=False,):
    ''' turn a name and/or abbr into a regular expression string matching whole, decimal and fraction numbers
        Arguments:
            name: {str} the UOM name as plain text or regex formula text
            abbr: {str} the UOM name as plain text, regex formula text, or {list} of separate abbr pieces
            exp: {str} exponent to add between the name/abbr and potential pluralization
            b_in_parenthesis: {bool} allow abbr or name to be in parenthesis. Turn off if output is to be in ratio
            b_strict_single_char: {bool} require strict abbr suffix if abbreviation is single digit
            b_range: {bool} to match range of numbers
        Returns:
            {str} of text ready to be put into re.compile
    '''
    assert name or abbr
    if b_range:
        unit = f"{BEFORE}({NUMBER}{BETWEEN3})?{NUMBER}{UNIT_SEP}"
    else:
        unit = f"{BEFORE}{NUMBER}{UNIT_SEP}"
    
    a_pieces = [] 
    if name:
        a_pieces.append(f"{name}s?{exp}{BR}")
        a_pieces.append(f"\({name}\)")
    
    if not exp:
        if isinstance(abbr, (list, tuple)):
            s_both = "|".join(abbr)
            if b_strict_single_char:
                s_short = "|".join([x for x in abbr if len(x) == 1])
                s_long = "|".join([x for x in abbr if len(x) > 1])
                a_pieces.append(f"({s_short})s?{STRICT_ABBR_BR}")
                a_pieces.append(f"({s_long})s?{ABBR_BR}")
            else:
                a_pieces.append(f"({s_both})s?{ABBR_BR}")
            
            if b_in_parenthesis:
                a_pieces.append(f"\(({s_both})s?{P_ABBR_BR}\)")

        elif len(abbr) == 1 and b_strict_single_char:
            a_pieces.append(f"({abbr})s?{STRICT_ABBR_BR}".format(abbr))
            if b_in_parenthesis:
                a_pieces.append(f"\(({abbr})s?{P_ABBR_BR}\)".format( "|".join(abbr)))
        elif abbr:
            a_pieces.append(f"({abbr})s?{ABBR_BR}")
            if b_in_parenthesis:
                a_pieces.append(f"\(({abbr})s?{P_ABBR_BR}\)")
        
    elif abbr:
        if isinstance(abbr, (list, tuple)): abbr = "|".join(abbr)
        a_pieces.append(f"({abbr})s?{exp}{ABBR_BR}")
        if b_in_parenthesis:
            a_pieces.append(f"\(({abbr})s?{exp}{P_ABBR_BR}\)")
        
    unit += r"({})".format("|".join(a_pieces))
    return unit


def make_metric_regex_name(
    name, abbr,
    exp='',
    b_small=True,
    b_large=True,
    b_tenth_hundredth=False,
    b_ten_hundred=False,
    b_in_parenthesis=True,
    b_strict_single_char=True,
):
    ''' turn a name and/or abbr into a regular expression string for just the name portion, with SI metric quantity prefixes
        Arguments:
            name: {str} the UOM name as plain text or regex formula text
            abbr: {str} the UOM name as plain text, regex formula text, or {list} of separate abbr pieces
            exp: {str} exponent to add between the name/abbr and potential pluralization
            b_whole: {bool} to match only whole numbers instead of decimals/fractions
            b_small: {bool} match small prefixes (1/1000th and smaller) (default True)
            b_large: {bool} match large prefixes (1000 and bigger) (default True)
            b_tenth_hundredth: {bool} match centi/deci (default false)
            b_ten_hundred: {bool} match deca/hecto (default false)
            b_in_parenthesis: {bool} allow abbr or name to be in parenthesis. Turn off if output is to be in ratio
            b_strict_single_char: {bool} require strict abbr suffix if abbreviation is single digit
        Returns:
            {str} of text ready to be put into re.compile or combined with a number regex string
    '''
    small_letters = ["f", "n", "μ", "m"]
    large_letters = ["k", "m", "g", "t", "p", "e"]
    th_small_letters = ["c", "d"]
    th_large_letters = ["dk", "h"]
    small_words = ["femto", "nano", "milli"]
    large_words = ["kilo", "mega", "giga", "tera", "peta", "exa"]
    th_small_words = ["centi", "deci"]
    th_large_words = ["deca", "hecto"]
    
    prefix_abbr, prefix_words = [], []
    if b_small:
        prefix_abbr += small_letters
        prefix_words += small_words
    if b_tenth_hundredth:
        prefix_abbr += th_small_letters
        prefix_words += th_small_words
        
    if b_large:
        prefix_abbr += large_letters
        prefix_words += large_words
    if b_ten_hundred:
        prefix_abbr += th_large_letters
        prefix_words += th_large_words
        
    prefix_abbr = r"({})".format("|".join(prefix_abbr)) if prefix_abbr else ''
    prefix_words = r"({})".format("|".join(prefix_words)) if prefix_words else ''
    
    if len(abbr) == 1 and not exp and b_strict_single_char:
        metric_abbr = f"({prefix_abbr}{abbr}s?{exp}{ABBR_BR}|{abbr}{exp}{STRICT_ABBR_BR})"
    elif abbr:
        metric_abbr = f"{prefix_abbr}?{abbr}s?{exp}{ABBR_BR}"
    
    metric_name = f"{prefix_words}?{name}s?{exp}{BR}"
    
    if b_in_parenthesis:
        s_unit = f"(({metric_abbr}|{metric_name})|\(({metric_abbr}|{metric_name})\))"
    else:
        s_unit = f"({metric_abbr}|{metric_name})"
    return s_unit


def make_metric_regex_str(
    name, abbr,
    exp='',
    b_whole=False,
    b_range=False,
    b_small=True,
    b_large=True,
    b_tenth_hundredth=False,
    b_ten_hundred=False,
    b_in_parenthesis=True,
    b_strict_single_char=True,
):
    ''' turn a name and/or abbr into a regular expression string, with SI metric quantity prefixes
        Arguments:
            name: {str} the UOM name as plain text or regex formula text
            abbr: {str} the UOM name as plain text, regex formula text, or {list} of separate abbr pieces
            exp: {str} exponent to add between the name/abbr and potential pluralization
            b_whole: {bool} to match only whole numbers instead of decimals/fractions
            b_range: {bool} to match range of numbers
            b_small: {bool} match small prefixes (1/1000th and smaller) (default True)
            b_large: {bool} match large prefixes (1000 and bigger) (default True)
            b_tenth_hundredth: {bool} match centi/deci (default false)
            b_ten_hundred: {bool} match deca/hecto (default false)
            b_in_parenthesis: {bool} allow abbr or name to be in parenthesis. Turn off if output is to be in ratio
            b_strict_single_char: {bool} require strict abbr suffix if abbreviation is single digit
        Returns:
            {str} of text ready to be put into re.compile
    '''
    '''
    small_letters = ["f", "n", "μ", "m"]
    large_letters = ["k", "m", "g", "t", "p", "e"]
    th_small_letters = ["c", "d"]
    th_large_letters = ["dk", "h"]
    small_words = ["femto", "nano", "milli"]
    large_words = ["kilo", "mega", "giga", "tera", "peta", "exa"]
    th_small_words = ["centi", "deci"]
    th_large_words = ["deca", "hecto"]
    
    prefix_abbr, prefix_words = [], []
    if b_small:
        prefix_abbr += small_letters
        prefix_words += small_words
    if b_tenth_hundredth:
        prefix_abbr += th_small_letters
        prefix_words += th_small_words
        
    if b_large:
        prefix_abbr += large_letters
        prefix_words += large_words
    if b_ten_hundred:
        prefix_abbr += th_large_letters
        prefix_words += th_large_words
        
    prefix_abbr = r"({})".format("|".join(prefix_abbr)) if prefix_abbr else ''
    prefix_words = r"({})".format("|".join(prefix_words)) if prefix_words else ''
    
    if len(abbr) == 1 and not exp:
        metric_abbr = f"({prefix_abbr}{abbr}s?{exp}{ABBR_BR}|{abbr}{exp}{STRICT_ABBR_BR})"
    elif abbr:
        metric_abbr = f"{prefix_abbr}?{abbr}s?{exp}{ABBR_BR}"
    
    metric_name = f"{prefix_words}?{name}s?{exp}{BR}"
    '''
    unit_name = make_metric_regex_name(
        name=name,
        abbr=abbr,
        exp=exp,
        b_small=b_small,
        b_large=b_large,
        b_tenth_hundredth=b_tenth_hundredth,
        b_ten_hundred=b_ten_hundred,
        b_in_parenthesis=b_in_parenthesis,
        b_strict_single_char=b_strict_single_char,
    )
    
    if b_range:
        if b_whole:
            s_unit = f"{BEFORE}({WHOLE}{BETWEEN3})?{WHOLE}{UNIT_SEP}{unit_name}"
        else:
            s_unit = f"{BEFORE}({NUMBER}{BETWEEN3})?{NUMBER}{UNIT_SEP}{unit_name}"
    else:
        if b_whole:
            s_unit = f"{BEFORE}{WHOLE}{UNIT_SEP}{unit_name}"
        else:
            s_unit = f"{BEFORE}{NUMBER}{UNIT_SEP}{unit_name}"
    return s_unit


def make_count_regex_str(name, abbr):
    ''' turn a name and/or abbr into a regular expression string matching whole numbers
        Arguments:
            name: {str} the UOM name as plain text or regex formula text
            abbr: {str} the UOM name as plain text, regex formula text, or {list} of separate abbr pieces
        Returns:
            {str} of text ready to be put into re.compile
    '''
    if not abbr:
        unit = f"{BEFORE}{WHOLE}{UNIT_SEP}({name}s?{BR}|\({name}s?\))"
    elif len(abbr) == 1:
        unit = f"{BEFORE}{WHOLE}{UNIT_SEP}(({abbr}s?{STRICT_ABBR_BR}|{name}s?{BR})|\(({abbr}s?{P_ABBR_BR}|{name}s?)\))"
    else:
        unit = f"{BEFORE}{WHOLE}{UNIT_SEP}(({abbr}s?{ABBR_BR}|{name}s?{BR})|\(({abbr}s?{P_ABBR_BR}|{name}s?)\))"
    return unit


def make_pack_regex_str(name, abbr):
    ''' turn a name and/or abbr into a regular expression string matching whole numbers and basic pack number words
        Arguments:
            name: {str} the UOM name as plain text or regex formula text
            abbr: {str} the UOM name as plain text, regex formula text, or {list} of separate abbr pieces
        Returns:
            {str} of text ready to be put into re.compile
    '''
    if not abbr:
        unit = f"(({PACK}|{BEFORE}{WHOLE}){UNIT_SEP}({name}s?( of)?{BR}|\({name}s?\)))"
        unit += f"|({name}s?[- ]of[- ]({PACK}|{WHOLE})([ _-]?{name}s?)?{BR})"
    elif len(abbr) == 1:
        unit = f"({PACK}|{BEFORE}{WHOLE}){UNIT_SEP}(({abbr}s?( of)?{STRICT_ABBR_BR}|{name}s?( of)?{BR})|\(({abbr}s?{P_ABBR_BR}|{name}s?)\))"
        unit += f"|(({name}s?|{abbr}/s?{P_ABBR_BR})[- ]of[- ]({PACK}|{WHOLE})([ _-]?({name}s?|{abbr}/s?{P_ABBR_BR}))?{BR})"
    else:
        unit = f"(({PACK}|{BEFORE}{WHOLE}){UNIT_SEP}(({abbr}s?( of)?{ABBR_BR}|{name}s?( of)?{BR})|\(({abbr}s?{P_ABBR_BR}|{name}s?)\)))"
        unit += f"|(({name}s?|{abbr}/s?{P_ABBR_BR})[- ]of[- ]({PACK}|{WHOLE})([ _-]?({name}s?|{abbr}/s?{P_ABBR_BR}))?{BR})"
    
    return unit


cdef tuple position_sort_key(re_match):
    ''' for sorting regex matches by start position, then end position '''
    return (re_match.start(), re_match.end())


cdef int dict_sort_key_len(tuple t_val):
    ''' for dict items by the length of the key '''
    return len(t_val[0])


cdef int dict_sort_value_len(tuple t_val):
    ''' for dict items by the length of the value '''
    return len(t_val[1])


cpdef list get_abbr_names(str s_abbr):
    ''' Get a list of the possible names for a given abbreviation
        Try the original form first before trying lowercase, without period, without other non-alphanumerics
        Arguments:
            s_abbr: {str} unit of measurement abbreviation (not including the number or unit-separator)
        Returns:
            {list} of potential UOM names or None
    '''
    if s_abbr in e_ABBREVIATION_NAMES: return e_ABBREVIATION_NAMES[s_abbr]
    
    s_abbr = s_abbr.lower()
    if s_abbr in e_ABBREVIATION_NAMES: return e_ABBREVIATION_NAMES[s_abbr]
    
    s_abbr = re.sub(r"\.", "", s_abbr)
    if s_abbr in e_ABBREVIATION_NAMES: return e_ABBREVIATION_NAMES[s_abbr]
    
    s_abbr = re.sub("[_ ]of", "", s_abbr, flags=re.I)
    if s_abbr in e_ABBREVIATION_NAMES: return e_ABBREVIATION_NAMES[s_abbr]
    
    s_abbr = re.sub(r"[^a-z0-9μΩ²³'\"]", "", s_abbr, flags=re.I)
    if s_abbr in e_ABBREVIATION_NAMES: return e_ABBREVIATION_NAMES[s_abbr]
    return []


cpdef str get_abbr_name(str s_abbr, context):
    ''' Get the best possible name for a given abbreviation
        Find the list of potential names.
        If there are none, return the original abbreviation.
        If there is just one, return it.
        If there are multiple possibilities, pick the best one if there is context included,
        otherwise return the original abbreviation,
        Arguments:
            s_abbr: {str} unit of measurement abbreviation (not including the number or unit-separator)
            context: {set} of words to check for context if there are are multiple potential matches
        Returns:
            {str} of the appropriate UOM name or the original abbreviation
    '''
    cdef list a_matches, a_names, a_context_matches
    cdef dict e_matches
    cdef set c_overlap, c_context_matches
    cdef str s_name
    
    a_names = get_abbr_names(s_abbr)
    if not a_names:
        #print('no names')
        return s_abbr
    if len(a_names) == 1: return a_names[0]
    
    if not context:
        #print('no context')
        return s_abbr
    
    if isinstance(context, str):
        #context = set(FwText.word_tokenize(context.lower(), stop_words=False))
        context = set(context.lower().split())
    elif isinstance(context, (list, tuple)):
        context = set(context)
    if not isinstance(context, set):
        #print('no context')
        return s_abbr
    
    e_matches = {}
    for s_name in a_names:
        c_overlap = e_CONTEXTS.get(s_name, set()).intersection(context)
        if not c_overlap: continue
        e_matches[s_name] = c_overlap
    if not e_matches:
        #print('no context match')
        return s_abbr
    
    ##a_matches = sorted(e_matches.items(), key=lambda x: len(x[1]), reverse=True)
    #a_matches = sorted(e_matches.items(), key=dict_sort_value_len, reverse=True)
    #return a_matches[0][0]
    s_name, c_context_matches = max(e_matches.items(), key=dict_sort_value_len)
    return s_name


cdef class TokenizationResult(object):
    """
    @description: Contains information about an indentified dimension string.
    @prop string_match: {str} contains the identified dimension string.
    @prop pattern_type: {str} contains the type of pattern that found the dimension.
    @prop position_start: {int} the start position of the match, in the original document.
    @prop position_end: {int} the end position of the match, in the original document.
    """
    cpdef public match
    cpdef public str string_match
    cpdef public str pattern_type
    cpdef public int position_start
    cpdef public int position_end

    def __init__(self, re_match, string_match, pattern_type, position_start, position_end):
        self.match = re_match
        self.string_match = string_match
        self.pattern_type = pattern_type
        self.position_start = position_start
        self.position_end = position_end\
    
    cpdef _normalize(self, bint b_replace_abbr=False, context=None):
        ''' take a dimension and try to normalize it's format
            for example standardizing spaces around "x"
        '''
        cdef str s_dimension
        cdef int i_len
        cdef bint b_multi_d
        cdef set c_uoms
        cdef list a_uoms
        
        i_len = len(self.string_match)
        s_dimension = self.string_match.replace(" ", "_")
        
        s_dimension = re.sub("[_ ]*(x|by)[_ ]*(?![a-z])", "_x_", s_dimension, flags=re.I)
        b_multi_d = i_len != len(s_dimension)
    
        s_dimension = re.sub("(?<![0-9])\.(?=[0-9])", "0.", s_dimension)
        s_dimension = re.sub(f"(?<=[0-9])_*{UNIT_SEP}_*(?=[a-z])", "_", s_dimension, flags=re.I)
        if s_dimension[-1] == '.': s_dimension = s_dimension[:-1]
        
        c_uoms = set()
        a_uoms = []
        #print('s_dimension', s_dimension)
        
        if b_replace_abbr:
            a_reversed_matches = sorted(re_UOM_ABBR_REVERSED.finditer(s_dimension), key=position_sort_key, reverse=True)
            if a_reversed_matches:
                for re_unit_match in a_reversed_matches:
                    s_unit_name = get_abbr_name(re_unit_match.group(1), context=context)
                    s_unit = re_unit_match.group(3)
                    
                    c_uoms.add(s_unit_name)
                    a_uoms.append(s_unit_name)
                    
                    #print(re_unit_match.group(), 's_unit_name', s_unit_name)
                    #if s_unit_name != re_unit_match.group():
                    
                    s_dimension = ''.join([
                        s_dimension[:re_unit_match.start()],
                        s_unit,
                        "_",
                        s_unit_name,
                        s_dimension[re_unit_match.end():]])
            else:
                for re_unit_match in sorted(re_UOM_ABBR.finditer(s_dimension), key=position_sort_key, reverse=True):
                    if re_unit_match.group() == 'x': continue
                    s_unit_name = get_abbr_name(re_unit_match.group(), context=context)
                    
                    c_uoms.add(s_unit_name)
                    a_uoms.append(s_unit_name)
                    
                    #print(re_unit_match.group(), 's_unit_name', s_unit_name)
                    if s_unit_name != re_unit_match.group():
                        if re_unit_match.group() in {'"', "''", "'", "°", "”", "’"}:
                            #s_unit_name = ''.join(("_", s_unit_name))
                            s_dimension = ''.join([
                                s_dimension[:re_unit_match.start()],
                                "_",
                                s_unit_name,
                                s_dimension[re_unit_match.end():]])
                        else:
                            s_dimension = ''.join([
                                s_dimension[:re_unit_match.start()],
                                s_unit_name,
                                s_dimension[re_unit_match.end():]])
                
                    
                #print(re_unit_match, s_unit_name)
        #print('c_uoms', c_uoms)
        #print('a_uoms', a_uoms)
        if (b_multi_d or "_x_" in s_dimension) and len(a_uoms) > 1 and len(c_uoms) == 1:
            s_dimension = s_dimension.replace("_" + a_uoms[0], '', len(a_uoms) - 1)
            #print('b_multi_d', b_multi_d)
        return s_dimension

    def __repr__(self):
        return "<Dimension {}: '{}'>".format(self.pattern_type, self.string_match)

    def __str__(self):
        return self.string_match

    def __richcmp__(self, other, op_code):  # This is cython's __eq__ apparently
        if op_code == 2:  # equivalent to python's __eq__
            return self.string_match == other.string_match
        else:
            raise NotImplementedError("This comparison method is not implemented yet")


cdef class Dimensionizer(object):
    """
    @description: Holds methods to identify dimension patterns in documents, and turn
    those into single tokens. IE:
    27 inches ----> 27_inches  |  1/2 meters ------> 1/2_meters
    """
    cpdef public dict patterns
    
    def __init__(self):
        """
        @description: Precompile the known patterns on object initialization.
        """
        self.patterns = {}
        
        self.patterns['torque'] = [
            make_unit_regex_str("(lbs?f?\.?|pounds?)[-_ ]?(ins?\.?|inch(es)?|fts?\.?|f(ee|oo)ts?)", ""),
            make_metric_regex_str("newton[ -]?met(er|re)", "(N[ -]?m)"),
        ]
        
        inch = f"((\"|''|{UNIT_SEP}in{ABBR_BR}|{UNIT_SEP}inch(e?s)?{BR})|{UNIT_SEP}\((in{P_ABBR_BR}|inch(e?s)?)\))"
        foot = f"(('(?<!')|{UNIT_SEP}fts?{ABBR_BR}|{UNIT_SEP}f(ee|oo)ts?{BR})|{UNIT_SEP}\((fts?{P_ABBR_BR}|f(ee|oo)ts?)\))"
        yard = f"{UNIT_SEP}((yds?{ABBR_BR}|yards?{BR})|\((yds?{P_ABBR_BR}|yards?)\))"
        mile = f"{UNIT_SEP}((mis?{ABBR_BR}|miles?{BR})|\((mis?{P_ABBR_BR}|miles?)\))"
        #imperial = f"({inch}|{foot}|{yard}|{mile})"
        imperial = f"((\"|''?|{UNIT_SEP}(in{ABBR_END}|(ins|ft|yd|mi)s?{ABBR_BR})|{UNIT_SEP}(inch(es)?|f(ee|oo)ts?|yards?|miles?){BR})|{UNIT_SEP}\(((in|ft|yd|mi)s?{P_ABBR_BR}|(inch(es)?|f(ee|oo)ts?|yards?|miles?))\))"
        
        between_inch = f"((\"|''|{UNIT_SEP}ins?{P_ABBR_BR}|{UNIT_SEP}inch(es)?)|{UNIT_SEP}\((in{P_ABBR_BR}|inch(es)?)\))"
        between_foot = f"(('(?<!')|{UNIT_SEP}fts?{P_ABBR_BR}|{UNIT_SEP}f(ee|oo)ts?)|{UNIT_SEP}\((fts?{P_ABBR_BR}|f(ee|oo)ts?)\))"
        between_yard = f"{UNIT_SEP}((yds?{P_ABBR_BR}|yards?)|\((yds?{P_ABBR_BR}|yards?)\))"
        between_mile = f"{UNIT_SEP}((mis?{P_ABBR_BR}|miles?)|\((mis?{P_ABBR_BR}|miles?)\))"
        #between_imperial = f"({between_inch}|{between_foot}|{between_yard}|{between_mile})"
        between_imperial = f"((\"|''?|{UNIT_SEP}(in|ft|yd|mi)s?{P_ABBR_BR}|{UNIT_SEP}(inch(es)?|f(ee|oo)ts?|yards?|miles?)|{UNIT_SEP}\(((in|ft|yd|mi)s?{P_ABBR_BR}|(inch(es)?|f(ee|oo)ts?|yards?|miles?))\)))"
        
        inch2 = f"{UNIT_SEP}((in[2²]{ABBR_BR}|inch(e?s)?[2²]{BR})|\((in{P_ABBR_BR}|inch(e?s)?)[2²]\))"
        foot2 = f"{UNIT_SEP}((fts?[2²]{ABBR_BR}|f(ee|oo)ts?[2²]{BR})|\((fts?{P_ABBR_BR}|f(ee|oo)ts?)[2²]\))"
        yard2 = f"{UNIT_SEP}((yds?[2²]{ABBR_BR}|yards?[2²]{BR})|\((yds?{P_ABBR_BR}|yards?)[2²]\))"
        mile2 = f"{UNIT_SEP}((mis?[2²]{ABBR_BR}|miles?[2²]{BR})|\((mis?{P_ABBR_BR}|miles?)[2²]\))"
        imperial2 = f"({inch2}|{foot2}|{yard2}|{mile2})"
        
        inch3 = f"{UNIT_SEP}((in[3³]{ABBR_BR}|inch(e?s)?[3³]{BR})|\((in{P_ABBR_BR}|inch(e?s)?)[3³]\))"
        foot3 = f"{UNIT_SEP}((fts?[3³]{ABBR_BR}|f(ee|oo)ts?[3³]{BR})|\((fts?{P_ABBR_BR}|f(ee|oo)ts?)[3³]\))"
        yard3 = f"{UNIT_SEP}((yds?[3³]{ABBR_BR}|yards?[3³]{BR})|\((yds?{P_ABBR_BR}|yards?)[3³]\))"
        mile3 = f"{UNIT_SEP}((mis?[3³]{ABBR_BR}|miles?[3³]{BR})|\((mis?{P_ABBR_BR}|miles?)[3³]\))"
        imperial3 = f"({inch3}|{foot3}|{yard3}|{mile3})"
        
        metric_abbr = f"([fpnμcdkmgt]ms?{ABBR_BR}|m{STRICT_ABBR_BR})"
        metric_name = f"(femto|pico|nano|milli|micro|centi|deci|deca|kilo|mega|giga|tera)?met(er|re)s?{BR}"
        metric = f"{UNIT_SEP}(({metric_abbr}|{metric_name})|\(({metric_abbr}|{metric_name})\))"
        
        a_volume_units = [
            #('lit(er|re)', 'l', True),
            ('cubic centimeter', 'cc', False),
            #('barrel', 'bbl', False),
            #('gallon', 'ga?l', False),
            #('quart', 'qt', False),
            #('pint', 'pt', False),
            ('(barrel|gallon|quart|pint|(table|tea)spoon)', '(bbl|ga?l|qt|pt|tb?sp)', False),
            ('cup', 'c', False),
            #('tablespoon', 'tbsp', False),
            #('teaspoon', 'tsp', False),
            #('(table|tea)spoon', 'tb?sp', False),
            ('fl(uid|\.)?[- ]ounce', 'fl\.?[ -]?oz', False)]
        self.patterns['volume'] = [
            f"{BEFORE}{NUMBER}{CUBIC}{imperial}",  # cubic
            f"{BEFORE}{NUMBER}{CUBIC}{metric}",  # cubic
            f"{BEFORE}{NUMBER}{imperial3}",  # area
            make_metric_regex_str("met(er|re)", "m", exp='[3³]'),
            make_metric_regex_str('lit(er|re)', 'l', b_tenth_hundredth=True, b_ten_hundred=True),
        ]
        self.patterns['volume'].extend([
            make_metric_regex_str(name, abbr)
            if add_prefix else
            make_unit_regex_str(name, abbr)
            for name, abbr, add_prefix in a_volume_units])
        
        self.patterns['area'] = [
            f"{BEFORE}{NUMBER}{SQUARE}({imperial}|{metric})",  # area
            f"{BEFORE}{NUMBER}{imperial2}",  # area
            make_metric_regex_str("met(er|re)", "m", exp='[2²]', b_tenth_hundredth=True),
            make_unit_regex_str("acre", "ac"),
        ]
        
        self.patterns['distance'] = [
            f"{BEFORE}({NUMBER}{between_imperial}{BETWEEN2})+{NUMBER}{imperial}",  # multidimensional with units between
            f"{BEFORE}{NUMBER}{metric}({BETWEEN2}{NUMBER}{metric})+",  # multidimensional with units between
            f"{BEFORE}{NUMBER}({BETWEEN2}{NUMBER})*({imperial}|{metric})",  # normal or multidimensional with units at end
        ]
        
        a_weight_units = [
            ('gram', '(g|gr|gm)', True, {'b_range': True}),
            ('ton(ne)?', 't', True, {'b_small': False, 'b_range': True}),
            ('(pound|ounce)', '(lb|oz)', False, {'b_range': True}),
        ]
        self.patterns['weight'] = [
            make_metric_regex_str(name, abbr, **e_kwargs)
            if add_prefix else
            make_unit_regex_str(name, abbr, **e_kwargs)
            for name, abbr, add_prefix, e_kwargs in a_weight_units]
        
        a_thickness_units = [
            ('mil', '', False),]
        self.patterns['thickness'] = [
            make_metric_regex_str(name, abbr)
            if add_prefix else
            make_unit_regex_str(name, abbr)
            for name, abbr, add_prefix in a_thickness_units]
        
        a_gauge_units = [
            ('gauge', '(awg|ga)', False),]
        self.patterns['gauge'] = [
            make_metric_regex_str(name, abbr, b_range=True)
            if add_prefix else
            make_unit_regex_str(name, abbr, b_range=True)
            for name, abbr, add_prefix in a_gauge_units]
        
        a_electric_units = [
            ('amp(ere)?', 'a', True),
            ('volt(age)?', 'v', True),
            ('watt(age)?', 'w', True),
            ('amp(ere)?hour', 'ah', True),]
        self.patterns['current'] = [
            make_metric_regex_str(name, abbr, b_range=True)
            if add_prefix else
            make_unit_regex_str(name, abbr, b_range=True)
            for name, abbr, add_prefix in a_electric_units]
        
        a_hertz_units = [
            ('hertz', 'hz', True),]
        self.patterns['hertz'] = [
            make_metric_regex_str(name, abbr)
            if add_prefix else
            make_unit_regex_str(name, abbr)
            for name, abbr, add_prefix in a_hertz_units]
        
        a_lumen_units = [
            ('candela', 'cd', True),
            #('lumen', 'lms', False),
            #('candlepower', 'cp', False),
            ('(lumen|candlepower)', '(lms|cp)', False),
        ]
        self.patterns['lumens'] = [
            make_metric_regex_str(name, abbr, b_range=True)
            if add_prefix else
            make_unit_regex_str(name, abbr, b_range=True)
            for name, abbr, add_prefix in a_lumen_units]
        
        a_outlet_units = [
            #('outlet', ''),
            #('gang', ''),
            #('port', ''),
            #('bay', ''),
            ('(outlet|gang|port|bay)', ''),
        ]
        self.patterns['number of outlets'] = [
            make_pack_regex_str(name, abbr)
            for name, abbr in a_outlet_units]

        a_pack_units = [
            #('pack', 'pk'),
            #('piece', 'pc'),
            #('cannister', 'can'),
            #('carton', 'ct'),
            #('crate', ''),
            #('set', ''),
            #('bag', 'bg'),
            #('rack', ''),
            #('case', 'cs'),
            ('(pack|piece|cannister|carton|crate|set|bag|rack|case)', '(pk|pc|can|ct|set|bg|rck|cs)'),
            (
                '((pack|piece|cannister|carton|crate|set|bag|rack|case)s?|(pk|pc|can|ct|set|bg|rck|cs)s?\.?)'
                '[/ _-]*'
                '((pack|piece|cannister|carton|crate|set|bag|rack|case)s?|(pk|pc|can|ct|set|bg|rck|cs)s?\.?)',
                ''),
        ]
        self.patterns['pack'] = [
            make_pack_regex_str(name, abbr)
            for name, abbr in a_pack_units]
        
        a_count_units = [
            #('count', 'cnt'),
            #('quantity', 'qty'),
            #('pair', 'pr'),
            #('gross', 'gr'),
            #('dozen', 'dz'),
            #('pill', ''),
            #('capsule', ''),
            #('tablet', ''),
            #('ply', ''),
            #('sheet', 'sht'),
            #('ream', 'rm'),
            #('roll', 'rl'),
            ('(count|quantity|pair|gross|dozen|each)', '(cnt|qty|pr|gr|dzn?|doz|ea)'),
            ('(pill|capsule|tablet|ply|sheet|ream|roll)', '(tab|sht|rm|rl)'),
        ]
        self.patterns['count'] = [
            make_pack_regex_str(name, abbr)
            for name, abbr in a_count_units]

        a_memory_units = [
            ('(byte|bit)', 'b'),]
        self.patterns['memory'] = [
            make_metric_regex_str(name, abbr, b_small=False)
            for name, abbr in a_memory_units]

        self.patterns['time'] = [
            make_metric_regex_str("second", "sec", b_large=False),
            f"{BEFORE}{NUMBER}{UNIT_SEP}((min|hr|dy|mon|yr)s?{ABBR_BR}|(minute|hour|day|month|year)s?{BR})",
            #r"((\d+,)*\d+(\.\d+)?[ -])?(\d+,)*\d+(\.\d+)?(\s*| *- *)(second|minute|hour|day|month|year)s?",
            #r"((\d+,)*\d+(\.\d+)?[ -])?(\d+,)*\d+(\.\d+)?(\s*| *- *)(sec|min|hr|dy|mon|yr)s?(\.|\b|$)"
        ]
        
        self.patterns['carat'] = [
            f"{BEFORE}{NUMBER}{UNIT_SEP}((karat|carat)s?{BR}|k{STRICT_ABBR_BR}|(kt|c|c[tw]|c?tw|cwt|cttw|dt?w|tdw)s?{ABBR_BR})",
        ]
        
        self.patterns['size'] = [
            r"(^|\b)(size|width|height|length|depth|diameter|thickness){UNIT_SEP}{NUMBER}",
            r"(^|\b)(ring|band|shoe){UNIT_SEP}{NUMBER}",
        ]
        
        self.patterns['horsepower'] = [
            f"{BEFORE}({NUMBER}[ -]*)?{NUMBER}{UNIT_SEP}(hps?{ABBR_BR}|horsepowers?{BR})",
        ]
        
        self.patterns['energy'] = [
            make_metric_regex_str("joule", "j"),
            make_unit_regex_str("(ins?\.?|inch(es)?|fts?\.?|f(ee|oo)ts?)[-_ ]?(lbs?\.?|pounds?)", ""),
        ]
        
        self.patterns['pressure'] = [
            f"{NUMBER}{UNIT_SEP}(psia?{ABBR_BR}|((lbs?{P_ABBR_BR}|pounds?) *per *(sq?\.?|square) *(ins?{P_ABBR_BR}|inch(es)?)))",
            make_metric_regex_str("pascal", "pa"),
        ]
        
        self.patterns['decibel'] = [
            make_unit_regex_str("decibel", "db"),
        ]
        
        self.patterns['degree'] = [
            make_unit_regex_str("(degree|°)[fck]?", "deg"),
            make_unit_regex_str("celsius", "c"),
            make_unit_regex_str("fahren(heit)?", "f"),
            make_unit_regex_str("kelvin", "k"),
        ]
        
        self.patterns['flow'] = [
        #    make_unit_regex_str("(degree|°)[fck]?", "deg"),
        #    make_unit_regex_str("celsius", "c"),
        #    make_unit_regex_str("fahren(heit)?", "f"),
        #    make_unit_regex_str("kelvin", "k"),
        ]
        
        ##### Compile patterns into regexes #####
        self.patterns = {
            s_unit: self._compile_patterns(a_patterns)
            for s_unit, a_patterns in self.patterns.items()
        }
        
    def _compile_patterns(self, patterns_list):
        """
        @description: compiles a list of raw string patterns
        @arg: {list} patterns_list: The list of patterns to compile.
        @return: {list} the compiled list of patterns
        """
        #return PatternsList([
        #    re.compile(pattern, re.IGNORECASE) for pattern in patterns_list
        #])
        return [re.compile(pattern, re.IGNORECASE) for pattern in patterns_list]

    def tokenize_dimensions(self, document, b_normalize=True, b_replace_abbr=False):
        """
        @description: given a document, turns into a single token all the
        token groups that match the known product dimensions patterns.
        @arg document: {str} the document to look for dimension patterns in.
        @return: {str} The document with dimension patterns tokenized together.
        """
        cdef str s_doc, s_unit
        cdef list a_pattern_group, dimensions, matches
        dimensions = []

        c_chars = set(document)
        s_doc = document.lower()
        #c_document = set(s_doc.split())
        #if (set(c_document) & set(c_COUNTS)):
        
        if any(num in c_chars for num in c_COUNT_DIGITS) or any([num in s_doc for num in c_COUNTS]):
            #print("has num, check patterns")
            for s_unit, a_pattern_group in self.patterns.items():
                document, matches = self._tokenize_patterns(
                    document,
                    s_unit,
                    a_pattern_group,
                    b_normalize=b_normalize,
                    b_replace_abbr=b_replace_abbr,)
                dimensions.extend(matches)

        return document, dimensions

    cpdef _tokenize_patterns(self, str document, str s_unit, list a_pattern_group, bint b_normalize=True, bint b_replace_abbr=False):
        """
        @description: given a document and a list of patterns, searches for
        groups of tokens matching the patterns on the list, and concatenates
        them into a single token by replacing spaces with underscores.
        @arg document: {str} the document to tokenize
        @arg pattern_group: {list} The list of patterns to search for in the document.
        @return: {str} the document original document, with all their tokens matching
        the patterns on the list, concatenated via underscores.
        """
        cdef list a_matches
        cdef str s_match, s_clean_match
        cdef list matches = []
        cdef set c_tokens
        
        if b_replace_abbr:
            c_tokens = set(document.lower().split())
        else:
            c_tokens = None
            
        for idx, re_pattern in enumerate(a_pattern_group):
            a_matches = sorted(
                list(re_pattern.finditer(document)),
                key=position_sort_key,
                reverse=True)
            
            #print(s_unit, idx, 'matches:', len(a_matches))
            
            for re_match in a_matches:
                #print(re_match, re_match.start(), re_match.end())
                s_match = document[re_match.start(): re_match.end()]
                o_TokRes = TokenizationResult(
                    re_match, s_match, "{}_{}".format(s_unit, idx),
                    re_match.start(), re_match.end()
                )
                matches.append(o_TokRes)
                
                if b_normalize:
                    s_clean_match = o_TokRes._normalize(b_replace_abbr=b_replace_abbr, context=c_tokens)
                    document = ''.join((document[:re_match.start()], s_clean_match, document[re_match.end():]))
                else:
                    s_clean_match = s_match.replace(" ", "_")
                    document = document.replace(s_match, s_clean_match)

        return document, matches
