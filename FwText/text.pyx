﻿from __future__ import absolute_import
import os, re, string, sys
from operator import itemgetter
import numpy as np
from .html_characters import get_html_char_codes
from .font_widths import get_text_pixel_width
from .cleaning import *
from .plural import *
from .tokenize import *
from .ngrams import *
from .search import *
from .dimension_abbreviations import *
from .dimensions import *

__all__ = [
    "sort_tokens_by_length",
    "unique_subset_strings", "unique_regex_results",
    #"appendRelativePositionToTokens",
    #"extractNLTKStems",
    "extract_nltk_stems",
    "is_alpha_numeric", "has_alpha_numeric",
    "edit_distance", "text_differences",
    "remove_doc_substring",
    "get_quoted_phrases", "get_quoted_phrases_and_pos", "get_quoted_phrase_words", 
    "c_dimension_names", "e_dimension_names", #old
    "SRE_MATCH_TYPE", "NLTK_STEMMER",
    
    # from search.pyx
    "word_indexes", "pos_contained_in_spans", "pos_overlaps_in_spans",
    "merge_overlapped_spans", "merge_adjacent_spans",
    "word_in_text", "word_instr", "find_char_sequence", "find_word_sequence", "word_in_set",
    "get_token_spans", "get_consecutive_spans",
    "get_phrase_position", "get_all_phrase_positions",
    "get_proximities", "proximity", "get_nearest_proximity", "get_sentence_proximity",
    "get_tokens_between",
    "are_two_substrings_consecutive", "are_consecutive_ordered", "are_substrings_consecutive",
    
    # from tokenize.pyx
    "title_tokenize", "description_tokenize",
    "word_tokenize", "sentence_tokenize",
    "split_strip", "split_words",
    "split_camelcase", "split_number_uom", "split_edge_punctuation", "split_edge_quotes",
    "split_on_edge_punctuation",
    "join_oxford", "join_sentence",
    "a_TITLES", "a_ABBREV",
    
    # from ngrams.pyx
    "all_ngrams", "all_ngrams_lists", "all_ngrams_flat", "all_ngrams_tuples",
    "end_grams", "skip_bigrams", "skip_grams", "char_ngrams",
    "text_list_to_ngram_list", "ngram_freq_coverage_in_list",
    "ngram_freq_in_list", "tuple_ngram_freq_in_list",
    "ngram_coverage_in_list", "tuple_ngram_coverage_in_list",
    "ngram_table", "get_ngram_count", "combine_plural_singular_stems",
    "is_compound_word", "compound_word_in_ngrams", "get_compounds",
    "freq_dict_to_lower",
    "insert_phrase", "unique_ngrams_to_phrase",
    
    # from plural.pyx
    "all_plural_singular", "pluralize", "singularize",
    "pluralize_tuple", "singularize_tuple",
    
    # from cleaning.pyx
    "trim", "remove_hyphens", "remove_apostrophes", "remove_possessives",
    "remove_periods", "fix_sentence_periods",
    "replace_ampersands", "replace_with", "remove_trademarks", "remove_slashes",
    "remove_html_characters", "replace_html_characters",
    "strip_unicode_accents", "strip_quotes",
    "clean_text", "clean_title", "clean_description",
    "clean_group_of_dicts", "clean_list", "remove_all_punctuation", "remove_stopwords",
    
    "s_UTF8_PUNCTUATION", "re_ALPHANUM", "re_ALPHA", "re_NUM",
    "re_PUNCT", "re_PUNCT_NOSPACE", "re_PUNCT_NOPERIOD", "re_PUNCT_NOSPACE_NOPERIOD",
    "re_PUNCT_WITH_EXCEPT", "re_PUNCT_UTF",
    "re_APOSTROPHE", "re_SPACES", "re_WHITESPACE", "re_DIGITS",
    "re_MISC", "re_MPN",
    
    # from dimension_abbreviations
    'e_ABBR_NAMES', 'e_CONTEXTS',
    
    # from dimensions
    "TokenizationResult", "Dimensionizer",
    "make_unit_regex_name", "make_unit_regex_str",
    "make_metric_regex_name", "make_metric_regex_str",
    "make_count_regex_str", "make_pack_regex_str",
    "get_abbr_names", "get_abbr_name",
    "a_count_digits", "a_count_words", "c_dimension_names",
]
 
SRE_MATCH_TYPE = type(re.match("", ""))

s_UTF8_PUNCTUATION = r"!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~¡¢£¤¥¦§¨ª«¬¯°±´µ¶·¹º»¿×÷∞"
re_ALPHANUM = re.compile(r"[A-Z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚]+", flags=re.I)
re_ALPHA = re.compile(r"[A-Z]+", flags=re.I)
re_NUM = re.compile(r"[0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚]+")
re_PUNCT = re.compile(r"[^A-Z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚]+", flags=re.I)
re_PUNCT_NOSPACE = re.compile(r"[^A-Z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚&' \t]+", flags=re.I)
re_PUNCT_NOPERIOD = re.compile(r"[{}]+".format(''.join(
    [re.escape(s_char) for s_char in list(s_UTF8_PUNCTUATION)
     if s_char not in "'./-&"]
)), flags=re.I)
re_PUNCT_NOSPACE_NOPERIOD = re.compile(r"[^A-Z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚&'./ \t]+", flags=re.I)
re_WHITESPACE = re.compile(r"[ \t\r\n]+", flags=re.I)
re_STR_PUNCT = re.compile("r[" + re.escape(''.join([x for x in string.punctuation if x not in "'-_"])) + "]+")
s_QT = '"'
re_TRADEMARK_SYMBOLS = re.compile(r"([™®©]|(?<! [A-Z])[T][Mm](?=[ .,?!():;" + s_QT + "\r\n-]|'[Ss]|$))")
re_TRADEMARK_WORDS = re.compile(r"(trademark|registered|copyright|&trade;|&reg;|&copy;)", flags=re.I)

c_WORD_SEP_CHARS = set("'`~-_\"/\:@+")   #"#$%^&*()=,.<>?:|{}[]")

re_MISC = re.compile("(tm +|\(tm\)|\(registered\)|\(r\)|'s(?= |$)|(?<=s)'(?= |$))", flags=re.I)
re_DIGITS = re.compile("\d")
a_MPN_PATTERNS = [
    r"#?[a-z]{2,}-?0+(-?[a-z]+0+)+",  # mpn_1
    r"#?[a-z]{2,}(-?0+-?[a-z]+)+",  # mpn_2
    r"#?[a-z]{2,}(-?0+[a-z]+)+",  # mpn_3
    r"#?[a-z]{2,}-?0+",  # mpn_4
    r"#?0{4,}",  # mpn_5
    r"#?0{2,}-?([a-z]{3,}-?|[a-z]+)0+(-?([a-z]+|0+))*",  # mpn_6
    r"#?0{2,}-?[a-z]{3,}",  # mpn_7
    r"[a-z]+0+([a-z]+0+)+",  # mpn_8
    # r"0+\.0+(\.0+)+"  # mpn_9
    r"0+(\.0+){2,}"  # mpn_9
]

re_MPN = re.compile("({})".format('|'.join(a_MPN_PATTERNS)), flags=re.I)
re_PUNCT_UTF = re.compile(u"[+(),;" + b'\xc2\xa9\xc2\xae\xe2\x80\xa2\xe2\x84\xa2'.decode("utf8") + "]")
re_APOSTROPHE = re.compile("['`" + b'\xe2\x80\x99\xc2\xb4'.decode("utf8") + "]")
re_SPACES = re.compile(" {2,}")

cdef set c_ASCII = set(
    [s for s in
     r'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,?!@#$%^&*()-_+=";:<>[]{}\|/']
)
cdef set c_DIGITS = set("0123456789½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚")
cdef set c_UPPER = set("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
cdef set c_LOWER = set("abcdefghijklmnopqrstuvwxyz")
cdef set c_ALPHANUM = set(list(c_UPPER) + list(c_LOWER) + list(c_DIGITS))
cdef set c_LOWERNUM = set(list(c_LOWER) + list(c_DIGITS))

re_PUNCT_WITH_EXCEPT = re.compile("[{}]+".format(''.join(
    [re.escape(s_char) for s_char in s_UTF8_PUNCTUATION if s_char not in "- .'&_%"]
)), flags=re.I)

e_dimension_names = c_dimension_names  #old name

    
def sort_tokens_by_length(a_matches, b_descending=True, index=0):
    ''' sort a list-tuple of tokens/regex matches/lists-tuples or re matches by their lengths
        Arguments:
            a_matches: {list} of either regex match objects, or text tokens, or lists, or tuples
            b_descending: {bool} return in descending order of length
            index: {int} which element to use if list items are lists or tuples
        Returns:
            {list} of the same items in order by length
    '''
    if not a_matches: return a_matches
    if isinstance(a_matches, set): a_matches = tuple(a_matches)
    
    if isinstance(a_matches[0], SRE_MATCH_TYPE):
        # if list contains re match objects
        texts,lengths = zip(
            *sorted([(match, match.end() - match.start()) for match in a_matches],
                    key=itemgetter(1),
                    reverse=b_descending)
        )
    elif isinstance(a_matches[0], (list, tuple)):
        # if list contains lists-tuples
        texts,lengths = zip(
            *sorted([(element, len(element[index])) for element in a_matches],
                    key=itemgetter(1),
                    reverse=b_descending)
        )
    else:
        # if list contains strings
        texts,lengths = zip(
            *sorted([(text, len(text)) for text in a_matches],
                    key=itemgetter(1),
                    reverse=b_descending)
        )
        
    if isinstance(a_matches, list): texts = list(texts)
    elif isinstance(a_matches, tuple): texts = tuple(texts)
    return texts


cpdef list unique_subset_strings(a_strings, a_tokenized_text, b_case_insensitive=True):
    ''' filter short regex results that only appear inside of found larger results.
        This is to be used when the list of results comes from a combination of
        separate regexes.
        For example,
            if a product title was
                "Web exclusive brandnew red doll"
            and the found regex phrases were
                ['Web exclusive', 'exclusive']
            then exclusive should only be included as part of "web exclusive"
            
            but if a product title was
                "Web exclusive brandnew red exclusive doll"
            then exclusive should also be listed be included as part of "web exclusive"
        
        This is done on a tokenized word basis with ngrams
        Arguments:
            a_strings: {list} of string matches found
            a_tokenized_text: {list} of tokenized text
            b_case_insensitive: {bool} whether to do case insensitive matching between a_strings and a_tokenized_text
        Returns:
            {list} of unique string matches
    '''
    if not a_tokenized_text: return a_strings
    
    # get the ngrams of the words in the text to be turned into frequencies
    a_ngrams = all_ngrams_flat(
        [word.lower() for word in a_tokenized_text]
    )
    cdef dict e_ngrams = {}
    cdef str s_ngram
    for s_ngram in a_ngrams:
        if s_ngram in e_ngrams:
            e_ngrams[s_ngram] += 1
        else:
            e_ngrams[s_ngram] = 1
            
    # sort the results in descending order by length
    a_strings = sort_tokens_by_length(a_strings, b_descending=True)
    
    # filter down to the ones that are not completely contained within another
    a_filter = []
    for s_result in a_strings:
        if not s_result: continue
        a_result_ngrams = all_ngrams_flat(
            word_tokenize(
                s_result, remove_punct=False, stop_words=None, return_lower=True
            )
        )
        if e_ngrams.get(a_result_ngrams[0], 0) == 0: continue
        a_filter.append(s_result)
            
        for ngram in a_result_ngrams:
            if not ngram in e_ngrams: continue
            i_remaining_freq = e_ngrams.get(ngram,0)
            e_ngrams[ngram] -= 1
            
    return a_filter


cpdef list unique_regex_results(a_regexes, str s_text):
    ''' filter short regex results that only appear inside of found larger results.
        This is to be used when the list of results comes from a combination of
        separate regexes.
        For example,
            if a product title was
                "Web exclusive brandnew red doll"
            and the found regex phrases were
                ['Web exclusive', 'exclusive']
            then exclusive should only be included as part of "web exclusive"
            
            but if a product title was
                "Web exclusive brandnew red exclusive doll"
            then exclusive should also be listed be included as part of "web exclusive"
        
        This is done on a string basis
        Arguments:
            a_regexes: {list} of regex matches found
            s_text: {str} of text that the regex matches came from
        Returns:
            {list} of unique string matches
    '''
    # Get the matches for each regex
    cdef list a_matches = []
    for o_re in a_regexes:
        for o_match in o_re.finditer(s_text):
            a_matches.append(o_match)
    
    #sort matches in descending order by length
    a_matches = sort_tokens_by_length(a_matches)
    
    # filter down to the ones that are not completely contained within another
    cdef list a_spans = []
    for o_match in a_matches:
        if pos_contained_in_spans(o_match.span(), a_spans): continue
        a_spans.append(o_match.span())
    
    # turn spans into strings
    cdef tuple t_span
    as_unique = [s_text[t_span[0]:t_span[1]] for t_span in a_spans]
    return as_unique


# Consider removing this
#cpdef appendRelativePositionToTokens(doc, int decimal_places=1, stop_words="english"):
#    """ This function concatenates each token with its relative position within the string.
#        The decimal_places argument states the number of decimal digits to use
#        in the rounding of the relative position.
#        It can be passed as preprocessor argument to scikit models. """
#    cdef list tokens
#    cdef float tokens_length
#    if type(doc) != list:
#        tokens = word_tokenize(doc, stop_words=stop_words)
#    else:
#        tokens = doc
#    tokens_length = float(len(tokens))
#    return " ".join([
#        "{}{}".format(
#            t,  # Concatenate each token
#            # With its relative position within the string
#            int(100 * round(float(tokens.index(t)) / tokens_length, decimal_places))
#        )
#        for t in tokens
#    ])


cpdef extract_nltk_stems(doc, stop_words="english"):
    """Turns every token in the given doc, into its stem form."""
    return " ".join([
        NLTK_STEMMER(token)
        for token in word_tokenize(doc, stop_words=stop_words)
    ])


# Deprecate this function, in favor of its consistently named version
#cpdef extractNLTKStems(doc, stop_words="english"):
#    return extract_nltk_stems(doc, stop_words=stop_words)


#Cython and Spacy aren't playing along. Look into it.
#def generateSpacyLemmasExtractor():
#    """Loading spacy takes alot of time and memory. This function prevents
#    loading spacy every time this module is imported.
#    There's probably a clenear workaround, turning ExtractSpacyLemmas into a class
#    that contains loads spacy on __init__, and making use of the __call__ magic method."""
#    spacy_singleton = spacy.load("en")
#    def extractSpacyLemmas(cod):
#        """This function turns every token in the document into its lemma. """
#        return " ".join([
#            token.lemma_ for token
#            in spacy_singleton(doc.encode('ASCII', 'ignore').decode('ASCII').lower())
#        ])

#    return extractSpacyLemmas


cpdef bint is_alpha_numeric(str s_string):
    if re_NUM.search(s_string) and re_ALPHA.search(s_string):
        return True
    else:
        return False


cpdef bint has_alpha_numeric(str s_string):
    if re_ALPHANUM.search(s_string):
        return True
    else:
        return False
    
cpdef int edit_distance(str s_1, str s_2, bint b_case_insensitive=False):
    ''' returns: character levenshtein distance '''
    cdef int i_len1, i_len2, x, y, i_x, i_y
    cdef str s_x, s_y
    
    if s_1 == s_2: return 0
    i_len1, i_len2 = len(s_1), len(s_2)
    if i_len1 == 0: return i_len2
    if i_len2 == 0: return i_len1
    
    np_dist = np.zeros([i_len1 + 1, i_len2 + 1], dtype=int)
    
    if b_case_insensitive:
        s_1, s_2 = s_1.lower(), s_2.lower()
      
    np_dist[:, 0] = np.array(range(i_len1 + 1), dtype=int)
    np_dist[0, :] = np.array(range(i_len2 + 1), dtype=int)
    
    for i_x, s_x in enumerate(s_1):
        x = i_x + 1
        for i_y, s_y in enumerate(s_2):
            y = i_y + 1
            if s_x == s_y:
                np_dist[x, y] = np_dist[x - 1, y - 1]
            else:
                np_dist[x, y] = 1 + min(np_dist[x - 1, y], np_dist[x, y - 1], np_dist[x - 1, y - 1])

    return np_dist[-1, -1]


cpdef dict text_differences(str s_text1, str s_text2):
    ''' Compare two strings. '''
    cdef str s_alphanum1, s_alphanum2, s_alpha1, s_alpha2, s_num1, s_num2, s_punct1, s_punct2
    cdef dict e_diff = {}
        
    if s_text1 == s_text2:
        return {}
    elif s_text1.lower() == s_text2.lower():
        return {'case': edit_distance(s_text1, s_text2)}
    else:
        s_alphanum1, s_alphanum2 = ''.join(re_ALPHANUM.findall(s_text1)), ''.join(re_ALPHANUM.findall(s_text2))
        if s_alphanum1 == s_alphanum2:
            return {'punctuation': edit_distance(s_text1, s_text2)}
        
        s_alpha1, s_alpha2 = ''.join(re_ALPHA.findall(s_alphanum1)), ''.join(re_ALPHA.findall(s_alphanum2))
        s_num1, s_num2 = ''.join(re_NUM.findall(s_alphanum1)), ''.join(re_NUM.findall(s_alphanum2))
        s_punct1, s_punct2 = ''.join(re_PUNCT.findall(s_text1)), ''.join(re_PUNCT.findall(s_text2))
        
        if s_alpha1.lower() != s_alpha2.lower():
            e_diff['letters'] = edit_distance(s_alpha1, s_alpha2)
        elif s_alpha1 != s_alpha2:
            e_diff['case'] = edit_distance(s_alpha1, s_alpha2)
        
        if s_num1 != s_num2:
            e_diff['numbers'] = edit_distance(s_num1, s_num2)
            
        if s_punct1 != s_punct2:
            e_diff['punctuation'] = edit_distance(s_punct1, s_punct2)
            
        return e_diff


def remove_doc_substring(a_doc, a_substring=[]):
    ''' remove a tokenized substring from a tokenized doc, for example, remove a brand from a title '''
    if isinstance(a_doc, tuple):
        a_doc = list(a_doc)
        b_return_tuple = True
    else:
        b_return_tuple = False

    t_substring_pos = get_phrase_position(a_doc, a_substring)
    if t_substring_pos:
        for i_pos in reversed(range(t_substring_pos[0], t_substring_pos[1])):
            a_doc.pop(i_pos)
    
    if b_return_tuple:
        return tuple(a_doc)
    else:
        return a_doc
    

cpdef list get_quoted_phrases(text, bint use_apostrophes=False):
    ''' Get the quoted phrases from a text.
        The text may be a flat string,
        or
        tokenized still including quotes
        where quotes may either be individual tokens,
        or be at the beginning or end of word tokens.
        
        Quotes directly after a number will not start a phrase. '''
    cdef list a_phrases = []
    cdef bint b_phrase = False
    cdef bint b_apostrophe_phrase = False
    cdef str s_word, s_letter
    cdef list a_letters = []
    cdef int index, i_end, i_len
    
    if isinstance(text, (list, tuple)):  # if input is tokenized
        i_end = len(text) - 1
        for index, s_word in enumerate(text):
            i_len = len(s_word)
            if b_phrase:  # continue or end a phrase
                if not b_apostrophe_phrase:
                    if s_word == '"' or s_word[0] == '"':
                        b_phrase = False
                    elif s_word[-1] == '"':
                        b_phrase = False
                        a_phrases[-1].append(s_word[:-1])
                    else:
                        a_phrases[-1].append(s_word)
                    
                elif b_apostrophe_phrase:
                    if s_word == "'" or s_word[0] == "'":
                        b_phrase = False
                    elif s_word[-1] == "'":
                        b_phrase = False
                        a_phrases[-1].append(s_word[:-1])
                    else:
                        a_phrases[-1].append(s_word)

            else:  # start phrase or skip word
                if use_apostrophes:
                    if s_word == "'":
                        b_phrase, b_apostrophe_phrase = True, True
                        a_phrases.append([])
                    elif s_word[0] == "'" and s_word[-1] == "'":
                        a_phrases.append([s_word[1:-1]])
                    elif s_word[0] == "'" and (i_len > 5 or (i_len > 1 and not s_word[1].islower())):
                        b_phrase, b_apostrophe_phrase = True, True
                        a_phrases.append([s_word[1:]])
                    elif s_word[-1] == "'":
                        #don't start a phrase if the quote is directly after a number
                        if i_len < 2 or (s_word[-2] not in c_DIGITS and s_word[-2] != 's'):
                            b_phrase, b_apostrophe_phrase = True, True
                            a_phrases.append([])
                    else:
                        pass
                        
                if not b_phrase:   
                    if s_word == '"':
                        b_phrase, b_apostrophe_phrase = True, False
                        a_phrases.append([])
                    elif s_word[0] == '"' and s_word[-1] == '"':
                        a_phrases.append([s_word[1:-1]])
                    elif s_word[0] == '"':
                        b_phrase, b_apostrophe_phrase = True, False
                        a_phrases.append([s_word[1:]])
                    elif s_word[-1] == '"' and index < i_end:
                        #don't start a phrase if the quote is directly after a number
                        if i_len < 2 or s_word[-2] not in c_DIGITS:
                            b_phrase, b_apostrophe_phrase = True, False
                            a_phrases.append([])
                    else:
                        pass
                        

        return [x for x in a_phrases if x]
    
    else:  # if input is string
    
        # TODO: THIS COULD BE OPTIMIZED BY APPENDING PHRASES AT A TIME INSTEAD OF INDIVIDUAL LETTERS
        i_end = len(text) - 1
        for index, s_letter in enumerate(text):
            if b_phrase:  # continue or end a phrase
                if index == i_end: break
                if not b_apostrophe_phrase and s_letter == '"':
                    b_phrase = False
                elif (
                    b_apostrophe_phrase and s_letter == "'" and
                    (index == 0 or (text[index - 1] not in c_DIGITS and text[index - 1] != 's')) and
                    (index == i_end or (text[index + 1] != 's'))
                ):
                    b_phrase = False
                else:
                    a_phrases[-1].append(s_letter)
            else:  # start phrase or skip letter
                if s_letter == '"' and (index == 0 or text[index - 1] not in c_DIGITS):
                    #don't start a phrase if the quote is directly after a number
                    b_phrase, b_apostrophe_phrase = True, False
                    a_phrases.append([])
                elif (
                    use_apostrophes and s_letter == "'" and
                    ((not text[index + 1].islower()) and
                     (index == 0 or (text[index - 1] not in c_DIGITS and text[index - 1] != 's')))
                ):
                    #don't start a phrase if the quote is directly after a number
                    b_phrase, b_apostrophe_phrase = True, True
                    a_phrases.append([])
                else:
                    pass

        return [''.join(a_letters).strip() for a_letters in a_phrases if a_letters]


cpdef set get_quoted_phrase_words(text, bint use_apostrophes=False):
    ''' Get a set of words that appear in quoted phrases in the text. '''
    cdef str s_word
    cdef list a_phrase
    cdef list a_phrases = get_quoted_phrases(text, use_apostrophes=use_apostrophes)
    if isinstance(text, str):
        a_phrases = [split_strip(phrase) for phrase in a_phrases if phrase]
        
    return set([s_word for a_phrase in a_phrases for s_word in a_phrase])

        
cdef set c_NUM_QUOTES = {"'", '"'}
cdef dict e_QUOTE_START_ENDS = {'"': '"', "'": "'"}


cpdef list get_quoted_phrases_and_pos(text, dict e_start_ends=None):
    ''' Get the quoted phrases from a text.
        The text may be a flat string,
        or
        tokenized still including quotes
        where quotes may either be individual tokens,
        or be at the beginning or end of word tokens.
        
        Quotes directly after a number will not start a phrase.
        
        return a list of tuples of phrases and positions
    '''
    cdef list a_phrases, a_phrase_pos, a_letters
    cdef str s_word, s_letter, s_end_char
    cdef int idx, i_end, i_len, i_start
    cdef bint b_phrase = False
    a_phrases, a_phrase_pos = [], []
    if e_start_ends is None: e_start_ends = e_QUOTE_START_ENDS
    i_start = 0
    
    if isinstance(text, (list, tuple)):  # if input is tokenized
        i_end = len(text) - 1
        for idx, s_word in enumerate(text):
            i_len = len(s_word)
            if b_phrase:  # continue or end a phrase
                if s_word[0] == s_end_char: #s_word == s_end_char or :
                    b_phrase, s_end_char = False, ''
                elif s_word[-1] == s_end_char:
                    b_phrase, s_end_char = False, ''
                    a_phrases[-1].append(s_word[:-1])
                    a_phrase_pos[-1][1] = idx + 1
                else:
                    a_phrases[-1].append(s_word)
                    a_phrase_pos[-1][1] = idx + 1
                    
            else:  # start phrase or skip word
                if s_word[0] in e_start_ends:
                    if i_len == 1:  # single character
                        b_phrase, s_end_char = True, e_start_ends[s_word]
                        a_phrases.append([])
                        i_start = idx + 1
                        a_phrase_pos.append([i_start, i_start])
                    elif s_word[-1] == e_start_ends[s_word[0]]:  # word in quotes
                        a_phrases.append([s_word[1: -1]])
                        a_phrase_pos.append((idx, idx + 1))
                    else:  # word is start
                        b_phrase, s_end_char = True, e_start_ends[s_word[0]]
                        a_phrases.append([s_word[1:]])
                        a_phrase_pos.append([idx, idx + 1])
                elif s_word[-1] in e_start_ends and s_word[-1] in c_NUM_QUOTES and idx < i_end:
                    # don't start a phrase if the quote is directly after a number
                    if s_word[-2] not in c_DIGITS and s_word[-2] != 's':
                        b_phrase, s_end_char = True, e_start_ends[s_word[-1]]
                        a_phrases.append([])
                        i_start = idx + 1
                        a_phrase_pos.append([i_start, i_start])
                else:
                    pass
                        
        return [(tuple(phr), tuple(pos))
                for phr, pos in zip(a_phrases, a_phrase_pos)
                if phr and pos[0] < pos[1]]
    
    else:  # if input is raw string
        a_letters = []
        i_end = len(text) - 1
        for idx, s_letter in enumerate(text):
            if b_phrase:  # continue or end a phrase
                if s_letter == s_end_char:
                    if (s_letter in c_NUM_QUOTES and 
                        ((text[idx - 1] in c_DIGITS) or 
                        (s_letter == "'" and idx < i_end and text[idx + 1] == 's'))
                    ):
                        pass
                    else:
                        b_phrase, s_end_char = False, ''
                        a_phrases.append(text[i_start: idx])
                        a_phrase_pos.append((i_start, idx))
                    
            else:  # start phrase or skip letter
                if s_letter in e_start_ends:
                    s_end_char = e_start_ends[s_letter]
                    if s_letter in c_NUM_QUOTES and idx > 0 and text[idx - 1] in c_DIGITS:
                        #don't start a phrase if the quote is directly after a number
                        s_end_char = ''
                    elif (
                        s_letter == "'" and idx > 0 and  idx < i_end and
                        ((text[idx - 1] == 's' and text[idx + 1] == ' ') or
                         (text[idx - 1] != ' ' and text[idx + 1] != ' ') or
                         (idx < i_end - 2 and text[idx - 1] == ' ' and text[idx + 1] == 'n' and text[idx + 2] == ' '))
                    ):
                        #don't start a phrase if possessive
                        s_end_char = ''
                    else:
                        b_phrase, i_start = True, idx + 1
                        #a_phrase_pos.append((i_start, i_start))
            
        if b_phrase and i_start < i_end:
            a_phrases.append(text[i_start: ])
            a_phrase_pos.append((i_start, i_end))
        return list(zip(a_phrases, a_phrase_pos))
