﻿from __future__ import absolute_import
from .tokenize import *
from .plural import *

__all__ = [
    "all_ngrams", "all_ngrams_lists", "all_ngrams_flat", "all_ngrams_tuples",
    "end_grams", "skip_bigrams", "skip_grams", "char_ngrams",
    "text_list_to_ngram_list", "ngram_freq_coverage_in_list",
    "ngram_freq_in_list", "tuple_ngram_freq_in_list",
    "ngram_coverage_in_list", "tuple_ngram_coverage_in_list",
    "ngram_table", "get_ngram_count", "combine_plural_singular_stems",
    "is_compound_word", "compound_word_in_ngrams", "get_compounds",
    "freq_dict_to_lower",
    "insert_phrase", "unique_ngrams_to_phrase",
    
    # for use with FwUtility.filter_kwargs
    "c_SKIP_GRAM_KWARGS", "c_ALL_NGRAMS_TUPLES_KWARGS",
]

c_SKIP_GRAM_KWARGS = {
    'i_max_size', 'i_min_size',
    'i_max_skip', 'b_sort_skips', 'b_skip_ngrams_only',
    'stop_words', 'remove_punct'}
c_ALL_NGRAMS_TUPLES_KWARGS = {
    'i_max_size', 'i_min_size', 'stop_words', 'remove_punct'}


cpdef list all_ngrams(text, int i_max_size=6, int i_min_size=1,
                      stop_words="english", remove_punct=True):
    ''' Returns a list of lists of ngrams from longest to shortest with sub-lists from first to last.'''
    cdef list a_words, a_all_ngrams, a_ngrams
    cdef int i_size, i

    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_words) + 1))):
        z_ngrams = zip(*[a_words[i:] for i in range(i_size)])
        a_ngrams = [' '.join(x) for x in z_ngrams]
        a_all_ngrams.append(a_ngrams)
    return a_all_ngrams


cpdef list all_ngrams_lists(text, int i_max_size=6, int i_min_size=1,
                            stop_words="english", remove_punct=True):
    ''' Returns a list of lists of ngrams from longest to shortest with
        sub-lists from first to last with each ngram being a list containing words. '''
    cdef list a_words, a_all_ngrams, a_ngrams
    cdef int i_size, i

    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_words) + 1))):
        z_ngrams = zip(*[a_words[i:] for i in range(i_size)])
        a_ngrams = [x for x in z_ngrams]
        a_all_ngrams.append(a_ngrams)
    return a_all_ngrams


cpdef list all_ngrams_flat(text, int i_max_size=6, int i_min_size=1, stop_words="english", remove_punct=True):
    ''' Returns a list of ngrams strings from longest to shortest from first to last for each size.'''
    cdef list a_words, a_all_ngrams, a_ngrams
    cdef int i_size, i

    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_words) + 1))):
        z_ngrams = zip(*[a_words[i:] for i in range(i_size)])
        a_ngrams = [' '.join(x) for x in z_ngrams]
        a_all_ngrams.extend(a_ngrams)
    return a_all_ngrams


cpdef list all_ngrams_tuples(text, int i_max_size=6, int i_min_size=1, stop_words="english", bint remove_punct=True):
    ''' Returns a list of ngrams tuples from longest to shortest from first to last for each size.'''
    cdef list a_words, a_all_ngrams, a_ngrams
    cdef int i_size, i

    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_words) + 1))):
        z_ngrams = zip(*[a_words[i:] for i in range(i_size)])
        a_ngrams = [tuple(x) for x in z_ngrams]
        a_all_ngrams.extend(a_ngrams)
    return a_all_ngrams


cpdef list end_grams(words, int i_max_length=4, return_tuples=None):
    ''' pass in list of words and return the ngrams anchored on end word '''
    cdef list a_words, a_ngrams
    cdef int i_size
    
    if not words: return []
    if isinstance(words, str):
        a_words = words.split()
        if return_tuples is None: return_tuples = False
    elif isinstance(words, (list, tuple)):
        a_words = words
        if return_tuples is None: return_tuples = True
    else:
        return []
    
    a_ngrams = []
    if return_tuples:
        for i_size in reversed(range(1, min(i_max_length, len(a_words)) + 1)):
            a_ngrams.append(tuple(a_words[-i_size:]))
    else:
        for i_size in reversed(range(1, min(i_max_length, len(a_words)) + 1)):
            a_ngrams.append(' '.join(a_words[-i_size:]))
    return a_ngrams
    

cpdef list skip_bigrams(text, int i_max_dist=4, int i_min_dist=1,
                        stop_words="english", bint remove_punct=True):
    ''' get a list of skip bigrams
        i_min_dist=1 includes normal bigrams '''
    cdef list a_words, a_all_bigrams
    cdef int i_start, i_end, i_words
    
    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)
    
    i_words = len(a_words)
    a_all_bigrams = []
    for i_start in range(0, i_words - i_min_dist):
        for i_end in range(i_start + i_min_dist, min(i_start + i_max_dist + 1, i_words)):
            a_all_bigrams.append((a_words[i_start], a_words[i_end]))
            
    return a_all_bigrams


cpdef list skip_grams(text, int i_max_size=5, int i_min_size=1,
                      int i_max_skip=3, bint b_sort_skips=True, b_skip_ngrams_only=False,
                      stop_words="english", bint remove_punct=True):
    ''' skip gram generator from nltk is too unreliable '''
    cdef list a_text, a_ngrams, a_skipngrams
    cdef tuple t_ngram, t_skip_gram
    cdef int i_phrase_len, i_size, i_start, i_end, i_skip_size, i_skip_start, i_skip_end
    
    if isinstance(text, (list, tuple)):
        a_text = list(text)
    elif isinstance(text, str):
        a_text = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)
    
    i_phrase_len = len(a_text)
    a_ngrams, a_skipngrams = [], []
    i_max_size = max(i_min_size, min(i_phrase_len, i_max_size)) + 1
    i_max_skip += 1
    
    for i_size in reversed(range(i_min_size, i_max_size)):
        for i_start in range(0, i_phrase_len - i_size + 1):
            i_end = i_start + i_size
            t_ngram = tuple(a_text[i_start:i_end])
            a_ngrams.append(t_ngram)
            if i_size < 3: continue
            for i_skip_size in range(1, min(i_max_skip, i_size)):
                for i_skip_start in range(1, i_size - i_skip_size):
                    i_skip_end = i_skip_start + i_skip_size
                    t_skip_gram = t_ngram[: i_skip_start] + t_ngram[i_skip_end: ]
                    a_skipngrams.append(t_skip_gram)
    
    if b_sort_skips and a_skipngrams:
        a_skipngrams.sort(key=len, reverse=True)
    
    if b_skip_ngrams_only:
        return a_skipngrams
    else:
        return a_ngrams + a_skipngrams


cpdef list char_ngrams(text, int i_max_size=6, int i_min_size=1):
    ''' Returns a list of character ngrams tuples from longest to shortest from first to last for each size.'''
    cdef list  a_all_ngrams, a_ngrams
    cdef int i_size, i

    if isinstance(text, str):
        a_chars = list(text)
    elif isinstance(text, (list, tuple)):
        a_chars = text
    
    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_chars) + 1))):
        z_ngrams = zip(*[a_chars[i:] for i in range(i_size)])
        a_ngrams = [tuple(x) for x in z_ngrams]
        a_all_ngrams.extend(a_ngrams)
    return a_all_ngrams





cpdef list text_list_to_ngram_list(list a_text_list, int i_max_size=6,
                                   int i_min_size=1, stop_words="english",
                                   bint remove_punctuation=True,
                                   bint b_case_insensitive=True,
                                   bint b_return_tuples=False):
    ''' Pass in a list of docs (strings such as sentences or attribute values).
        Calculuate the frequency (absolute count of occurences)
        and coverage (count of docs containing at least one occurence)
    '''
    cdef list a_words, a_ngrams
    cdef list a_ngram_list = []
    
    for text in a_text_list:
        if isinstance(text, str):
            if b_case_insensitive: text = text.lower()
            a_words = word_tokenize(text, remove_punctuation, stop_words)
        else:
            a_words = list(text)
        
        if b_return_tuples:
            a_ngrams = all_ngrams_tuples(a_words, i_max_size, i_min_size, stop_words)
        else:
            a_ngrams = all_ngrams_flat(a_words, i_max_size, i_min_size, stop_words)
        a_ngram_list.append(a_ngrams)
    return a_ngram_list
    

cpdef ngram_freq_coverage_in_list(list a_ngram_list):
    ''' Pass in a list of lists of ngrams.
        Calculuate the frequency (absolute count of occurences)
        and coverage (count of docs containing at least one occurence)
        and max occurence in any single doc. '''
    cdef list a_ngrams
    cdef dict e_frequency = {}
    cdef dict e_coverage = {}
    cdef dict e_max = {}
    cdef dict e_ngrams
    cdef int i_count
    
    for a_ngrams in a_ngram_list:
        e_ngrams = {}
        for s_ngram in a_ngrams:
            if s_ngram in e_ngrams: e_ngrams[s_ngram] += 1
            else: e_ngrams[s_ngram] = 1
            
        for s_ngram, i_count in e_ngrams.items():
            if s_ngram in e_frequency:
                e_frequency[s_ngram] += i_count
                e_coverage[s_ngram] += 1
                e_max[s_ngram] = max(i_count, e_max[s_ngram])
            else:
                e_frequency[s_ngram] = i_count
                e_coverage[s_ngram] = 1
                e_max[s_ngram] = i_count
                
    return e_frequency, e_coverage, e_max


cpdef dict ngram_freq_in_list(list a_ngram_list):
    ''' Pass in a list of lists of ngrams as strings. Calculuate the frequency (absolute count of occurences) '''
    cdef list a_ngrams
    cdef dict e_frequency = {}
    cdef dict e_ngrams
    cdef str s_ngram
    cdef int i_count
    
    for a_ngrams in a_ngram_list:
        e_ngrams = {}
        for s_ngram in a_ngrams:
            if s_ngram in e_ngrams: e_ngrams[s_ngram] += 1
            else: e_ngrams[s_ngram] = 1
        for s_ngram, i_count in e_ngrams.items():
            if s_ngram in e_frequency:
                e_frequency[s_ngram] += i_count
            else:
                e_frequency[s_ngram] = i_count
    return e_frequency


cpdef dict tuple_ngram_freq_in_list(list a_ngram_list):
    ''' Pass in a list of lists of ngrams as tuples. Calculuate the frequency (absolute count of occurences) '''
    cdef list a_ngrams
    cdef dict e_frequency = {}
    cdef dict e_ngrams
    cdef tuple t_ngram
    cdef int i_count
    
    for a_ngrams in a_ngram_list:
        e_ngrams = {}
        for t_ngram in a_ngrams:
            if t_ngram in e_ngrams: e_ngrams[t_ngram] += 1
            else: e_ngrams[t_ngram] = 1
        for t_ngram, i_count in e_ngrams.items():
            if t_ngram in e_frequency:
                e_frequency[t_ngram] += i_count
            else:
                e_frequency[t_ngram] = i_count
    return e_frequency


cpdef dict ngram_coverage_in_list(list a_ngram_list):
    ''' Pass in a list of lists of ngrams.
        Calculuate the coverage (count of docs containing at least one occurence) '''
    cdef list a_ngrams
    cdef dict e_coverage = {}
    cdef dict e_ngrams
    cdef str s_ngram
    cdef int i_count
    
    for a_ngrams in a_ngram_list:
        e_ngrams = {}
        for s_ngram in a_ngrams:
            if s_ngram in e_ngrams: e_ngrams[s_ngram] += 1
            else: e_ngrams[s_ngram] = 1
        for s_ngram, i_count in e_ngrams.items():
            if s_ngram in e_coverage:
                e_coverage[s_ngram] += 1
            else:
                e_coverage[s_ngram] = 1
    return e_coverage


cpdef dict tuple_ngram_coverage_in_list(list a_ngram_list):
    ''' Pass in a list of lists of ngrams. Calculuate the coverage
    (count of docs containing at least one occurence) '''
    cdef list a_ngrams
    cdef dict e_coverage = {}
    cdef dict e_ngrams
    cdef tuple t_ngram
    cdef int i_count
    
    for a_ngrams in a_ngram_list:
        e_ngrams = {}
        for t_ngram in a_ngrams:
            if t_ngram in e_ngrams: e_ngrams[t_ngram] += 1
            else: e_ngrams[t_ngram] = 1
        for t_ngram, i_count in e_ngrams.items():
            if t_ngram in e_coverage:
                e_coverage[t_ngram] += 1
            else:
                e_coverage[t_ngram] = 1
    return e_coverage
    

cpdef list ngram_table(list a_text_list, int i_max_size=6, int i_min_size=1,
                       stop_words="english", bint remove_punctuation=True,
                       bint b_case_insensitive=True, bint b_use_tuples=True):
    ''' Pass in a list of texts/docs.
        Calculuate the frequency (absolute count of occurences)
        and coverage (count of docs containing at least one occurence)
        and max (most occurences in a single doc).
        Return a list of tuples of index,ngram,freq,coverage,max,length. '''
    cdef list a_ngram_list
    cdef dict e_frequency, e_coverage, e_max, e_frequency_ci, e_coverage_ci, e_max_ci
    cdef tuple t_ngram, t_lower
    cdef str s_ngram, s_word, s_lower
    cdef int i_index, i_freq, i_count, i_coverage, i_max
    cdef list at_ngrams = []
    
    a_ngram_list = text_list_to_ngram_list(a_text_list, i_max_size,
                                           i_min_size, stop_words,
                                           remove_punctuation, False, b_use_tuples)

    e_frequency, e_coverage, e_max = ngram_freq_coverage_in_list(a_ngram_list)
    if b_case_insensitive:
        e_frequency_ci = freq_dict_to_lower(e_frequency)
        e_coverage_ci = freq_dict_to_lower(e_coverage)
        e_max_ci = freq_dict_to_lower(e_max)
        
        if b_use_tuples:
            for i_index, t_ngram in enumerate(e_frequency):
                t_lower = tuple([s_word.lower() for s_word in t_ngram])
                if t_lower in e_frequency_ci:
                    i_freq = e_frequency_ci[t_lower]
                    i_coverage = e_coverage_ci.get(t_lower, 0)
                    i_max = e_max_ci.get(t_lower[0], 0)
                    at_ngrams.append(
                        (i_index, t_ngram, i_freq, i_coverage, i_max, len(t_ngram))
                    )
                    del e_frequency_ci[t_lower]
                    del e_coverage_ci[t_lower]
                    del e_max_ci[t_lower]
        else:
            for i_index, s_ngram in enumerate(e_frequency):
                s_lower = s_ngram.lower()
                if s_lower in e_frequency_ci:
                    i_freq = e_frequency_ci[s_lower]
                    i_coverage = e_coverage_ci.get(s_lower, 0)
                    i_max = e_max_ci.get(s_lower[0], 0)
                    at_ngrams.append(
                        (i_index, s_ngram, i_freq, i_coverage, i_max, len(s_lower))
                    )
                    del e_frequency_ci[s_lower]
                    del e_coverage_ci[s_lower]
                    del e_max_ci[s_lower]
    else:
        for i_index, t_ngram_freq in enumerate(e_frequency.items()):
            i_coverage = e_coverage.get(t_ngram_freq[0], 0)
            i_max = e_max.get(t_ngram_freq[0], 0)
            at_ngrams.append(
                (i_index, t_ngram_freq[0], t_ngram_freq[1], i_coverage, i_max, len(t_ngram_freq[0]))
            )
    
    return at_ngrams


def get_ngram_count(t_ngram, e_ngrams):
    ''' Get the count of the ngram in all it's variations from the ngram freq dict '''
    if not t_ngram: return 0
    i_count = e_ngrams.get(t_ngram, 0)
    t_plural, t_singular = pluralize_tuple(t_ngram), singularize_tuple(t_ngram)
    
    if t_plural != t_ngram:
        i_count += e_ngrams.get(t_plural, 0)
    if t_singular != t_ngram:
        i_count += e_ngrams.get(t_singular, 0)
    
    if len(t_ngram) == 1:
        t_compound = compound_word_in_ngrams(t_ngram[0], e_ngrams)
        if t_compound:
            i_count += e_ngrams.get(t_compound, 0)
            t_plural, t_singular = pluralize_tuple(t_compound), singularize_tuple(t_compound)
            if t_plural != t_compound:
                i_count += e_ngrams.get(t_plural, 0)
            if t_singular != t_compound:
                i_count += e_ngrams.get(t_singular, 0)
            
            s_start, s_end = t_compound
            s_plural_end, s_singular_end = pluralize(s_end), singularize(s_end)
            i_count += e_ngrams.get(('{}-{}'.format(s_start, s_end),), 0)
            i_count += e_ngrams.get(('{}_{}'.format(s_start, s_end),), 0)
            if s_plural_end != s_end:
                i_count += e_ngrams.get(('{}-{}'.format(s_start, s_plural_end),), 0)
                i_count += e_ngrams.get(('{}_{}'.format(s_start, s_plural_end),), 0)
            if s_singular_end != s_end:
                i_count += e_ngrams.get(('{}-{}'.format(s_start, s_singular_end),), 0)
                i_count += e_ngrams.get(('{}_{}'.format(s_start, s_singular_end),), 0)
            
    elif len(t_ngram) > 1:
            s_start, s_end = t_ngram
            s_plural_end, s_singular_end = pluralize(s_end), singularize(s_end)
            i_count += e_ngrams.get((''.join([s_start, s_end]),), 0)
            i_count += e_ngrams.get(('-'.join([s_start, s_end]),), 0)
            i_count += e_ngrams.get(('_'.join([s_start, s_end]),), 0)
            
            if s_plural_end != s_end:
                i_count += e_ngrams.get((''.join([s_start, s_plural_end]),), 0)
                i_count += e_ngrams.get((''.join([s_start, s_singular_end]),), 0)
                i_count += e_ngrams.get(('-'.join([s_start, s_plural_end]),), 0)
                
            if s_singular_end != s_end:
                i_count += e_ngrams.get(('-'.join([s_start, s_singular_end]),), 0)
                i_count += e_ngrams.get(('_'.join([s_start, s_plural_end]),), 0)
                i_count += e_ngrams.get(('_'.join([s_start, s_singular_end]),), 0)
            
    return i_count


def combine_plural_singular_stems(e_ngrams):
    ''' Combine the counts of the plural and singular forms of the ngram counts. '''
    e_combined = {}
    at_ngrams = e_ngrams.keys()
    
    for t_ngram in at_ngrams:
        if t_ngram in e_combined: continue
        t_plural, t_singular = pluralize_tuple(t_ngram), singularize_tuple(t_ngram)
        if t_plural in e_combined or t_singular in e_combined: continue
        t_stem = tuple([NLTK_STEMMER(word) for word in t_ngram])
        if t_stem in e_combined: continue
        
        i_count = e_ngrams[t_ngram]
        
        if t_plural != t_ngram:
            i_count += e_ngrams.get(t_plural, 0)
        if t_singular != t_ngram:
            i_count += e_ngrams.get(t_singular, 0)
        if t_stem != t_ngram and t_stem != t_plural and t_stem != t_singular:
            i_count += e_ngrams.get(t_stem, 0)
        
        e_combined[t_ngram] = i_count
        e_combined[t_plural] = i_count
        e_combined[t_singular] = i_count
        if t_stem in e_combined: e_combined[t_stem] = i_count
    return e_combined


cpdef bint is_compound_word(str s_compound, dictionary, int i_start=2):
    ''' Test to see if a compound word like "corkboard" exists in the set/dictionary
        as individual words. '''
    cdef int i_split
    cdef str s_start, s_end, s_test
    cdef dict e_dict
    cdef set c_dict
    
    if len(s_compound) < 5: return False
    s_compound = s_compound.lower()
    
    if isinstance(dictionary, dict):
        e_dict = dictionary
        for i_split in range(i_start, len(s_compound) - i_start + 1):
            s_start, s_end = s_compound[:i_split], s_compound[i_split:]
            if (s_start, s_end) in dictionary:
                return True
            
            if (s_start in e_dict or (s_start,) in e_dict) and \
               (s_end in e_dict or (s_end,) in e_dict):
                return True
            
            s_test = '{}_{}'.format(s_start, s_end)
            if s_test in e_dict or (s_test,) in e_dict:
                return True
            
            s_test = '{}-{}'.format(s_start, s_end)
            if s_test in e_dict or (s_test,) in e_dict:
                return True
    
    else:
        if isinstance(dictionary, set):
            c_dict = dictionary
        else:
            c_dict = set(dictionary)
        
        for i_split in range(i_start, len(s_compound) - i_start + 1):
            s_start, s_end = s_compound[:i_split], s_compound[i_split:]
            if (s_start, s_end) in c_dict:
                return True
            
            if (s_start in c_dict or (s_start,) in c_dict) and \
               (s_end in c_dict or (s_end,) in c_dict):
                return True
            
            s_test = '{}_{}'.format(s_start, s_end)
            if s_test in c_dict or (s_test,) in c_dict:
                return True
            
            s_test = '{}-{}'.format(s_start, s_end)
            if s_test in c_dict or (s_test,) in c_dict:
                return True
            
    return False


cpdef compound_word_in_ngrams(str s_compound, dict e_ngrams, int i_start=2):
    ''' Test to see if a compound word like "corkboard" exists in the ngram dictionary,
        as bigram in either original, hyphenated or ngram form. '''
    cdef int i_split
    cdef str s_start, s_end, s_test
    cdef tuple t_test
    
    if len(s_compound) < 5: return None
    s_compound = s_compound.lower()
    # if s_compound in e_ngrams: return s_compound
    # t_compound = tuple([s_compound])
    # if t_compound in e_ngrams: return t_compound
    
    for i_split in range(i_start, len(s_compound) - i_start + 1):
        s_start, s_end = s_compound[:i_split], s_compound[i_split:]
        # if not s_start in e_ngrams and not tuple([s_start]) in e_ngrams: continue
        # if not s_end in e_ngrams and not tuple([s_end]) in e_ngrams: continue
        if (s_start, s_end) in e_ngrams: return (s_start, s_end)
        
        s_test = '{}_{}'.format(s_start, s_end)
        t_test = tuple([s_test])
        if s_test in e_ngrams: return s_test
        if t_test in e_ngrams: return t_test
        
        s_test = '{}-{}'.format(s_start, s_end)
        t_test = tuple([s_test])
        if s_test in e_ngrams: return s_test
        if t_test in e_ngrams: return t_test
    
    return None

cpdef dict get_compounds(dict e_test_words, dict e_ngrams=None):
    ''' the words in e_test_words that appear as compound ngrams in e_ngrams '''
    cdef tuple t_ngram
    cdef dict e_compounds = {}
    if e_ngrams is None: e_ngrams = e_test_words
    for t_ngram in e_test_words.keys():
        if len(t_ngram) > 1: continue
        # if t_ngram[0] in ENGLISH: continue
        compound = compound_word_in_ngrams(t_ngram[0], e_ngrams, i_start=3)
        if not compound: continue
        e_compounds[t_ngram] = compound
    return e_compounds


cpdef dict freq_dict_to_lower(dict e_freq):
    ''' Convert a frequency dict to lower-case and return it.
    This is here only to be used internally. '''
    cdef dict e_ci_freq = {}
    cdef int i_count
    cdef str s_key
    cdef tuple t_key
    
    for v_key, i_count in e_freq.items():
        if isinstance(v_key, str):
            s_key = v_key.lower()
            if s_key in e_ci_freq:
                e_ci_freq[s_key] += i_count
            else:
                e_ci_freq[s_key] = i_count
        elif isinstance(v_key, tuple):
            t_key = tuple([s_key.lower() for s_key in v_key])
            if t_key in e_ci_freq:
                e_ci_freq[t_key] += i_count
            else:
                e_ci_freq[t_key] = i_count
        else:
            if v_key in e_ci_freq:
                e_ci_freq[v_key] += i_count
            else:
                e_ci_freq[v_key] = i_count
                
    return e_ci_freq


cpdef list insert_phrase(list a_text, tuple t_phrase, bint b_prepend=False, int i_max_iter=10):
    ''' Insert the words of a phrase into a text if they aren't already present,
        anchoring each word not in a_text on an adjacent word already in a_text.
        If there is no overlap, either insert at beginning or end, depending on b_prepend option.
        Arguments:
            text: {list} of tokenized text
            t_phrase: {tuple} of ngram to insert
            b_prepend: {bool} if there is no overlap, insert at beginning instead of adding to end
            i_max_iter: {int} max iterations before giving up
        Returns:
            {list} of tokenized text expanded to include phrase
    '''
    
    cdef list a_text_lower, a_phrase, a_phrase_lower
    cdef dict e_text
    cdef int i_missing, i_inserted, i_iter, idx, i_pos  #, i_word
    cdef str word, next_word, s_word, s_next_word
    
    if not a_text:
        a_text.extend(list(t_phrase))
        return a_text
    if not t_phrase:
        return a_text
    
    a_text_lower = [word.lower() for word in a_text]
    a_phrase = list(t_phrase)
    a_phrase_lower = [word.lower() for word in t_phrase]
        
    e_text = {word: idx for idx, word in enumerate(a_text_lower)}
    i_missing = len([word for word in a_phrase_lower if word not in e_text])
    if i_missing == len(t_phrase):
        if b_prepend:
            return list(t_phrase) + a_text
        else:
            a_text.extend(list(t_phrase))
            return a_text
        
    i_iter, i_inserted = 0, 0
    while i_inserted < i_missing:
        if i_iter >= i_max_iter: break
        for word, next_word, s_word, s_next_word in zip(a_phrase_lower, a_phrase_lower[1:], a_phrase, a_phrase[1:]):
            
            if word not in e_text and next_word in e_text:
                i_pos = e_text[next_word]
                a_text.insert(i_pos, s_word)
                a_text_lower.insert(i_pos, word)
                e_text = {word: idx for idx, word in enumerate(a_text_lower)}
                i_inserted += 1
            
            elif word in e_text and next_word not in e_text:
                i_pos = e_text[word] + 1
                a_text.insert(i_pos, s_next_word)
                a_text_lower.insert(i_pos, next_word)
                e_text = {word: idx for idx, word in enumerate(a_text_lower)}
                i_inserted += 1
        i_iter += 1
    return a_text


#def find_tuple_anchor(a_text, t_phrase, b_reverse=True, b_case_sensitive=False):
#    ''' Find a position anchor for a word in the t_phrase inside the a_text
#        Arguments:
#            a_text: {list} of tokenized text that contains at least one word of t_phrase
#            t_phrase: {tuple} with at least one word present in a_text
#            b_reverse: {bool} if words in phrase should be checked for anchor from back to front
#            b_case_sensitive: {bool} ignore case difference between phrase and text
#        Returns:
#            {int} of the anchor position in the text
#            
#    '''
#    #TODO: create version of this that returns a separate index for each word supporting multiple anchors
#    if not a_text or not t_phrase: return -1
#    if not b_case_sensitive:
#        a_text = [word.lower() for word in a_text]
#        t_phrase = tuple([word.lower() for word in t_phrase])
#    
#    i_anchor = -1
#    e_text = {word: idx for idx, word in enumerate(a_text)}
#    #print('e_text', e_text)
#    if b_reverse:
#        for idx, word in enumerate(reversed(t_phrase)):
#            print(idx, word)
#            if word in e_text:
#                i_anchor = e_text[word]
#                if idx != 0:
#                    i_anchor += 1
#                else:
#                    i_anchor -= 1
#                break
#    return i_anchor


cpdef list unique_ngrams_to_phrase(list a_ngrams, bint b_pre_sorted=True, bint b_merge=True, bint b_prepend=False):
    ''' Combine ngrams to phrase, only adding them if their
        words aren't already contained in the phrase being constructed
        Arguments:
            a_ngrams: {list} of n-ngrams or skip-grams
            b_pre_sorted: {bool} if the ngrams are already sorted from longest to shortest
            b_merge: {bool} try to merge ptype ngrams first by identifying word overlaps
            b_prepend: {bool} if no overlap, prepend shorter ngram before longer ngram
        Returns:
            {list} of combined words from ngrams
    '''
    cdef list a_phrase, a_ngram
    cdef set c_words
    cdef tuple t_ngram
    cdef str s_word
    
    
    if not a_ngrams: return []
    if not b_pre_sorted:
        a_ngrams.sort(key=len, reverse=True)
    
    a_phrase = list(a_ngrams[0])
    c_words = set(a_phrase)
    
    for t_ngram in a_ngrams[1:]:
        a_ngram = [s_word for s_word in t_ngram if s_word.lower() not in c_words]
        if not a_ngram: continue
        
        c_words.update([s_word.lower() for s_word in a_ngram])
        
        if b_merge and len(a_ngram) < len(t_ngram):
            #i_pos = find_tuple_anchor(a_phrase, t_ngram)
            a_phrase = insert_phrase(a_phrase, t_ngram)
        else:
            if b_prepend:
                a_ngram.extend(a_phrase)
                a_phrase = a_ngram
            else:
                a_phrase.extend(a_ngram)
        
    return a_phrase
