﻿from __future__ import absolute_import
__all__ = [
    "all_plural_singular", "pluralize", "singularize",
    "pluralize_tuple", "singularize_tuple",
]

at_irregular = [
    ("Oxen", "Ox"), ("Feet", "Foot"), ("Teeth", "Tooth"), ("Children", "Child"), ("People", "Person"),
    ("Geese", "Goose"), ("Mice", "Mouse"), ("Criteria", "Criterion"), ("Dice", "Die"), ("Curricula", "Curriculum"),
    ("Safes", "Safe"), ("Humans", "Human"), ("Echoes", "Echo"), ("Embargoes", "Embargo"), ("Heroes", "Hero"),
    ("Potatoes", "Potato"), ("Tomatoes", "Tomato"), ("Fungi", "Fungus"), ("Torpedoes", "Torpedo"),
    ("Vetoes", "Veto"), ("Does", "Doe"), ("Foes", "Foe"), ("Hoes", "Hoe"), ("Joes", "Joe"),
    ("Noes", "No"), ("Poes", "Poe"), ("Toes", "Toe"), ("Woes", "Woe"), ("Ho's", "Ho"), ("To's", "To"),
    ("Jewelry", "Jewelry"), ("Storage", "Storage"), ("Barracks", "Barracks"), ("Means", "Means"),
    ("Offspring", "Offspring"), ("Species", "Species"), ("Series", "Series"),
    ("Deer", "Deer"), ("Moose", "Moose"), ("Fish", "Fish"), ("Shrimp", "Shrimp"), ("Swine", "Swine"),
    ("Goes", "Go"), ("Are", "Is"), ("Aren't", "Isn't"), ("Were", "Was"), ("Weren't", "Wasn't"), ("These", "This"),
    ("Gasses", "Gas"), ("Tonnes", "Ton"), ("Shoes", "Shoes"), ("Tennis", "Tennis"), ("Knife", "Knives"),
    ("Lives", "Life"), ("Halves", "Half"), ("Calves", "Calf"), ("Loaves", "Loaf"), ("Leaves", "Leaf"),
    ("Wives", "Wife"), ("Shelves", "Shelf"), ("Yourselves", "Yourself"), ("Themselves", "Themself"),
    ("Cookies", "Cookie"), ('About', 'About'),  ('Us', 'I'), ('We', 'Me'), ("Myself", "Myself"),
    ('Him', 'Him'), ('Her', 'Her'), ('Himself', 'Himself'), ('Herself', 'Herself'), ("Itself", "Itself"),
    ('Police', 'Police'), ('Now', 'Now'), ('Seen', 'Seen'), ('Having', 'Having'),
    ('P', 'P'), ('H', 'H'), ('W', 'W'), ('L', 'L'), ('F', 'F'), ('R', 'R'), ('E', 'E'),
    ('Here', 'Here'), ('There', 'There'), ('Says', 'Says'), ('Told', 'Told'), ('Became', 'Became'), ('Began', 'Began'),
    ('Born', 'Born'), ('Really', 'Really'), ('Very', 'Very'), ('Too', 'Too'), ('Won', 'Won'), ('Done', 'Done'),
    ('Often', 'Often'), ('Got', 'Got'), ('Came', 'Came'), ('Held', 'Held'), ('Built', 'Built'),
    ('Already', 'Already'), ('Always', 'Always'), ('Usually', 'Usually'),
    ('Must', 'Must'), ('Simply', 'Simply'), ('Quickly', 'Quickly'), ('Everything', 'Everything'),
    ('Nearly', 'Nearly'), ('Ever', 'Ever'), ('Originally', 'Originally'), ('Separately', 'Separately'),
    ('Gave', 'Gave'), ('Wrote', 'Wrote'), ('Paris', 'Paris'), ('Finally', 'Finally'), ('Generally', 'Generally'),
    ('Sent', 'Sent'), ('Anything', 'Anything'), ('Everyone', 'Everyone'), ('Currently', 'Currently'),
    ('Naturally', 'Naturally'), ('Highly', 'Highly'), ('Especially', 'Especially'), ('Golf', 'Golf'),
    ('Fell', 'Fell'), ('Immediately', 'Immediately'), ('Previously', 'Previously'), ('Automatically', 'Automatically'),
    ('Extremely', 'Extremely'), ('Red', 'Red'),
    ("Shoes", "Shoe"), ("Knives", "Knife"), ("Movies", "Movie"), ("Status", "Status"), ("Basis", "Basis"),
    ("Busses", "Bus"), ("Reliefs", "Relief"), ("Beliefs", "Belief"), ("Roofs", "Roof"),
    ("Hoodies", "Hoodie"), ("Foodies", "Foodie"),
    ("News", "News"), ("New", "New"),
]
cdef dict E_IRREGULAR_SINGULAR = {s_singular.lower(): s_plural for s_plural, s_singular in at_irregular}
cdef dict E_IRREGULAR_PLURAL = {s_plural.lower(): s_singular for s_plural, s_singular in at_irregular}

cdef set c_no_plural = {
    "ted", "ers", "ons", "ies", "nts", "les", "ces", "red", "eas", "ths",}
cdef set c_es_plural3 = {'tch'}
cdef set c_s_plural3 = {
    "ing", "ion", "ent", "ter", "nce", "ate", "eed", "nth", "oto"}
cdef dict e_plural3_ends = {suff: 'es' for suff in c_es_plural3}
e_plural3_ends.update({suff: 's' for suff in c_s_plural3})

cdef set c_plur_suff3 = set(e_plural3_ends.keys()).union({
    "tch", "man", "dex", "ies", "eed", "nth", "oto"})

c_no_plural = c_no_plural.union({
    "'s", "ed", "bs", "cs", "ds", "es", "gs", "ks", "ls", "ms", "ns", "os",
    "ps", "rs", "ts", "ws", "ys",
})
cdef set c_es_plural2 = {"ss", "ch", "sh", "ox", "ex", "ax", "zz"}
cdef set c_s_plural2 = {"er", "oo", "ay", "ey", "oy", "uy", "ff"}
cdef dict e_plural2_ends = {suff: 'es' for suff in c_es_plural2}
e_plural2_ends.update({suff: 's' for suff in c_s_plural2})

cdef set c_plur_suff2 = set(e_plural2_ends.keys()).union({
    "ss", "ch", "sh", "ox", "ex", "er", "oo", "ay", "ey", "oy", "uy", "is",
    "fe", "ix", "us", })

cdef dict e_plural1_ends = {
    "a": "s", "e": "s", "o": "es", "s": "es", "z": "zes"}
cdef set c_plur_suff1 = {"a", "o", "s", "e", "z", "y", "f"}

cdef set c_sing_suff4 = {
    "ches", "shes", "sses", "lles", "zzes", } #"lves"}
cdef set c_sing_suff3 = {
    "men", "ies", "xes", "oes",}


cpdef set all_plural_singular(set c_words):
    ''' take a set of words and make sure it includes
        all the singular and plural variations of each
        Arguments:
            words: {set} of str words
        Returns:
            {set} of plural and singular words
    '''
    c_words.update([pluralize(word) for word in c_words])
    c_words.update([singularize(word) for word in c_words])
    return c_words


cpdef str pluralize(str word):
    ''' Try to pluralize the word using standard plural forms and
        a list of irregular plural/singular words.
        Arguments:
            word: {str} word to pluralize
        Returns:
            {str} pluralized word
    '''
    cdef str s_word, s_plural, s_suffix, s_end
    cdef bint b_upper
    
    word = word.strip()
    if not word: return None
    cdef int i_len = len(word)
    
    if i_len >= 3 and word[-3:] in c_no_plural: return word
    if i_len >= 2 and word[-2:] in c_no_plural: return word
    if word[-1] == "'": return word
    
    s_word = word.lower()
    s_plural = None
    
    if s_word in E_IRREGULAR_SINGULAR:
        s_plural = E_IRREGULAR_SINGULAR[s_word]
        if word.isupper(): return s_plural.upper()
        elif word.islower(): return s_plural.lower()
        else: return s_plural
    
    b_upper = word.isupper()
    s_suffix = s_word[-3:]
    if s_suffix in c_plur_suff3:
        s_end = e_plural3_ends.get(s_suffix)
        if s_end:
            s_plural = word + s_end
        elif s_suffix == "man":
            s_plural = word[:-2] + "en"
        elif s_suffix == "dex":
            s_plural = word[:-2] + "ices"
        
        if s_plural:
            if b_upper: return s_plural.upper()
            else: return s_plural
    
    s_suffix = s_suffix[-2:]
    if s_suffix in c_plur_suff2:
        s_end = e_plural2_ends.get(s_suffix)
        if s_end:
            s_plural = word + s_end
        elif s_suffix == "is":
            s_plural = word[:-2] + "es"
        elif s_suffix == "fe":
            s_plural = word[:-2] + "ves"
        elif s_suffix == "ix":
            if i_len > 3:
                s_plural = word[:-1] + "ces"
            else:
                s_plural = word + "es"
        #elif s_suffix == "us":
        #    s_plural = word + "ses"
            #needed for latin words, but better to handle with irregular collection
            #if i_len > 4:
            #    s_plural = word[:-2] + "i"
            #if s_suffix == "um":
            #    s_plural = s_word[:-2] + "a"
        
        if s_plural:
            if b_upper: return s_plural.upper()
            else: return s_plural

    s_suffix = s_suffix[-1]
    if s_suffix in c_plur_suff1:
        #if s_suffix == "a" 'if there are more latin words than modern english
        #    s_plural = s_word + "e"
        if s_suffix in e_plural1_ends:
            s_plural = word + e_plural1_ends[s_suffix]
        elif s_suffix == "y":
            s_plural = word[:-1] + "ies"
        elif s_suffix == "f":
            s_plural = word[:-1] + "ves"
    
    else:
        s_plural = word + "s"
    
    if b_upper: return s_plural.upper()
    else: return s_plural


cpdef str singularize(str word):
    ''' Try to singularize the word using standard singular forms and a list of irregular plural/singular words.
        Arguments:
            word: {str} word to singularize
        Returns:
            {str} singularized word
    '''
    cdef str s_word, s_singular, s_suffix
    cdef bint b_upper
    
    word = word.strip()
    if not word: return None
    cdef int i_len = len(word)
    if i_len == 1: return word
    if "'" in word[-2:]: return word
    
    s_word = word.lower()
    s_singular = None
    
    if s_word in E_IRREGULAR_PLURAL:
        s_singular = E_IRREGULAR_PLURAL[s_word]
        if word.isupper(): return s_singular.upper()
        elif word.islower(): return s_singular.lower()
        return s_singular
    
    if word[-1] != "s" and word[-2:] != "en":
        return word
    b_upper = word.isupper()
    if s_word[-5:] in ["arves"]:
        s_singular = word[:-3] + "f"
    
    elif s_word[-4:] in c_sing_suff4:
        if s_word[-4:] in ["ches", "shes", "sses", "lles", "zzes"]:
            s_singular = word[:-2]
        #elif s_word[-4:] in ["lves"]:
        #    s_singular = word[:-3] + "f"
    
    elif s_word[-3:] in c_sing_suff3:
        if s_word[-3:] == "men":
            s_singular = word[:-2] + "an"
        elif s_word[-3:] == "ies" and i_len > 4:
            s_singular = word[:-3] + "y"
        elif s_word[-3:] == "xes":
            s_singular = word[:-2]
        elif s_word[-3:] == "oes":
            if i_len > 5:
                s_singular = word[:-2]
            else:
                s_singular = word[:-1]
            
    elif s_word[-2:] == "es":
        if s_word[-4:-2] in ["nn"]:
            s_singular = word[:-3]
        else:
            s_singular = word[:-1]
        
    elif s_word[-1] == "s":
        s_singular = word[:-1]
    else:
        s_singular = word
        
    if b_upper: return s_singular.upper()
    else: return s_singular


cpdef tuple pluralize_tuple(tuple t_words, bint b_last_word_only=True):
    cdef str s_word
    if not t_words: return t_words
    if b_last_word_only:
        return tuple(list(t_words[:-1]) + [pluralize(t_words[-1])])
    else:
        return tuple([pluralize(s_word) for s_word in t_words])

cpdef tuple singularize_tuple(tuple t_words, bint b_last_word_only=True):
    cdef str s_word
    if not t_words: return t_words
    if b_last_word_only:
        return tuple(list(t_words[:-1]) + [singularize(t_words[-1])])
    else:
        return tuple([singularize(s_word) for s_word in t_words])