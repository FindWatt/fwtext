﻿from __future__ import absolute_import
import re
from unicodedata import category, normalize
from nltk.corpus import stopwords
from .html_characters import get_html_char_codes
#from .dimension_patterns import (
#    DimensionsTokenizer, e_dimension_names, e_count_words,
#    dimensions_tokenizer, dimensionizer)

from .dimensions import Dimensionizer

dim_tok = Dimensionizer()

__all__ = [
    "trim", "remove_hyphens", "remove_apostrophes", "remove_possessives",
    "remove_periods", "fix_sentence_periods",
    "replace_ampersands", "replace_with", "remove_trademarks", "remove_slashes",
    "remove_html_characters", "replace_html_characters",
    "strip_unicode_accents", "strip_quotes",
    "clean_text", "clean_title", "clean_description",
    "clean_group_of_dicts", "clean_list", "remove_all_punctuation",
    "remove_stopwords",
    
    "s_UTF8_PUNCTUATION", "re_ALPHANUM", "re_ALPHA", "re_NUM",
    "re_PUNCT", "re_PUNCT_NOSPACE", "re_PUNCT_NOPERIOD", "re_PUNCT_NOSPACE_NOPERIOD",
    "re_PUNCT_WITH_EXCEPT", "re_PUNCT_UTF",
    "re_APOSTROPHE", "re_SPACES", "re_WHITESPACE", "re_MISC", "re_DIGITS",
    
]

s_UTF8_PUNCTUATION = r"!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~¡¢£¤¥¦§¨ª«¬¯°±´µ¶·¹º»¿×÷∞"
re_ALPHANUM = re.compile(r"[A-Z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚]+", flags=re.I)
re_ALPHA = re.compile(r"[A-Z]+", flags=re.I)
re_NUM = re.compile(r"[0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚]+")
re_PUNCT = re.compile(r"[^A-Z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚]+", flags=re.I)
re_PUNCT_NOSPACE = re.compile(r"[^A-Z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚&' \t]+", flags=re.I)
re_PUNCT_NOPERIOD = re.compile(r"[{}]+".format(''.join(
    [re.escape(s_char) for s_char in list(s_UTF8_PUNCTUATION)
     if s_char not in "'./-&"]
)), flags=re.I)
re_PUNCT_NOSPACE_NOPERIOD = re.compile(r"[^A-Z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚&'./ \t]+", flags=re.I)
re_PUNCT_WITH_EXCEPT = re.compile("[{}]+".format(''.join(
    [re.escape(s_char) for s_char in s_UTF8_PUNCTUATION if s_char not in "- .\"'&_%"]
)), flags=re.I)
re_PUNCT_UTF = re.compile(u"[+(),;" + b'\xc2\xa9\xc2\xae\xe2\x80\xa2\xe2\x84\xa2'.decode("utf8") + "]")
re_APOSTROPHE = re.compile("['`" + b'\xe2\x80\x99\xc2\xb4'.decode("utf8") + "]")
re_SPACES = re.compile(" {2,}")
re_WHITESPACE = re.compile(r"[ \t\r\n]+", flags=re.I)
re_DIGITS = re.compile("\d")

cdef set c_ASCII = set(
    [s for s in
     r'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,?!@#$%^&*()-_+=";:<>[]{}\|/']
)
cdef set c_DIGITS = set("0123456789½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚")
cdef set c_UPPER = set("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
cdef set c_LOWER = set("abcdefghijklmnopqrstuvwxyz")
cdef set c_ALPHANUM = set(list(c_UPPER) + list(c_LOWER) + list(c_DIGITS))
cdef set c_LOWERNUM = set(list(c_LOWER) + list(c_DIGITS))

s_QT = '"'
re_TRADEMARK_SYMBOLS = re.compile(r"([™®©]|(?<! [A-Z])[T][Mm](?=[ .,?!():;" + s_QT + "\r\n-]|'[Ss]|$))")
re_TRADEMARK_WORDS = re.compile(r"(trademark|registered|copyright|&trade;|&reg;|&copy;)", flags=re.I)

a_MPN_PATTERNS = [
    r"#?[a-z]{2,}-?0+(-?[a-z]+0+)+",  # mpn_1
    r"#?[a-z]{2,}(-?0+-?[a-z]+)+",  # mpn_2
    r"#?[a-z]{2,}(-?0+[a-z]+)+",  # mpn_3
    r"#?[a-z]{2,}-?0+",  # mpn_4
    r"#?0{4,}",  # mpn_5
    r"#?0{2,}-?([a-z]{3,}-?|[a-z]+)0+(-?([a-z]+|0+))*",  # mpn_6
    r"#?0{2,}-?[a-z]{3,}",  # mpn_7
    r"[a-z]+0+([a-z]+0+)+",  # mpn_8
    # r"0+\.0+(\.0+)+"  # mpn_9
    r"0+(\.0+){2,}"  # mpn_9
]

re_MISC = re.compile("(tm +|\(tm\)|\(registered\)|\(r\)|'s(?= |$)|(?<=s)'(?= |$))", flags=re.I)
re_MPN = re.compile("({})".format('|'.join(a_MPN_PATTERNS)), flags=re.I)
re_WITH = re.compile(r"((^w| +[wW])(/ *| +)|^[wW]/ *)(?![.?!])")

# dict of html character codes that point to the unicode character they represent
cdef dict e_HTML_CHARS = get_html_char_codes()

# English Stopwords from nltk.corpus
cpdef set c_ENGLISH_STOPW = set(stopwords.words("english"))


cpdef str trim(str text):
    ''' Remove leading and trailing whitespace characters,
        and replace multiple consecutive ones inside text with single space. '''
    return re_WHITESPACE.sub(' ', text).strip()

    
cdef int i_min_char_overlap = 4
cdef int i_min_word_len = 3
cpdef str remove_hyphens(str s_text, bint b_dehyphenate_phrases=False):
    ''' The purpose of this function to remove dashes,
        but keep hyphenated phrases and acronyms (like "multi-colored" and "CD-ROM").
        I'm not sure yet if this will eventually be included in the standard word_tokenize
        pipeline the way remove_periods or remove_apostrophes are.
        Remove hyphens with a space on either side,
        Remove when part of a two-word phrase where one piece is
        contained in the other and longer than 3 characters.
        b_dehyphenate_words: optionally de-hypenate full words where both are longer. '''
    s_text = re.sub(r"^-|-$", '', re.sub(r"(?<![A-Za-z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚])-(?![A-Za-z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚])", ' ', s_text))
    a_bigrams = re.finditer(r"((?<![A-Za-z-])[A-Za-z]+)-([A-Za-z]+(?![A-Za-z-]))", s_text)
    cdef list as_text = list(s_text)
    cdef str s_prefix, s_suffix
    cdef int i_hyphen
    cdef bint b_overlap
    
    if a_bigrams:
        for re_match in a_bigrams:
            s_prefix, s_suffix = re_match.group(1), re_match.group(2)
            s_pre_lower, s_suf_lower = s_prefix.lower(), s_suffix.lower()
            
            if not s_prefix or not s_suffix or \
               min(len(s_prefix), len(s_suffix)) < i_min_char_overlap: b_overlap = False
            if s_pre_lower in s_suf_lower or s_suf_lower in s_pre_lower: b_overlap = True
            else: b_overlap = False
                
            if b_overlap:
                i_hyphen = re_match.start() + len(s_prefix)
                as_text[i_hyphen] = ' '
            elif (
                b_dehyphenate_phrases and
                min(len(s_prefix), len(s_suffix)) >= i_min_word_len and
                not s_prefix.isupper() and
                not s_suffix.isupper()
            ):
                i_hyphen = re_match.start() + len(s_prefix)
                as_text[i_hyphen] = ' '
                
    return ''.join(as_text)
    
    
cpdef str remove_apostrophes(str s_text):
    """ Finds '' surrounding words and removes them. Apostrophes ' in the middle of a word
    (o'clock, Edd's, etc.) are not replaced. """
    return re.sub(r"((?<![A-Za-z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚'])['’‘](?=[A-Za-z0-9])|(?<=[A-Za-z])['’‘](?![A-Za-z]))", '', s_text)


cpdef str remove_possessives(str s_text):
    """ Find possessive like "'s" or "s'" at the end of words and removes it. """
    # singular possessive
    s_text = re.sub(r"(?<=[A-Za-z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚])'[Ss]", '', s_text)
    # plural possessive
    s_text = re.sub(r"(?<=[A-Za-z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚])[Ss]'", 's', s_text)
    return s_text


cpdef str remove_periods(str s_text, s_replace=' ', bint b_fix_decimals=True):
    ''' Remove periods in text that are there as punctuation
        while leaving in those acting as decimal points or in acronyms. '''
    cdef int i_pos, ind, i_len
    cdef list clean_chars
    cdef str char
    i_len = len(s_text)
    
    clean_chars = []
    for ind, char in enumerate(s_text):
        if char != ".":
            clean_chars.append(char)
        elif ind + 1 < i_len:
            if s_text[ind + 1] in c_DIGITS:
                if s_text[max(ind - 1, 0)] in c_DIGITS:
                    clean_chars.append(char)
                else:
                    clean_chars.append("0.")
            elif s_text[max(ind - 1, 0)].isupper() and s_text[ind + 1].isupper():
                clean_chars.append(char)
            else:
                clean_chars.append(s_replace)
        else:
            clean_chars.append(s_replace)

    return "".join(clean_chars).strip()


cpdef str fix_sentence_periods(str s_text):
    ''' Fix periods in text that are supposed to be acting as sentence separators,
        but are missing trailing spaces.
        Ignore periods between digits (decimals),
        and periods between uppercase letters (Acronyms). '''
    cdef int i_pos, ind, i_len
    cdef list clean_chars
    cdef str char
    i_len = len(s_text)
    
    clean_chars = []
    for ind, char in enumerate(s_text):
        if char != ".":
            clean_chars.append(char)
        elif ind + 1 < i_len and not re_WHITESPACE.search(s_text[ind + 1]):
            
            if s_text[max(ind - 1, 0)] in c_DIGITS:
                if s_text[ind + 1] in c_DIGITS:
                    clean_chars.append(".")
                else:
                    clean_chars.append(". ")
            elif s_text[max(ind - 1, 0)].isupper() and s_text[ind + 1].isupper():
                clean_chars.append(".")
            else:
                clean_chars.append(". ")
        else:
            clean_chars.append(".")

    return "".join(clean_chars).strip()


cpdef str replace_ampersands(str s_text):
    ''' Finds & between words and replaces them with " and "
        Not sure whether or not to do the same inside abbreviation (like "A&W").
        HTML character codes should be removed before calling this function.
    '''
    if s_text == '&': return 'and'
    #s_text = re.sub(r"^ *&( |$)", 'and ', s_text)
    #s_text = re.sub(r"(^| )& *$", ' and', s_text)
    #s_text = re.sub(r"(^ *&|& *$)", 'and', s_text)
    s_text = re.sub(r"^(\W)*&+ +|&+(\W)*$", '\g<1>', s_text)
    s_text = re.sub(r"(?<=[a-z])&(?=[a-z])", ' and ', s_text)
    return re.sub(r" *& +", ' and ', s_text)


cpdef str replace_with(str s_text):
    ''' Finds abbreviated version of "with" and replaces them with the word '''
    #return re.sub(r"(^| +)w(/ *| +)(?![.?!])", ' with ', s_text, flags=re.I).strip()
    return re_WITH.sub(' with ', s_text).strip()


cpdef str remove_trademarks(str s_text):
    ''' Remove UTF/HTML chars and words for trademark, registered, and copyright. '''
    return re_TRADEMARK_WORDS.sub('', re_TRADEMARK_SYMBOLS.sub('', s_text))


cpdef str remove_slashes(str s_text):
    ''' remove slashes that aren't part of a fraction '''
    s_text = re.sub(r"((?<=[^0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚ ])|^) */", " ", s_text)
    return re.sub(r"/ *((?=[^0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚ ])|$)", " ", s_text).strip()


cpdef str remove_html_characters(str s_text):
    ''' Finds html characters in the pattern of &___; and removes '''
    return re.sub(r"&#?[A-Za-z0-9½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚]{2,8};", '', s_text, flags=re.I)


cpdef str replace_html_characters(str s_text):
    ''' Swap html character codes with their unicode equivalents '''
    cdef list a_text = list(s_text)
    cdef str s_match, s_char
    for re_match in reversed(list(re.finditer(r"(&#?[A-Z0-9a-z]{2,8};)", s_text))):
        s_match = s_text[re_match.start():re_match.end()]
        s_char = e_HTML_CHARS.get(s_match, '')
        a_text[re_match.start():re_match.end()] = [s_char]
    return ''.join(a_text)
    

cpdef str strip_unicode_accents(str s_unicode_text, bint b_maintain_length=False):
    ''' First check if character is in ascii list, then check for unicode category to speed things up. '''
    cdef str s_char
    if b_maintain_length:
        return ''.join([
            s_char if s_char in c_ASCII
            else normalize('NFD', s_char)[0] if category(s_char) != 'Mn' else ' '
            for s_char in s_unicode_text
        ])
    else:
        return ''.join([
            s_char if s_char in c_ASCII
            else normalize('NFD', s_char)[0] if category(s_char) != 'Mn' else ''
            for s_char in s_unicode_text
        ])


cpdef str strip_quotes(str s_text, bint b_unmatched=False):
    ''' Remove end quotes from a string, but not if the string
        is empty or containing a single quote or only two quotes '''
    cdef bint b_start, b_end
    if len(s_text) < 2: return s_text
    if not b_unmatched and len(s_text) > 2:
        if s_text[0] != '"' or s_text[-1] != '"': return s_text
        return s_text[1:-1]
    else:
        b_start = s_text[0] == '"'
        b_end = s_text[-1] == '"'
        if b_start and b_end:
            if len(s_text) > 2:
                return s_text[1:-1]
            else:
                return s_text
        elif b_start:
            return s_text[1:]
        elif b_end and s_text[-2] not in c_DIGITS:
            return s_text[:-1]
        else:
            return s_text


cpdef str clean_title(str s_text):
    #s_text = strip_unicode_accents(s_text)
    s_text = remove_hyphens(s_text)
    #s_text = remove_html_characters(s_text)
    s_text = replace_html_characters(s_text)
    s_text = remove_apostrophes(s_text)
    s_text = remove_periods(s_text)
    s_text = remove_trademarks(s_text)
    s_text = replace_with(s_text)
    s_text = replace_ampersands(s_text)
    s_text = re_PUNCT_WITH_EXCEPT.sub(' ', s_text)
    s_text = re_SPACES.sub(' ', s_text).strip()
    return s_text


cpdef str clean_description(str s_text):
    #s_text = strip_unicode_accents(s_text)
    s_text = remove_hyphens(s_text)
    s_text = remove_apostrophes(s_text)
    s_text = remove_periods(s_text)
    s_text = remove_trademarks(s_text)
    s_text = replace_with(s_text)
    #s_text = remove_html_characters(s_text)
    s_text = replace_html_characters(s_text)
    s_text = replace_ampersands(s_text)
    s_text = re_PUNCT_WITH_EXCEPT.sub(' ', s_text)
    s_text = re_SPACES.sub(' ', s_text).strip()
    return s_text


cpdef str clean_text(s_text, bint b_accents=True,
                     bint b_lowercase=False,
                     bint b_punctuation=False,
                     bint b_normalize_digits=False,
                     bint b_product_numbers=False,
                     bint b_product_dimensions=False,
                     bint b_trademarks=False,
                     bint b_html_characters=False,
                     bint b_ampersands=False,
                     bint b_periods=False,
                     bint b_apostrophes=True,
                     bint b_misc=True,
                     ):
    ''' Clean and simplify text before it is turned into tokens to
        improve matching and reduce memory requirements. '''
    s_clean = str(s_text)
    
    if b_html_characters: # moved in front of accents, apostrophes, trademarks
        s_clean = replace_html_characters(s_clean)
        #s_clean = remove_html_characters(s_clean)
        
    if b_accents:
        s_clean = strip_unicode_accents(s_clean)
    
    if b_apostrophes:  # standardize apostrophe characters
        s_clean = re_APOSTROPHE.sub("'", s_clean)

    if b_misc:  # remove trademark words and possessives
        s_clean = re_MISC.sub(' ', s_clean)

    if b_normalize_digits:
        s_clean = re_DIGITS.sub('0', s_clean)

    if b_product_numbers:
        s_clean = re_MPN.sub(' ', s_clean)

    if b_product_dimensions:
        s_clean, l_dimensions = dim_tok.tokenize_dimensions(s_clean)

    if b_punctuation:
        s_clean = re_PUNCT_UTF.sub(' ', s_clean)
        
    if b_trademarks:
        s_clean = remove_trademarks(s_clean)
        
    if b_ampersands:
        s_clean = replace_ampersands(s_clean)
        
    if b_periods:
        s_clean = remove_periods(s_clean)

    if b_lowercase:
        s_clean = s_clean.lower()

    s_clean = re_SPACES.sub(' ', s_clean).strip()
    return s_clean


cpdef str remove_all_punctuation(str s_text, bint b_keepspaces=True, bint b_dehyphenate_phrases=True):
    ''' Remove all punctuation, includes dashes and apostrophes. Spaces optional. '''
    if b_keepspaces:
        s_text = remove_hyphens(
            replace_ampersands(
                remove_slashes(
                    remove_apostrophes(
                        remove_periods(
                            re_PUNCT_NOSPACE_NOPERIOD.sub('', s_text), s_replace=' ')
                    )
                )
            ),
            b_dehyphenate_phrases=b_dehyphenate_phrases
        )
        return trim(s_text)
    else:
        s_text = remove_hyphens(
            replace_ampersands(
                remove_slashes(
                    remove_apostrophes(
                        remove_periods(
                            re_PUNCT_NOPERIOD.sub('', s_text),
                            s_replace=''
                        )
                    )
                )
            ),
            b_dehyphenate_phrases=b_dehyphenate_phrases
        )
        return re_WHITESPACE.sub('', s_text)


# considermaking group a position argument, because it's required
cpdef clean_group_of_dicts(group=None, bint b_accents=True, bint b_lowercase=False,
                           bint b_punctuation=False, bint b_normalize_digits=False,
                           bint b_product_numbers=False):
    ''' Clean up keys/values of group of dictionaries. '''
    cdef dict e_dict
    if isinstance(group, list):
        return [{clean_text(key, b_accents, b_lowercase, b_punctuation,
                            b_normalize_digits, b_product_numbers):
                 clean_text(val, b_accents, b_lowercase, b_punctuation,
                            b_normalize_digits, b_product_numbers)
                 for key, val in e_dict.items()}
                for e_dict in group]
    elif isinstance(group, tuple):
        return tuple(
            [{clean_text(key, b_accents, b_lowercase, b_punctuation,
                        b_normalize_digits, b_product_numbers):
              clean_text(val, b_accents, b_lowercase, b_punctuation,
                         b_normalize_digits, b_product_numbers)
              for key, val in e_dict.items()}
             for e_dict in group]
    )
    elif isinstance(group, dict):
        return {dict_key: {clean_text(key, b_accents, b_lowercase, b_punctuation,
                                      b_normalize_digits, b_product_numbers):
                           clean_text(val, b_accents, b_lowercase, b_punctuation,
                                      b_normalize_digits, b_product_numbers)
                           for key, val in e_dict.items()}
                for dict_key, e_dict in group.items()}


# Consider making a_list a positional argument, because it's required
cpdef list clean_list(a_list=None, bint b_accents=True, bint b_lowercase=False,
                      bint b_punctuation=False, bint b_normalize_digits=False, int b_product_numbers=False):
    ''' Clean up keys/values of group of dictionaries. '''
    if isinstance(a_list, list):
        return [clean_text(s_text, b_accents, b_lowercase, b_punctuation, b_normalize_digits, b_product_numbers)
                for s_text in a_list]


cpdef list remove_stopwords(list tokenized_document, stop_words="english"):
    """
    @description: removes stopwords from a tokenized document.
    @arg tokenized_document: {list} A list of words to clean up.
    @arg stopwords_list: {list or str} A list of stopwords,
    or the string "english" to remove English stopwords.
    @return: {list} A list of words, cleaned from stopwords.
    """
    cdef str word
    cdef set c_stop_words

    if stop_words == "english":
        c_stop_words = c_ENGLISH_STOPW
    elif stop_words is None:
        c_stop_words = set([])
    elif isinstance(stop_words, set):  # could optimize by not doing this reconversion
        #c_stop_words = set([word.lower() for word in stop_words])
        c_stop_words = stop_words
    elif isinstance(stop_words, list):
        c_stop_words = set([word.lower() for word in stop_words])
    else:
        raise Exception("Unrecognized set of stopwords")

    return [
        word for word in tokenized_document
        if not word.lower() in c_stop_words
    ]