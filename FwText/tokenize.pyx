﻿from __future__ import absolute_import
import re
from nltk import stem
from nltk.corpus import stopwords
from .cleaning import *
#from .dimension_patterns import (
#    DimensionsTokenizer, e_dimension_names, e_count_words,
#    dimensions_tokenizer, dimensionizer)
from .dimension_abbreviations import a_ABBR_NAMES
from .dimensions import Dimensionizer, c_dimension_names
dim_tok = Dimensionizer()
    
__all__ = [
    "title_tokenize", "description_tokenize",
    "word_tokenize", "sentence_tokenize",
    "split_strip", "split_words",
    "split_camelcase", "split_number_uom", "split_edge_punctuation", "split_edge_quotes",
    "split_on_edge_punctuation",
    "join_oxford", "join_sentence",
    "NLTK_STEMMER", "a_TITLES", "a_ABBREV",
]

# English Stopwords from nltk.corpus
cpdef set c_ENGLISH_STOPW = set(stopwords.words("english"))

NLTK_STEMMER = stem.SnowballStemmer("english").stem

cdef set c_ASCII = set(
    [s for s in
     r'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,?!@#$%^&*()-_+=";:<>[]{}\|/']
)
cdef set c_DIGITS = set("0123456789½¾¼⅓⅔⅛⅜⅝⅞⅕⅖⅗⅘⅙⅚")
cdef set c_UPPER = set("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
cdef set c_LOWER = set("abcdefghijklmnopqrstuvwxyz")
cdef set c_ALPHANUM = set(list(c_UPPER) + list(c_LOWER) + list(c_DIGITS))
cdef set c_LOWERNUM = set(list(c_LOWER) + list(c_DIGITS))

cdef set c_BREAK_CHARS = set(".?!•\n\r\t*")
cdef set c_BREAK_WORDS = {'The', 'This', 'Like', 'Made'}
cdef set c_SPACE_CHARS = {' ', '\n', '\r', '\t'}
cdef int i_MIN_SENT_CHARS = 7

cdef set c_NO_SEP = set("/-")
cdef set c_NO_PRECEDING = set(",.?!%")
cdef set c_NO_FOLLOWING = set("#$")

a_TITLES = [
    'mr', 'mrs', 'miss', 'ms',
    'dr', 'phd', 'prof',
    'fa', 'pr', 'br', 'sr', 'rev',
    'pvt', 'cpl', 'sgt', 'ssgt', 'msgt',
    'lt', 'cpt', 'capt', 'maj', 'ltcol', 'col', 'gen',
    'ens', 'ltjg', 'lcdr', 'cdr', 'radm', 'adm',
]
cdef set c_TITLES = set(a_TITLES)

a_ABBREV = [
    'a', 'amp', 'amps', 'b', 'bbl',
    'cm', 'cms', 'cm2', 'cm²', 'cm3', 'cm³',
    'c', 'cc', 'ccs', 'centi', 'cu',
    'f', 'fl', 'fs', 'ft', 'fts', 'ft2', 'ft²', 'ft3', 'ft³',
    'g', 'gs', 'gal', 'gals', 'gph', 'gb', 'gr', 'grs', 'gro',
    'ha', 'in', 'in2', 'in²', 'in3', 'in³',
    'k', 'kb', 'kg', 'kgs', 'kilo', 'kl', 'km', 'kms',
    'kt', 'kts', 'kw', 'kws', 'kwh', 'kwhs',
    'l', 'ls', 'lb', 'lbs', 'lt', 'lts', 'lph',
    'm', 'ms', 'm2', 'm²', 'm3', 'm³', 'mb', 'mcg', 'mcgs', 'mg', 'mgs',
    'mi', 'mis', 'ml', 'mls', 'mm', 'mms', 'mph', 'mphs', 'mt', 'mts',
    'nm', 'nms', 'oz', 'ozs', 'pt', 'pts', 'qt', 'qts',
    'rpm', 'rpms', 'sq', 't', 'ts', 'tbsp', 'tbsps', 'tsp', 'tsps',
    'w', 'yd', 'yds', 'yd2', 'yd²', 'yd3', 'yd³',
    'co', 'corp', 'inc', 'llc', 'llp', 'ltd',
    'ave', 'aves', 'blvd', 'blvds', 'ln', 'lns', 'rd', 'rds', 'st', 'sts',
    'etc', 'vs', 'x', 'col', 'cols', 'fig', 'figs', 'ltr', 'ltrs', 'off',
    'wd', 'ht', 'hts', 'ln', 'lns', 'wt', 'wts', 'yr', 'yrs',
    'max', 'min', 'approx', 'amt',
    'bg', 'cs', 'ct', 'no', 'num', 'doz', 'ea', 'gr', 'pk', 'pkg', 'pks', 'plt', 'qty', 'rl', 'rls',
    'mr', 'mrs', 'ms', 'dr', 'phd', 'prof', 'sgt', 'capt', 'adm', 'gen',
    'assem', 'assoc', 'assn', 'convr', 'lngth', 'appr', 'asst', 'assy', 'comp',
    'adj', 'adv', 'vb', 'nn', 'brz', 'btn', 'cyl', 'deg', 'dia', 'eta',
    'ext', 'mag', 'mtd', 'mtg', 'psi', 'std', 'ste', 'apt', 'brz',
    'hr', 'dy', 'mon', 'lg', 'sm', 'med', 'md', 'lar', 'n', 's', 'e', 'w',
    'am', 'pm',  "'n", 'aux', 'mfr', 'mfc',
    'xsmall', 'xxsmall', 'x-small', 'xx-small',
    'xlarge', 'xxlarge', 'xxxlarge', 'xxxxlarge', 'xxxxxlarge',
    'x-large', 'xx-large', 'xxx-large', 'xxxx-large', 'xxxxx-large'
]
cdef set c_ABBREV = set(a_ABBREV)


cpdef list title_tokenize(
    str title, stop_words=None, phrases=None, int max_phrase_len=4,
    bint use_underscore=True, bint return_tuples=False, bint return_lower=False,
    bint clean=True,
    bint dimensionize=True, bint normalize_dimensions=True, bint normalize_uom_abbr=False,
):
    """ Title tokenizer that splits on spaces and some punctuation.
        Pre-parses to handle standard title expectations.
        Can remove stopwords, and join phrases in a passed in dictionary/set.
        Arguments:
            title: {str or unicode} The string to tokenize.
            stop_words: Either the string "english" to remove English stopwords,
                        or a list of words to be cleaned from the string.
            phrases: {set|dict} a set of phrases to consolidate into single tokens
            max_phrase_len: {int} the maximum length of an ngram to look for in the phrase list
            use_underscore: {bin} whether to use '_' in joining phrases
            return_tuples: {bin} return subphrases as tuples in the tokenized list instead of joining with ' ' or '_'
            return_lower: {bin} convert words to lowercase before returning
            clean: {bin} clean the text before tokenizing
            dimensionize: {bool} identify the dimensions in the title
            normalize_dimensions: {bool} standardize the underscores around the "x" for multi-d
                make sure the leading zero is present for decimals
            normalize_uom_abbr: {bool} standardize abbreviated UOMs
        Returns:
            {list} A list of the tokens extracted from the document
    """
    cdef int i_first_word, i_max_phrase_len, i_last_word
    cdef tuple t_dims, t_phrase, t_phrase_lower
    cdef set c_dims, c_stop_words
    cdef str s_word, s_phrase
    cdef str s_title = strip_unicode_accents(title)
    
    if dimensionize:
        t_dims = dim_tok.tokenize_dimensions(s_title, b_normalize=normalize_dimensions, b_replace_abbr=normalize_uom_abbr)
        if use_underscore:
            c_dims = set([re.sub(' ', '_', clean_title(dim.string_match)) for dim in t_dims[1]])
        else:
            c_dims = set([clean_title(dim.string_match) for dim in t_dims[1]])
    else:
        c_dims = set()
    
    if clean: s_title = clean_title(s_title)
    cdef list a_tokenized = s_title.split()
    
    if not phrases: phrases = []
    
    if a_tokenized and (phrases or c_dims):
        i_first_word = 0
        while i_first_word < len(a_tokenized) - 1:
            for i_last_word in reversed(range(i_first_word + 2, max(len(a_tokenized) + 1, 1 + max_phrase_len))):
                t_phrase = tuple(a_tokenized[i_first_word:i_last_word])
                t_phrase_lower = tuple([word.lower() for word in a_tokenized[i_first_word:i_last_word]])
                if use_underscore:
                    s_phrase = '_'.join(t_phrase)
                else:
                    s_phrase = ' '.join(t_phrase)

                if (
                    s_phrase in c_dims or t_phrase in phrases or
                    s_phrase.lower() in phrases or t_phrase_lower in phrases
                ):
                    if return_tuples:
                        a_tokenized[i_first_word:i_last_word] = [t_phrase]
                    else:
                        a_tokenized[i_first_word:i_last_word] = [s_phrase]
                    break

            i_first_word += 1
    
    if stop_words:
        if stop_words is "english": c_stop_words = c_ENGLISH_STOPW
        elif isinstance(stop_words, set): c_stop_words = stop_words
        elif isinstance(stop_words, (list, tuple)): c_stop_words = set([s_word.lower() for s_word in stop_words])
        a_tokenized = [x for x in a_tokenized
                       if not isinstance(x, str) or x.lower() not in c_stop_words]

    if return_lower:
        a_tokenized = [x.lower() if isinstance(x, str)
                       else tuple([s_word.lower() for s_word in x])
                       for x in a_tokenized]

    return a_tokenized
    

cpdef list description_tokenize(
    str description, stop_words=None,
    phrases=None, set dictionary=None, set trademarks=None, int max_phrase_len=4,
    bint use_underscore=True, bint return_sentences=True,
    bint return_tuples=False, bint return_lower=False,
    bint clean=True,
    bint dimensionize=True, bint normalize_dimensions=True, bint normalize_uom_abbr=False,
):
    """ Description tokenizer that divides the description into sentences, and then into words.
            Splits on spaces and some punctuation.
            Pre-parses to handle standard description expectations.
            Can remove stopwords, and join phrases in a passed in dictionary/set.
        Arguments:
            description: {str or unicode} The string to tokenize.
            stop_words: Either the string "english" to remove English stopwords,
                or a list of words to be cleaned from the string.
            phrases: {set|dict} a set of phrases to consolidate into single tokens
            max_phrase_len: {int} the maximum length of an ngram to look for in the phrase list
            use_underscore: {bin} whether to use '_' in joining phrases
            return_sentences: {bin} return list of sentences that are list of words, instead of just a list of all words
            return_tuples: {bin} return subphrases as tuples in the tokenized list instead of joining with ' ' or '_'
            return_lower: {bin} convert words to lowercase before returning
            clean: {bin} clean the text before tokenizing
            dimensionize: {bool} identify the dimensions in the title
            normalize_dimensions: {bool} standardize the underscores around the "x" for multi-d
                make sure the leading zero is present for decimals
            normalize_uom_abbr: {bool} standardize abbreviated UOMs
        Returns:
            {list} A list of the tokens extracted from the document
    """
    cdef int i_first_word, i_max_phrase_len, i_last_word
    cdef tuple t_dims, t_phrase, t_phrase_lower
    cdef list a_description, a_sentences, a_tokenized
    cdef set c_dims, c_stop_words
    cdef str s_word, s_phrase
    cdef str s_description = strip_unicode_accents(description)
    
    if dimensionize:
        t_dims = dim_tok.tokenize_dimensions(description, b_normalize=normalize_dimensions, b_replace_abbr=normalize_uom_abbr)
        if use_underscore:
            c_dims = set([re.sub(' ', '_', clean_description(dim.string_match)) for dim in t_dims[1]])
        else:
            c_dims = set([clean_description(dim.string_match) for dim in t_dims[1]])
    else:
        c_dims = set()
    
    if clean:
        a_sentences = [
            clean_description(sentence)
            for sentence in
            sentence_tokenize(description, dictionary=dictionary, trademarks=trademarks)]

    if not phrases: phrases = []
    
    a_description = []
    for s_sentence in a_sentences:
        if not s_sentence: continue
        a_tokenized = s_sentence.split()

        if a_tokenized and (phrases or c_dims):
            i_first_word = 0
            while i_first_word < len(a_tokenized) - 1:
                for i_last_word in reversed(range(i_first_word + 2, max(len(a_tokenized) + 1, 1 + max_phrase_len))):
                    t_phrase = tuple(a_tokenized[i_first_word:i_last_word])
                    t_phrase_lower = tuple([word.lower() for word in a_tokenized[i_first_word:i_last_word]])
                    if use_underscore:
                        s_phrase = '_'.join(t_phrase)
                    else:
                        s_phrase = ' '.join(t_phrase)

                    if (
                        s_phrase in c_dims or t_phrase in phrases or
                        s_phrase.lower() in phrases or t_phrase_lower in phrases
                    ):
                        if return_tuples:
                            a_tokenized[i_first_word:i_last_word] = [t_phrase]
                        else:
                            a_tokenized[i_first_word:i_last_word] = [s_phrase]
                        break

                i_first_word += 1

        if stop_words:
            if stop_words is "english": c_stop_words = c_ENGLISH_STOPW
            elif isinstance(stop_words, set): c_stop_words = stop_words
            elif isinstance(stop_words, (list, tuple)): c_stop_words = set([s_word.lower() for s_word in stop_words])
            a_tokenized = [x for x in a_tokenized
                           if not isinstance(x, str) or x.lower() not in c_stop_words]

        if return_lower:
            a_tokenized = [x.lower() if isinstance(x, str)
                           else tuple([s_word.lower() for s_word in x])
                           for x in a_tokenized]
        
        if return_sentences:
            a_description.append(a_tokenized)
        else:
            a_description.extend(a_tokenized)
        
    return a_description
    

cpdef list word_tokenize(str s_text, bint remove_punct=True, stop_words="english",
                         vocabulary=None, bint return_lower=False, bint convert_ampersand=False):
    """
    @description: Simple tokenizer that splits on spaces.Can remove punctuation
    and stopwords.
    @arg text: {str or unicode} The string to tokenize.
    @arg remove_punct: {bool} Whether to remove punctuation.
    @arg stop_words: Either the string "english" to remove English stopwords,
    or a list of words to be cleaned from the string.
    @return: {list} A list of the tokens extracted from the document
    """
    cdef list a_tokenized
    cdef str s_word
    
    if convert_ampersand:
        s_text = replace_ampersands(s_text)
        
    if remove_punct:
        s_text = remove_hyphens(
            remove_slashes(
                remove_apostrophes(
                    remove_periods(
                        re_PUNCT_NOPERIOD.sub(' ', s_text)
                    )
                )
            ),
            b_dehyphenate_phrases=True
        )

    if stop_words:
        a_tokenized = remove_stopwords(s_text.split(), stop_words=stop_words)
    else:
        a_tokenized = s_text.split()

    if return_lower:
        a_tokenized = [x.lower() for x in a_tokenized]
                       
    if vocabulary is not None:
        return [s_word for s_word in a_tokenized if s_word in vocabulary]
    else:
        return a_tokenized
        

cpdef list sentence_tokenize(str s_text, set dictionary=None, set trademarks=None):
    cdef str s_pp, s_p, s_c, s_n, s_nn, s_nw
    cdef str s_last_word, s_last_word_lower, s_sent, s_next_word
    cdef int i_char, i_word_start, i_prior_word_start, i_sent_start, i_split_char, i_len, i_end
    cdef bint use_dictionary = True if dictionary else False
    cdef bint use_trademarks = True if trademarks else False
    
    s_text = replace_html_characters(s_text).strip()
    cdef set c_tms = set(trademarks) if trademarks else set()
    cdef list a_sentences = []
    
    i_len = len(s_text)
    i_char, i_end = 0, i_len - (max(2, i_MIN_SENT_CHARS))
    if not s_text: return []
    if len(s_text) < i_MIN_SENT_CHARS: return [s_text]
    
    s_pp, s_p, s_c, s_n, s_nn = '', '', s_text[0], s_text[1], s_text[2]
    i_word_start, i_prior_word_start, i_sent_start, i_split_char = 0, 0, 0, 0
    
    while i_char < i_end:
        # check for split
        if s_c in c_LOWERNUM:
            i_char += 1
            s_pp, s_p, s_c, s_n, s_nn = s_p, s_c, s_n, s_nn, s_text[i_char + 2]
            continue
            
        elif not use_dictionary and s_c in c_UPPER:
            i_char += 1
            s_pp, s_p, s_c, s_n, s_nn = s_p, s_c, s_n, s_nn, s_text[i_char + 2]
            continue
            
        if s_c in c_SPACE_CHARS:
            i_prior_word_start = i_word_start
            i_word_start = i_char + 1
            if s_c != ' ':
                i_split_char = i_char  # returns
            elif s_n in c_UPPER and s_p in c_SPACE_CHARS:
                i_split_char = i_char  # 2 spaces in a row
                
        elif s_c in c_UPPER and s_n in c_LOWER and s_pp in c_LOWER:
            if s_p in c_LOWER:
                if use_trademarks:
                    i_next_space = i_char
                    while i_next_space < i_len and s_text[i_next_space] not in c_SPACE_CHARS: i_next_space += 1
                    s_last_word = s_text[i_word_start:i_next_space]
                    s_last_word_lower = s_last_word.lower()
                    if (
                        s_last_word in trademarks or (s_last_word,) in trademarks or
                        s_last_word_lower in trademarks or (s_last_word_lower,) in trademarks
                    ):
                        pass
                    elif s_text[i_word_start : i_char] in dictionary:  # deliberally only checking exact case
                        i_split_char = i_char  # missing period and space
                    
                elif s_text[i_word_start : i_char] in dictionary:  # deliberally only checking exact case
                    i_split_char = i_char  # missing period and space
            # too many false positives to try at this point
            #elif s_p in c_SPACE_CHARS:
            #    s_last_word = s_text[i_prior_word_start : i_char - 1]
            #    print('"{}"'.format(s_last_word))
            #    if s_last_word.islower():
            #        i_next_space = i_char
            #        while i_next_space < i_len and s_text[i_next_space] not in c_SPACE_CHARS: i_next_space += 1
            #        s_next_word = s_text[i_char:i_next_space]
            #        s_nw = s_text[i_next_space + 1]
            #        print('i_next_space: {}, s_next_word: "{}", s_nw:"{}"'.format(i_next_space, s_next_word, s_nw))
            #        i_split_char = i_char  # missing period
                
        elif s_c in c_BREAK_CHARS:
            s_last_word = s_text[i_word_start : i_char]
            s_last_word_lower = s_last_word.lower()
            
            if s_n == "'" and s_nn == "s":
                pass  # abbreviation possessive
            elif s_c == '.':
                if s_n in {',', '&', ';', ':'}:
                    pass  # probably an abbreviation
                elif s_p in c_DIGITS:
                    if s_n in c_DIGITS:
                        pass  # decimal
                    elif s_pp in c_DIGITS:
                        i_split_char = i_char + 1  # integer
                    else:
                        i_split_char = i_char - 1  # numbered list?
                elif s_n in c_DIGITS:
                    i_split_char = i_char  #broken decimal?
                elif s_n in c_SPACE_CHARS:
                    if s_nn in c_SPACE_CHARS:
                        i_split_char = i_char + 1  # double space
                    elif s_nn in {')', ']', '}'}:
                        pass  # period inside parentheses or brackets doesn't end sentence
                    elif s_nn in {',', '&', ';', ':', '.'}:
                        pass  # probably an abbreviation
                    elif s_last_word.istitle() and s_last_word_lower in c_TITLES:
                        pass  # personal title
                    elif s_nn in c_UPPER and not s_last_word.istitle():
                        i_split_char = i_char + 1  # end sentence
                    elif s_last_word_lower in c_ABBREV:  #  abbreviation
                        if s_last_word.istitle():
                            pass # now that we have a separate title check, maybe this should be split?
                        else:
                            i_split_char = i_char + 1  # not sure if this is correct?
                    elif s_nn in c_LOWER:
                        pass
                    else:
                        i_split_char = i_char + 1
                elif s_n == '.':
                    pass  # ellipses
                elif s_nn == '.':
                    pass  # acronym
                elif s_n in {')', ']', '}'}:
                    pass  # period inside parentheses or brackets doesn't end sentence
                elif s_n == '"':
                    i_split_char = i_char + 2  # end quote sentence
                elif s_n in c_UPPER and s_nn in c_LOWER:
                    i_split_char = i_char + 1  # missing space
                elif s_n in c_LOWER:
                    pass
                else:
                    i_split_char = i_char + 1
            elif s_c in {'!', '?'}:
                if s_n in c_BREAK_CHARS:
                    pass
                elif s_n in c_SPACE_CHARS and s_nn in c_LOWER:
                    pass  # probably a trademark type situation, but could be capitalization problem
                elif s_n in {')', ']', '}'}:
                    pass  # inside parentheses or brackets doesn't end sentence
                elif s_n in {',', '&', ';', ':'}:
                    pass  # probably an trademark
                elif c_tms and s_last_word_lower in c_tms:
                    pass  # this needs to actually check if punctuation is part of trademark
                else:
                    i_split_char = i_char + 1
            elif s_c in {'•', '*'}:
                if s_c == '*' and s_p in c_SPACE_CHARS:
                    i_split_char = i_char  # acting as bullet
                elif s_c == '•':
                    i_split_char = i_char  # bullet
            else:
                i_split_char = i_char

        # apply the split
        if i_split_char > 0:
            if i_split_char >= i_end:
                break
            elif i_split_char > i_sent_start:
                s_sent = s_text[i_sent_start : i_split_char].strip()
                if s_sent: a_sentences.append(s_sent)
            i_prior_word_start = i_word_start
            i_sent_start, i_word_start = i_split_char, i_split_char
            i_split_char = 0
            i_char += i_MIN_SENT_CHARS
            
            if i_char >= i_end:
                break
            elif i_sent_start > i_char:
                i_char = i_sent_start
            
            s_pp, s_p, s_c, s_n, s_nn = s_text[i_char - 2: i_char + 3]
        
        else:
            i_char += 1
            s_pp, s_p, s_c, s_n, s_nn = s_p, s_c, s_n, s_nn, s_text[i_char + 2]
            
    if len(s_text) > i_sent_start:
        s_sent = s_text[i_sent_start:].strip()
        if s_sent: a_sentences.append(s_sent)
        
    return a_sentences


cpdef list split_strip(str s_text, str s_delimiter=None, int i_max_split=-1, str s_chars=None, b_from_right=False):
    ''' Split a string into pieces by a delimiter and strip whitespace from each piece.
        Example "This : Example" split on ":" would yield a list of ["This", "Example"]. '''
    cdef str s
    if b_from_right:
        return [s.strip(s_chars) for s in s_text.rsplit(s_delimiter, i_max_split)]
    else:
        return [s.strip(s_chars) for s in s_text.split(s_delimiter, i_max_split)]


cpdef split_words(list a_words, f_split):
    ''' Go through the words and split using the specified function
        Expects the results of f_split to be a tuple or list if successful.
        Words that weren't split can be return as string or None
        Operates in place on a_words list
    '''
    cdef int index
    cdef str word
    for index, word in reversed(list(enumerate(a_words))):
        split = f_split(word)
        if not split or isinstance(split, str): continue
        if isinstance(split, tuple):
            a_words[index:index+1] = list(split)
        elif isinstance(split, list):
            a_words[index:index+1] = split

re_camelcase = re.compile(r'([A-Z][a-z]+)([A-Z][a-z]+)+')
re_camelcase_split = re.compile(r'([A-Z][a-z]+)')
cpdef tuple split_camelcase(str s_word):
    ''' Examine a word to see if it is camel case.
        If it is, return a tuple that splits it into its constituent pieces
    '''
    cdef str s_x
    cdef tuple t_split
    if re_camelcase.search(s_word):
        t_split = tuple([s_x for s_x in re_camelcase_split.split(s_word) if s_x])
        return t_split
    else:
        return tuple()


re_uom_split = re.compile(r'^([^A-Z]+?)[-_ ,.]*([A-Z][A-Z_.-]+)$', flags=re.I)
cpdef tuple split_number_uom(str s_word):
    ''' Examine a word to see if it is a combination of number
        and unit of measurement. If it is, then split it.
    '''
    o_re = re_uom_split.search(s_word)
    if not o_re: return tuple()
    s_num, s_uom = o_re.groups()
    #if not re_NUM.search(s_num) or not s_uom.lower() in e_dimension_names:
    if not re_NUM.search(s_num) or not s_uom.lower() in c_dimension_names:
        return tuple()
    
    return (s_num, s_uom)


cpdef split_edge_punctuation(str s_word):
    ''' Split word that contains leading or trailing punctuation. '''
    cdef int i_lead_len, i_trail_len
    cdef list a_word
    
    o_leading = re.search(r'^[-/\[\](),!%&*:;]+(?=(\w|[\'"]))', s_word)
    o_trailing = re.search(r'(?<=(\w|[\'"]))[-/\[\](),!?.$&*:;]+$', s_word)
    
    if not o_leading and not o_trailing: return s_word
    
    if o_leading:
        i_lead_len = o_leading.end()
        a_word = [s_word[:i_lead_len], s_word[i_lead_len:]]
    else:
        a_word = [s_word]

    if o_trailing:
        i_trail_len = o_trailing.end() - o_trailing.start()
        a_word[-1] = a_word[-1][:-i_trail_len]
        a_word.append(s_word[-i_trail_len:])
    
    return a_word


cpdef split_edge_quotes(str s_word):
    ''' Split leading and trailing quotes off. Leave dimensions. '''
    cdef list a_word
    if len(s_word) <= 1: return s_word
    if len(s_word) == 2 and s_word == '""' or s_word == "''": return s_word
    
    if s_word[0] == '"' and s_word[-1] == '"':
        return ['"', s_word[1:-1], '"']
    
    elif s_word[0] == "'" and s_word[-1] == "'":
        return ["'", s_word[1:-1], "'"]
    
    elif s_word[0] == "'" or s_word[0] == '"':
        return [s_word[0], s_word[1:]]
    
    elif (s_word[-1] == "'" or s_word[-1] == '"') and not s_word[-2] in set(list('0123456789')):
        return [s_word[:-1], s_word[-1]]
    
    else:
        return s_word


cpdef list split_on_edge_punctuation(list a_words):
    ''' Split words in list containing with possible leading/trailing punctuation
        into separate tokens.
        Example: ["test/",",this-", "out"] becomes ["test", "/", ",", "this", "-", "out"]. '''
    cdef list a_new = []
    cdef str s_word, s_trailing
    cdef int i_lead_len, i_trail_len
    
    for s_word in a_words:
        o_leading = re.search("^[-/\[\](),!%&*:;]+(?=\w)", s_word)
        o_trailing = re.search("(?<=\w)[-/\[\](),!$&*:;]+$", s_word)
        if o_leading:
            i_lead_len = o_leading.end()
            a_new.append(s_word[:i_lead_len])
            s_word = s_word[i_lead_len:]
            
        if o_trailing:
            i_trail_len = o_trailing.end() - o_trailing.start()
            s_trailing = s_word[-i_trail_len:]
            s_word = s_word[:-i_trail_len]
            
        a_new.append(s_word)
        if o_trailing: a_new.append(s_trailing)
            
    return a_new


cpdef str join_oxford(list a_list):
    ''' Join a list with commas between elements
    and the last word separated by "and" '''
    if not a_list:
        return ''
    elif len(a_list) == 1:
        return str(a_list[0])
    else:
        return ''.join([
            ', '.join(a_list[:-1]),
            ', and ',
            str(a_list[-1])
        ])
    

cpdef str join_sentence(list a_list, str s_delimiter=' '):
    ''' Try to intelligently join words taking into account punctuation
        that should not have spaces before/after. '''
    if not a_list: return ''
    
    cdef list a_new = [a_list[0]]
    cdef int i_len = len(a_list)
    cdef int index = 1
    cdef str s_elem, s_prec, s_foll
    
    while index < i_len:
        s_elem, s_prec = a_list[index], a_list[index - 1]
        s_foll = a_list[index + 1] if index < i_len - 1 else ''
        if not s_elem: continue
            
        if s_elem in c_NO_SEP and re_ALPHANUM.search(s_prec) and re_ALPHANUM.search(s_foll):
            a_new[-1] += s_elem + s_foll
            index += 1
        elif s_elem in c_NO_PRECEDING and re_ALPHANUM.search(s_prec):
            a_new[-1] += s_elem
        elif  s_prec in c_NO_FOLLOWING and re_ALPHANUM.search(s_elem):
            a_new[-1] += s_elem
        else:
            a_new.append(s_elem)
            
        index += 1
    return s_delimiter.join(a_new)
