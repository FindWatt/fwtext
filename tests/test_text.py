﻿'''
Test cases for FwText/text.pyx
'''
from __future__ import absolute_import
import pyximport; pyximport.install()
import string
import re
import pytest
import FwText


class TestSortTokensByLength():
    def test_blank(self):
        result = FwText.sort_tokens_by_length([], b_descending=False)
        assert result == []
    
    def test_descending_default(self):
        result = FwText.sort_tokens_by_length(set(['test', 'a', 'length', 'check']), b_descending=True)
        assert result == ('length', 'check', 'test', 'a')
    
    def test_ascending(self):
        result = FwText.sort_tokens_by_length(set(['test', 'a', 'length', 'check']), b_descending=False)
        assert result == ('a', 'test', 'check', 'length')
        
    def test_list(self):
        result = FwText.sort_tokens_by_length(['test', 'a', 'length', 'check'], b_descending=False)
        assert result == ['a', 'test', 'check', 'length']
        
    def test_regexes(self):
        s_text = 'test a length check'
        a_patterns = ["a", "length", "check", "to", "test"]
        a_regexes = [re.compile(r"\b{}\b".format(pattern))
                     for pattern in a_patterns]
        a_matches = [o_match
                     for o_re in a_regexes
                     for o_match in o_re.finditer(s_text)]
        print(a_matches)
        a_matches = FwText.sort_tokens_by_length(a_matches)
        result = [s_text[match.start():match.end()] for match in a_matches]
        assert result == ['length', 'check', 'test', 'a']
        
    def test_list_elements(self):
        result = FwText.sort_tokens_by_length(
            [(3, 'test'), (4, 'a'), (1, 'length'), (2, 'check')],
            index=1,
        )
        assert result == [(1, 'length'), (2, 'check'), (3, 'test'), (4, 'a')]


class TestUniqueSubsetStrings():
    def test_blank_list(self):
        result = FwText.unique_subset_strings(
            [],
            FwText.word_tokenize("Web exclusive brandnew red doll Web- red")
        )
        assert result == []

    def test_blank_tokens(self):
        result = FwText.unique_subset_strings(
            ['Web exclusive', 'exclusive'],
            FwText.word_tokenize("")
        )
        assert result == ['Web exclusive', 'exclusive']

    def test_overlapped(self):
        result = FwText.unique_subset_strings(
            ['Web exclusive', 'exclusive'],
            FwText.word_tokenize("Web exclusive brandnew red doll Web- red")
        )
        assert result == ['Web exclusive']

    def test_overlapped_and_separate(self):
        result = FwText.unique_subset_strings(
            ['Web exclusive', 'exclusive'],
            FwText.word_tokenize("Web exclusive brandnew red exclusive doll Web- red")
        )
        assert result == ['Web exclusive', 'exclusive']

    def test_separate(self):
        result = FwText.unique_subset_strings(
            ['Web exclusive', 'brandnew', 'exclusive'],
            FwText.word_tokenize("Web exclusive brandnew red exclusive doll Web- red")
        )
        assert result == ['Web exclusive', 'exclusive', 'brandnew']


class TestUniqueRegexResults():
    regexes = [re.compile(r'web\s*exclusive', re.I),
               re.compile(r'brand\s*new', re.I),
               re.compile('exclusive', re.I),]
                 
    def test_blank_list(self):
        result = FwText.unique_regex_results(
            [],
            "Web exclusive brandnew red doll Web- red"
        )
        assert result == []

    def test_blank_tokens(self):
        result = FwText.unique_regex_results(
            self.regexes,
            ""
        )
        assert result == []

    def test_overlapped(self):
        result = FwText.unique_regex_results(
            self.regexes,
            "Web exclusive brandnew red doll Web- red"
        )
        assert result == ['Web exclusive', 'brandnew']

    def test_overlapped_and_separate(self):
        result = FwText.unique_regex_results(
            self.regexes,
            "Web exclusive brandnew red exclusive doll Web- red"
        )
        assert result == ['Web exclusive', 'exclusive', 'brandnew']

    def test_separate(self):
        result = FwText.unique_regex_results(
            self.regexes,
            "Web exclusive brandnew red exclusive doll Web- red"
        )
        assert result == ['Web exclusive', 'exclusive', 'brandnew']


class TestExtractNLTKStems():
    def test_default_settings(self):
        result = FwText.extract_nltk_stems("Those Spartans are training with weights")
        assert result == "spartan train weight"

    def test_do_not_remove_stopwords(self):
        result = FwText.extract_nltk_stems("Those Spartans are training with weights",
            stop_words=None)
        assert result == "those spartan are train with weight"


class TestIsAlphaNumeric():
    def test_just_numbers(self):
        assert FwText.is_alpha_numeric("300") is False

    def test_just_letters(self):
        assert FwText.is_alpha_numeric("sparta") is False

    def test_letters_and_numbers(self):
        assert FwText.is_alpha_numeric("300spartans") is True

    def test_contains_spaces(self):
        assert FwText.is_alpha_numeric("300 spartans") is True

    def test_it_is_just_spaces(self):
        assert FwText.is_alpha_numeric("     ") is False

    def test_contains_punctuation(self):
        assert FwText.is_alpha_numeric("300 spartans!") is True

    def test_it_is_just_punctuation(self):
        assert FwText.is_alpha_numeric(":)") is False

    def test_non_ascii_are_not_letters(self):
        assert FwText.is_alpha_numeric("300 ñá") is False

    def test_empty_is_not_alphanumeric(self):
        assert FwText.is_alpha_numeric("") is False


class TestHasAlphaNumeri():
    def test_just_numbers(self):
        assert FwText.has_alpha_numeric("300") is True

    def test_just_letters(self):
        assert FwText.has_alpha_numeric("sparta") is True

    def test_letters_and_numbers(self):
        assert FwText.has_alpha_numeric("300spartans") is True

    def test_contains_spaces(self):
        assert FwText.has_alpha_numeric("300 spartans") is True

    def test_it_is_just_spaces(self):
        assert FwText.has_alpha_numeric("      ") is False

    def test_contains_punctuation(self):
        assert FwText.is_alpha_numeric("300 spartans!") is True

    def test_it_is_just_punctuation(self):
        assert FwText.is_alpha_numeric(":)") is False

    def test_non_ascii_are_not_letters(self):
        assert FwText.is_alpha_numeric("ñá") is False

    def test_empty_is_not_alphanumeric(self):
        assert FwText.is_alpha_numeric("") is False


class TestEditDistance():
    def test_case_sensitive(self):
        result = FwText.edit_distance("Sparta", "sparta")
        assert result == 1

    def test_case_insensitive(self):
        result = FwText.edit_distance("Sparta", "sparta", b_case_insensitive=True)
        assert result == 0


class TestDifferences():
    def test_case_difference(self):
        result = FwText.text_differences("This is Sparta", "this is sparta")
        assert result == {"case": 2}

    def test_punctuation_difference(self):
        result = FwText.text_differences("This is Sparta", "This is Sparta!")
        assert result == {"punctuation": 1}

    def test_spaces_are_punctuation(self): # Is this intended?
        result = FwText.text_differences("This is Sparta", "This is  Sparta")
        assert result == {"punctuation": 1}

    def test_letters_differences(self):
        result = FwText.text_differences("This is not Sparta", "This is Sparta")
        assert result["letters"] == 3 and result["punctuation"] == 1


class TestGetQuotedPhrases():
    def test_blank_tokenized(self):
        result = FwText.get_quoted_phrases([])
        assert result == []
    
    def test_blank_string(self):
        result = FwText.get_quoted_phrases('')
        assert result == []
    
    def test_tokenized(self):
        text = ['This', 'is', 'a', '"EMPIRE"', 'T-shirt', 'with', '8"', '"DARTH', 'VADER', '"']
        result = FwText.get_quoted_phrases(text)
        assert result == [['EMPIRE'], ['DARTH', 'VADER']]
    
    def test_string(self):
        text = 'This is a "STAR WARS" T-shirt with 8" "DARTH VADER "'
        result = FwText.get_quoted_phrases(text)
        assert result == ['STAR WARS', 'DARTH VADER']
    
    def test_apostrophe_string(self):
        text = "Home Accents' Centro dimension '1'6\" x 2'4\"' Doormat by 'Ashley \"test\" HomeStore'"
        result = FwText.get_quoted_phrases(text, use_apostrophes=True)
        assert result == ['1\'6" x 2\'4"', 'Ashley "test" HomeStore']
        result = FwText.get_quoted_phrases(text, use_apostrophes=False)
        assert result == ['test']
    
    def test_apostrophe_string_inside(self):
        text = "Sick Boy 'I Am A \"Sick\" Boy!' Sweatshirt 2XL Black"
        result = FwText.get_quoted_phrases(text, use_apostrophes=True)
        assert result == ['I Am A "Sick" Boy!']
        result = FwText.get_quoted_phrases(text, use_apostrophes=False)
        assert result == ['Sick']

    def test_apostrophe_start_abbreviation(self):
        text = "'round The Town Hoops by Free People, Gold, One Size"
        result = FwText.get_quoted_phrases(text, use_apostrophes=True)
        assert result == []


class TestGetQuotedPhraseWords():
    def test_blank_tokenized(self):
        result = FwText.get_quoted_phrase_words([])
        assert result == set()

    def test_blank_string(self):
        result = FwText.get_quoted_phrase_words('')
        assert result == set()
        
    def test_tokenized(self):
        text = ['This', 'is', 'a', '"STAR', 'WARS"', 'T-shirt', 'with', '8"', '"DARTH', 'VADER', '"']
        result = FwText.get_quoted_phrase_words(text)
        assert result == {'DARTH', 'STAR', 'VADER', 'WARS'}

    def test_string(self):
        text = 'This is a "STAR WARS" T-shirt with 8" "DARTH VADER "'
        result = FwText.get_quoted_phrase_words(text)
        assert result == {'DARTH', 'STAR', 'VADER', 'WARS'}


class TestGetQuotedPhrasesAndPos():
    def test_blank_tokenized(self):
        result = FwText.get_quoted_phrases_and_pos([])
        assert result == []
    
    def test_blank_string(self):
        result = FwText.get_quoted_phrases_and_pos('')
        assert result == []
    
    def test_tokenized(self):
        text = ['This', 'is', 'a', '"EMPIRE"', 'T-shirt', 'with', '8"', '"DARTH', 'VADER', '"']
        result = FwText.get_quoted_phrases_and_pos(text)
        assert result == [(('EMPIRE',), (3, 4)), (('DARTH', 'VADER'), (7, 9))]
        for t_phrase, t_pos in result:
            assert t_phrase == tuple([FwText.strip_quotes(word, b_unmatched=True) for word in text[t_pos[0]: t_pos[1]]])
    
    def test_string(self):
        text = 'This is a "STAR WARS" T-shirt with 8" "DARTH VADER "'
        result = FwText.get_quoted_phrases_and_pos(text)
        for t_phrase, t_pos in result:
            assert t_phrase == text[t_pos[0]: t_pos[1]]
        assert result == [('STAR WARS', (11, 20)), ('DARTH VADER ', (39, 51))]
        
    def test_apostrophe_string(self):
        text = "Home Accents' Centro dimension '1'6\" x 2'4\"' Doormat by 'Ashley \"test\" HomeStore'"
        result = FwText.get_quoted_phrases_and_pos(text)
        assert result == [('1\'6" x 2\'4"', (32, 43)), ('Ashley "test" HomeStore', (57, 80))]
        for t_phrase, t_pos in result:
            assert t_phrase == text[t_pos[0]: t_pos[1]]

    def test_apostrophe_string_inside(self):
        text = "Sick Boy 'I Am A \"Sick\" Boy!' Sweatshirt 2XL Black"
        result = FwText.get_quoted_phrases_and_pos(text)
        assert result == [('I Am A "Sick" Boy!', (10, 28))]
        for t_phrase, t_pos in result:
            assert t_phrase == text[t_pos[0]: t_pos[1]]
        
    def test_apostrophe_tokenized(self):
        text = ['Home', "Accents'", 'Centro', 'dimension', '\'1\'6"', 'x', '2\'4"\'', 'Doormat',
                'by', "'Ashley", '"test"', "HomeStore'"]
        result = FwText.get_quoted_phrases_and_pos(text)
        #for t_phrase, t_pos in result:
        #    assert t_phrase == tuple([FwText.strip_quotes(word, b_unmatched=True) for word in text[t_pos[0]: t_pos[1]]])
        assert result == [
            (('1\'6"', 'x', '2\'4"'), (4, 7)),
            (('Ashley', '"test"', 'HomeStore'), (9, 12))]

    def test_apostrophe_tokenized_inside(self):
        text = ['Sick', 'Boy', "'I", 'Am', 'A', '"Sick"', "Boy!'", 'Sweatshirt', '2XL', 'Black']
        result = FwText.get_quoted_phrases_and_pos(text)
        assert result == [(('I', 'Am', 'A', '"Sick"', 'Boy!'), (2, 7))]
        #for t_phrase, t_pos in result:
        #    assert t_phrase == tuple([FwText.strip_quotes(word, b_unmatched=True) for word in text[t_pos[0]: t_pos[1]]])
