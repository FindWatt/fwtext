﻿'''
Test cases for FwText/cleaning.pyx
'''
from __future__ import absolute_import
import pyximport; pyximport.install()
import string
import re
import pytest
import FwText


class TestTrim():
    def test_blank(self):
        result = FwText.trim("")
        assert result == ""
        
    def test_normal(self):
        result = FwText.trim("\t \r\nThis is a   test. \t ")
        assert result == "This is a test."


class TestRemoveHyphens():
    def test_with_spaces(self):
        result = FwText.remove_hyphens("This is a test - a really easy test")
        assert result == "This is a test   a really easy test"

    def test_equation(self):
        result = FwText.remove_hyphens("3-2=1")
        assert result == "3-2=1"

    def test_negative_number(self):
        result = FwText.remove_hyphens("It is -20 with windchill")
        assert result == "It is -20 with windchill"

    def test_keep_compound_word(self):
        result = FwText.remove_hyphens("Z-lon is a brand but slow-cooking is a phrase", b_dehyphenate_phrases=True)
        assert result == "Z-lon is a brand but slow cooking is a phrase"

    def test_keep_phrase(self):
        result = FwText.remove_hyphens("Z-lon is a brand but slow-cooking is a phrase", b_dehyphenate_phrases=False)
        assert result == "Z-lon is a brand but slow-cooking is a phrase"


class TestRemoveApostrophes():
    def test_time(self):
        result = FwText.remove_apostrophes("It's 11 o'clock")
        assert result == "It's 11 o'clock"

    def test_contraction(self):
        result = FwText.remove_apostrophes("This won't work")
        assert result == "This won't work"

    def test_single_quotes(self):
        result = FwText.remove_apostrophes("In 'extreme' cases")
        assert result == "In extreme cases"
        
    def test_single_quotes_angled(self):
        result = FwText.remove_apostrophes("It is a ‘bad’ idea")
        assert result == "It is a bad idea"

    def test_uom(self):
        result = FwText.remove_apostrophes("9' rug")
        assert result == "9' rug"

    def test_broken_uom(self):
        result = FwText.remove_apostrophes("9'' rug")
        assert result == "9'' rug"


class TestRemovePossessives():
    def test_singular(self):
        result = FwText.remove_possessives("Virginia's temperatures")
        assert result == "Virginia temperatures"
        
    def test_singular(self):
        result = FwText.remove_possessives("boys' scouting program")
        assert result == "boys scouting program"

    def test_number(self):
        result = FwText.remove_possessives("This is 2017's wettest month")
        assert result == "This is 2017 wettest month"


class TestRemovePeriods():
    def test_keep_decimal_points(self):
        result = FwText.remove_periods("I still owe you $500.00")
        assert result == "I still owe you $500.00"

    def test_remove_leading_trailing_dots(self):
        result = FwText.remove_periods("...Some words...")
        assert result == "Some words"

    def test_custom_replace_value(self):
        result = FwText.remove_periods("The end.", s_replace="<period>")
        assert result == "The end<period>"


class TestFixSentencePeriods():
    def test_blank(self):
        result = FwText.fix_sentence_periods("")
        assert result == ""
        
    def test_single_period(self):
        result = FwText.fix_sentence_periods(".")
        assert result == "."
        
    def test_keep_decimal_points(self):
        result = FwText.fix_sentence_periods("I still owe you $500.00.It was due last week.")
        assert result == "I still owe you $500.00. It was due last week."
        
        result = FwText.fix_sentence_periods("This needs to be done A.S.A.P.but not by you.")
        assert result == "This needs to be done A.S.A.P. but not by you."

    def test_add_space(self):
        result = FwText.fix_sentence_periods("This is a test.These sentences were not split.")
        assert result == "This is a test. These sentences were not split."
    

class TestReplaceAmpersands():
    def test_blank(self):
        result = FwText.replace_ampersands("")
        assert result == ""
        
    def test_keep_abbreviation(self):
        result = FwText.replace_ampersands("A&W root beer")
        assert result == "A&W root beer"
        
    def test_keep_html_characters(self):
        result = FwText.replace_ampersands("McDonalds &trade;")
        assert result == "McDonalds &trade;"
        
    def test_remove_between_words(self):
        result = FwText.replace_ampersands("root beer&french fries & hamburger")
        assert result == "root beer and french fries and hamburger"
        
    def test_remove_leading_trailing(self):
        result = FwText.replace_ampersands("& test&")
        assert result == "test"
    

class TestReplaceWith():
    def test_blank(self):
        result = FwText.replace_with("")
        assert result == ""
        
    def test_normal(self):
        result = FwText.replace_with("Hamburger with french fries")
        assert result == "Hamburger with french fries"
        
    def test_space(self):
        result = FwText.replace_with("Hamburger w french fries")
        assert result == "Hamburger with french fries"
        
    def test_slash_space(self):
        result = FwText.replace_with("Hamburger w/ french fries")
        assert result == "Hamburger with french fries"
        
    def test_slash(self):
        result = FwText.replace_with("Hamburger w/french fries")
        assert result == "Hamburger with french fries"
        
    def test_start(self):
        result = FwText.replace_with("W/ french fries")
        assert result == "with french fries"
        
    def test_white(self):
        result = FwText.replace_with("Phone B/W Cord")
        assert result == "Phone B/W Cord"


class TestRemoveTrademarks():
    def test_blank(self):
        result = FwText.remove_trademarks("")
        assert result == ""
        
    def test_normal(self):
        result = FwText.remove_trademarks("Aragorn® Lord of the Rings© McDonalds™ Cup")
        assert result == "Aragorn Lord of the Rings McDonalds Cup"
        
    def test_suffix(self):
        result = FwText.remove_trademarks("MicrosoftTM Office")
        assert result == "Microsoft Office"
        
    def test_abbreviation(self):
        result = FwText.remove_trademarks("Drive through ATM receipt")
        assert result == "Drive through ATM receipt"
    

class TestRemoveSlashes():
    def test_blank(self):
        result = FwText.remove_slashes("")
        assert result == ""
        
    def test_remove_slashes(self):
        result = FwText.remove_slashes("Black / White Tshirt in S/M/L")
        assert result == "Black  White Tshirt in S M L"
        
    def test_keep_fractions(self):
        result = FwText.remove_slashes("1 / 2 off 1/4 bits")
        assert result == "1 / 2 off 1/4 bits"

    

class TestRemoveHtmlCharacters():
    def test_blank(self):
        result = FwText.remove_html_characters("")
        assert result == ""
        
    def test_false_positive(self):
        result = FwText.remove_html_characters("A&W rootbeer; 12oz.")
        assert result == "A&W rootbeer; 12oz."
        
    def test_remove_html_characters(self):
        result = FwText.remove_html_characters("&gt;&gt;McDonalds &trade;")
        assert result == "McDonalds "
    

class TestReplaceHtmlCharacters():
    def test_blank(self):
        result = FwText.replace_html_characters("")
        assert result == ""
        
    def test_replace_html_characters(self):
        result = FwText.replace_html_characters(r"A&amp;W&trade; rootbeer; 12oz.&apos;")
        print(result)
        assert result == r"A&W™ rootbeer; 12oz.'"


class TestStripUnicodeAccents():
    def test_default_settings(self):
        result = FwText.strip_unicode_accents("áàäâ")
        assert result == "aaaa"


class TestStripQuotes():
    def test_blank(self):
        result = FwText.strip_quotes('')
        assert result == ''
    
    def test_single_quote(self):
        result = FwText.strip_quotes('"')
        assert result == '"'
    
    def test_double_quote(self):
        result = FwText.strip_quotes('""')
        assert result == '""'
    
    def test_normal(self):
        result = FwText.strip_quotes('"a"')
        assert result == 'a'
    
    def test_unmatched(self):
        result = FwText.strip_quotes('"test')
        assert result == '"test'
    
    def test_matched(self):
        result = FwText.strip_quotes('"test"', True)
        assert result == 'test'
    
    def test_start(self):
        result = FwText.strip_quotes('"test', True)
        assert result == 'test'
        
    def test_end(self):
        result = FwText.strip_quotes('test"', True)
        assert result == 'test'


class TestCleanText():
    def test_default_settings(self):
        result = FwText.clean_text("This is Spârtâ!")
        assert result == "This is Sparta!"

    def test_do_no_remove_accents(self):
        result = FwText.clean_text("This is Spârtâ!", b_accents=False)
        assert result == "This is Spârtâ!"

    def test_lowercase(self):
        result = FwText.clean_text("This is Sparta!", b_lowercase=True)
        assert result == "this is sparta!"

    def test_punctuation(self):
        # Missing test case for the punctuation defined by its bytes representation
        result = FwText.clean_text("remove these +(),; only", b_punctuation=True)
        assert result == "remove these only"

    def test_normalize_digits(self):
        result = FwText.clean_text(
            "They were 300 Spartans",
            b_normalize_digits=True
        )
        assert result == "They were 000 Spartans"

    def test_product_numbers(self):
        # This feature will be tested in depth in TestMPNPatterns
        result = FwText.clean_text("Product number 0.0.0", b_product_numbers=True)
        assert result == "Product number"

    def test_product_dimensions(self):
        # This feature will be tested in depth when we test
        # dimensions_tokenizers.tokenize_dimensions
        # For now this test will just make sure it works
        result = FwText.clean_text(
            "6 pack of Spartan drinks",
            b_product_dimensions=True
        )
        assert result == "6_pack_of Spartan drinks"

    def test_apostrophe(self):
        s_test = "Zukes' Zuke's Zuke`s Zuke{}s Zuke{}s".format(*list(b'\xe2\x80\x99\xc2\xb4'.decode("utf8")))
        result = FwText.clean_text(
            s_test,
            #b_apostrophes=True
        )
        assert result == "Zukes Zuke Zuke Zuke Zuke"


class TestCleanTitle():
    def test_blank(self):
        result = FwText.clean_title('')
        assert result == ''

    def test_hyphens(self):
        result = FwText.clean_title('This is a cleaning-test - for titles')
        assert result == 'This is a cleaning-test for titles'

    def test_apostrophes(self):
        result = FwText.clean_title("Tim's test for 'cleaning' titles")
        assert result == "Tim's test for cleaning titles"

    def test_periods(self):
        result = FwText.clean_title("Title Quality Analysis. Version 2.0")
        assert result == "Title Quality Analysis Version 2.0"

    def test_trademarks(self):
        result = FwText.clean_title("BankTM ATM Plaque™")
        assert result == "Bank ATM Plaque"

    def test_ampersands(self):
        result = FwText.clean_title("A&W Burger & Fries")
        assert result == "A&W Burger and Fries"

    def test_with(self):
        result = FwText.clean_title("Burger w / Fries")
        assert result == "Burger with Fries"

    def test_html_characters(self):
        result = FwText.clean_title("&apos;BODY&apos; T Shirt")
        assert result == "BODY T Shirt"

    def test_other_punctuation(self):
        result = FwText.clean_title("Pants &amp; T Shirt")
        assert result == "Pants and T Shirt"


class TestCleanDescription():
    def test_blank(self):
        result = FwText.clean_description('')
        assert result == ''

    def test_hyphens(self):
        result = FwText.clean_description('This is a cleaning-test - for titles')
        assert result == 'This is a cleaning-test for titles'

    def test_apostrophes(self):
        result = FwText.clean_description("Tim's test for 'cleaning' titles")
        assert result == "Tim's test for cleaning titles"

    def test_periods(self):
        result = FwText.clean_description("Title Quality Analysis. Version 2.0")
        assert result == "Title Quality Analysis Version 2.0"

    def test_trademarks(self):
        result = FwText.clean_description("BankTM ATM Plaque™")
        assert result == "Bank ATM Plaque"

    def test_ampersands(self):
        result = FwText.clean_description("A&W Burger & Fries")
        assert result == "A&W Burger and Fries"

    def test_with(self):
        result = FwText.clean_description("Burger w / Fries")
        assert result == "Burger with Fries"

    def test_html_characters(self):
        result = FwText.clean_description("&apos;BODY&apos; T Shirt")
        assert result == "'BODY' T Shirt"

    def test_other_punctuation(self):
        result = FwText.clean_description("Pants &amp; T Shirt")
        assert result == "Pants and T Shirt"


class TestCleanGroupOfDicts():
    def test_default_settings(self):
        test_dict = {"outter_key":{"Sôme-key1": "sóme$Value"}}
        result = FwText.clean_group_of_dicts(test_dict)
        assert result == {"outter_key":{"Some-key1":"some$Value"}}

    def test_lowercase(self):
        test_dict = {"outter_key":{"SOME_KEY":"SOME_VALUE"}}
        result = FwText.clean_group_of_dicts(test_dict, b_lowercase=True)
        assert result == {"outter_key":{"some_key":"some_value"}}

    def test_punctuation(self):
        # This feature should be fully tested on TestCleanText.test_punctuation
        test_dict = {
            "outter_key":{
                "some;key":"some+value",
                "another(key":"another)value"
            }
        }
        result = FwText.clean_group_of_dicts(test_dict, b_punctuation=True)
        assert result == {
            "outter_key":{
                "some key":"some value",
                "another key":"another value"
                }
            }

    def test_normalize_digits(self):
        test_dict = {
            "outter_key":{
                "key_one":"1",
                "key_twenty":"20"
            }
        }
        result = FwText.clean_group_of_dicts(test_dict, b_normalize_digits=True)
        assert result == {
            "outter_key":{
                "key_one":"0",
                "key_twenty":"00"
            }
        }

    def test_product_numbers(self):
        # This feature is tested in depth in TestMPNPatterns
        pass

    def test_list_of_dicts(self):
        test_list = [{"some;key":"some+value", "another(key":"another)value"},
            {"some;key":"some+value", "another(key":"another)value"}]
        result = FwText.clean_group_of_dicts(test_list, b_punctuation=True)
        assert result == [{"some key":"some value", "another key":"another value"},
            {"some key":"some value", "another key":"another value"}]

    def test_do_not_support_single_dictionary(self):
        test_dict = {"some;key":"some+value", "another(key":"another)value"}
        with pytest.raises(TypeError) as error_info:
            result = FwText.clean_group_of_dicts(test_dict)
        assert error_info.value.__str__() == "Expected dict, got str"


class TestCleanList():
    def test_default_settings(self):
        result = FwText.clean_list(["This is Spârtá!", "Thât is àlso Sparta."])
        assert result == ["This is Sparta!", "That is also Sparta."]

    def test_do_not_remove_accents(self):
        result = FwText.clean_list(["This is Spàrta!"], b_accents=False)
        assert result == ["This is Spàrta!"]

    def test_lowercase(self):
        result = FwText.clean_list(["This is Sparta!", "That is Sparta"], b_lowercase=True)
        assert result == ["this is sparta!", "that is sparta"]

    def test_punctuation(self):
        # This feature should be fully tested in TestCleanText.test_punctuation
        result = FwText.clean_list(["This+is+Sparta!"], b_punctuation=True)
        assert result == ["This is Sparta!"]

    def test_normalize_digits(self):
        result = FwText.clean_list(["They were 300 Spartans"], b_normalize_digits=True)
        assert result == ["They were 000 Spartans"]

    def test_product_numbers(self):
        # This feature should be tested in depth in TestMPNPatterns
        pass
        

class TestRemoveAllPunctuation():
    def test_default_settings(self):
        result = FwText.remove_all_punctuation("This. Is. Sparta{}".format(
            string.punctuation))
        assert result == "This Is Sparta"

    def test_do_not_keep_spaces(self):
        result = FwText.remove_all_punctuation("This. Is. Sparta{}".format(
            string.punctuation), b_keepspaces=False)
        assert result == "ThisIsSparta"


class TestRemoveStopwords():
    def test_default_settings(self):
        result = FwText.remove_stopwords(["This", "is", "Sparta"])
        assert result == ["Sparta"]

    def test_do_not_remove_stopwords(self):
        result = FwText.remove_stopwords(["This", "is", "Sparta"], stop_words=None)
        assert result == ["This", "is", "Sparta"]

    def test_custom_stopwords(self):
        result = FwText.remove_stopwords(["This", "is", "Sparta"],
            stop_words=["Sparta"])
        assert result == ["This", "is"]

    def test_custom_stopwords_case_insensitive(self):
        result = FwText.remove_stopwords(["This", "is", "Sparta"],
            stop_words=["sparta"])
        assert result == ["This", "is"]


class TestMPNPatterns():
    def test_pattern_mpn_1(self):
        pattern = FwText.text.a_MPN_PATTERNS[0]
        assert re.search(pattern, "ab0a0")
        assert re.search(pattern, "a0a0") is None
        assert re.search(pattern, "abc0abc0")
        assert re.search(pattern, "ab-0-ab0")
        assert re.search(pattern, "ab-000-ab0")
        assert re.search(pattern, "ab-0-ab0-ab0")
        assert re.search(pattern, "ab-0-ab0ab0")
        assert re.search(pattern, "ab00ab00")
        assert re.search(pattern, "#ab0ab0")

    def test_pattern_mpn_2(self):
        pattern = FwText.text.a_MPN_PATTERNS[1]
        assert re.search(pattern, "ab0a")
        assert re.search(pattern, "#ab0a")
        assert re.search(pattern, "abc0a")
        assert re.search(pattern, "a0a") is None
        assert re.search(pattern, "aba") is None
        assert re.search(pattern, "ab-0a")
        assert re.search(pattern, "ab-0-a")
        assert re.search(pattern, "ab00a")
        assert re.search(pattern, "ab0-a")
        assert re.search(pattern, "ab0ab")
        assert re.search(pattern, "ab0") is None
        assert re.search(pattern, "ab0a0a")

    def test_pattern_mpn_3(self):
        pattern = FwText.text.a_MPN_PATTERNS[2]
        assert re.search(pattern, "ab0a")
        assert re.search(pattern, "#ab0a")
        assert re.search(pattern, "a0a") is None
        assert re.search(pattern, "abc0a")
        assert re.search(pattern, "ab-0a")
        assert re.search(pattern, "ab00a")
        assert re.search(pattern, "aba") is None
        assert re.search(pattern, "ab0") is None
        assert re.search(pattern, "ab0aa")
        assert re.search(pattern, "ab-0a-0a")

    def test_pattern_mpn_4(self):
        pattern = FwText.text.a_MPN_PATTERNS[3]
        assert re.search(pattern, "ab0")
        assert re.search(pattern, "#ab0")
        assert re.search(pattern, "abc0")
        assert re.search(pattern, "a0") is None
        assert re.search(pattern, "ab-0")
        assert re.search(pattern, "ab") is None
        assert re.search(pattern, "ab00")

    def test_pattern_mpn_5(self):
        pattern = FwText.text.a_MPN_PATTERNS[4]
        assert re.search(pattern, "0000")
        assert re.search(pattern, "#0000")
        assert re.search(pattern, "000") is None
        assert re.search(pattern, "00000")

    def test_pattern_mpn_6(self):
        pattern = FwText.text.a_MPN_PATTERNS[5]
        assert re.search(pattern, "00abc0a0")
        assert re.search(pattern, "#00abc0a0")
        assert re.search(pattern, "0abc0a0") is None
        assert re.search(pattern, "00-abc0a0")
        assert re.search(pattern, "00-abc-0-0")
        assert re.search(pattern, "00abcd0a0")
        assert re.search(pattern, "00ab0a0") #is None
        assert re.search(pattern, "00in-0in") is None
        assert re.search(pattern, "00abc-0a0")
        assert re.search(pattern, "00abc00a0")
        assert re.search(pattern, "00abca0") #is None
        assert re.search(pattern, "00abc0-a0")
        assert re.search(pattern, "00abc0aa0")
        assert re.search(pattern, "00abc00") #is None
        assert re.search(pattern, "00abc0a-0")
        assert re.search(pattern, "00abc0a") #is None
        assert re.search(pattern, "00abc0a00")

    def test_pattern_mpn_7(self):
        pattern = FwText.text.a_MPN_PATTERNS[6]
        assert re.search(pattern, "00abc")
        assert re.search(pattern, "#00abc")
        assert re.search(pattern, "0abc") is None
        assert re.search(pattern, "000abc")
        assert re.search(pattern, "00-abc")
        assert re.search(pattern, "00abcd")
        assert re.search(pattern, "00ab") is None

    def test_pattern_mpn_8(self):
        pattern = FwText.text.a_MPN_PATTERNS[7]
        assert re.search(pattern, "a0a0")
        assert re.search(pattern, "aa0a0")
        assert re.search(pattern, "0a0") is None
        assert re.search(pattern, "a00a0")
        assert re.search(pattern, "aa0") is None
        assert re.search(pattern, "a0aa0")
        assert re.search(pattern, "a00") is None
        assert re.search(pattern, "a0a00")
        assert re.search(pattern, "a0a") is None
        assert re.search(pattern, "a0a0a0")

    def test_pattern_mpn_9(self):
        pattern = FwText.text.a_MPN_PATTERNS[8]
        assert re.search(pattern, "0.0.0")
        assert re.search(pattern, "00.0.0")
        assert re.search(pattern, ".0.0") is None
        assert re.search(pattern, "0.00.0")
        assert re.search(pattern, "0..0") is None
        assert re.search(pattern, "0.0.00")
        assert re.search(pattern, "0.0.") is None
        assert re.search(pattern, "0.0.0.0")
