﻿'''
Test cases for FwText/ngrams.pyx
'''
from __future__ import absolute_import
import pyximport; pyximport.install()
import string
import re
import pytest
import FwText


class TestAllNGrams():
    def test_default_settings(self):
        result = FwText.all_ngrams("This is Sparta!")
        assert result == [["Sparta"]]

    def test_do_not_remove_stopwords(self):
        result = FwText.all_ngrams("This is Sparta", stop_words=None)
        assert result == [
            ["This is Sparta"],
            ["This is", "is Sparta"],
            ["This", "is", "Sparta"]
        ]

    def test_limit_max_ngram_length(self):
        result = FwText.all_ngrams("This is Sparta", stop_words=None, i_max_size=2)
        assert result == [["This is", "is Sparta"], ["This", "is", "Sparta"]]

    def test_limit_min_ngram_length(self):
        result = FwText.all_ngrams("This is Sparta", stop_words=None, i_min_size=2)
        assert result == [["This is Sparta"], ["This is", "is Sparta"]]

    def test_do_not_remove_punctuation(self):
        result = FwText.all_ngrams("This is Sparta!", remove_punct=False)
        assert result == [["Sparta!"]]


class TestAllNGramsLists():
    def test_default_settings(self):
        result = FwText.all_ngrams_lists("This is Sparta!")
        assert result == [[("Sparta",)]]

    def test_do_not_remove_stopwords(self):
        result = FwText.all_ngrams_lists("This is Sparta", stop_words=None)
        assert result == [
            [("This", "is", "Sparta")],
            [("This", "is"), ("is", "Sparta")],
            [("This",), ("is",), ("Sparta",)]
        ]

    def test_limit_max_ngram_length(self):
        result = FwText.all_ngrams_lists("This is Sparta", stop_words=None, i_max_size=2)
        assert result == [
            [("This", "is"), ("is", "Sparta")],
            [("This",), ("is",), ("Sparta",)]
        ]

    def test_limit_min_ngram_length(self):
        result = FwText.all_ngrams_lists("This is Sparta", stop_words=None, i_min_size=2)
        assert result == [
            [("This", "is", "Sparta")],
            [("This", "is"), ("is", "Sparta")]
        ]

    def test_do_not_remove_punctuation(self):
        result = FwText.all_ngrams_lists("This is Sparta!", remove_punct=False)
        assert result == [[("Sparta!",)]]


class TestAllNGramsFlat():
    def test_default_settings(self):
        result = FwText.all_ngrams_flat("This is Sparta!")
        assert result == ["Sparta"]

    def test_do_not_remove_stopwords(self):
        result = FwText.all_ngrams_flat("This is Sparta", stop_words=None)
        assert result == ["This is Sparta", "This is", "is Sparta",
            "This", "is", "Sparta"]

    def test_limit_max_ngram_length(self):
        result = FwText.all_ngrams_flat("This is Sparta", stop_words=None, i_max_size=2)
        assert result == ["This is", "is Sparta", "This", "is", "Sparta"]

    def test_limit_min_ngram_length(self):
        result = FwText.all_ngrams_flat("This is Sparta", stop_words=None, i_min_size=2)
        assert result == ["This is Sparta", "This is", "is Sparta"]

    def test_do_not_remove_punctuation(self):
        result = FwText.all_ngrams_flat("This is Sparta!", remove_punct=False)
        assert result == ["Sparta!"]


class TestAllNGramsTuples():
    def test_default_settings(self):
        result = FwText.all_ngrams_tuples("This is Sparta!")
        assert result == [("Sparta",)]

    def test_do_not_remove_stopwords(self):
        result = FwText.all_ngrams_tuples("This is Sparta", stop_words=None)
        assert result == [("This", "is", "Sparta"), ("This", "is"), ("is", "Sparta"),
            ("This",), ("is",), ("Sparta",)]

    def test_limit_max_ngram_length(self):
        result = FwText.all_ngrams_tuples("This is Sparta", stop_words=None, i_max_size=2)
        assert result == [("This", "is"), ("is", "Sparta"), ("This",), ("is",), ("Sparta",)]

    def test_limit_min_ngram_length(self):
        result = FwText.all_ngrams_tuples("This is Sparta", stop_words=None, i_min_size=2)
        assert result == [("This", "is", "Sparta"), ("This", "is"), ("is", "Sparta")]

    def test_do_not_remove_punctuation(self):
        result = FwText.all_ngrams_tuples("This is Sparta!", remove_punct=False)
        assert result == [("Sparta!",)]


class TestEndGrams():
    def test_blank(self):
        result = FwText.end_grams("")
        assert result == []
        
    def test_string(self):
        result = FwText.end_grams("This is a test")
        assert result == ["This is a test", "is a test", "a test", "test"]
        
    def test_list(self):
        result = FwText.end_grams(["This", "is", "a", "test"])
        assert result == [("This", "is", "a", "test"), ("is", "a", "test"), ("a", "test"), ("test", )]
        
    def test_list_to_string(self):
        result = FwText.end_grams(["This", "is", "a", "test"], return_tuples=False)
        assert result == ["This is a test", "is a test", "a test", "test"]
        

class TestSkipBigrams():
    def test_blank(self):
        result = FwText.skip_bigrams("")
        assert result == []
        
    def test_bigrams(self):
        result = FwText.skip_bigrams("This is a test", i_max_dist=4, i_min_dist=1, stop_words=None, remove_punct=None)
        assert result == [('This', 'is'), ('This', 'a'), ('This', 'test'),
                          ('is', 'a'), ('is', 'test'), ('a', 'test')]
        
    def test_skip_only(self):
        result = FwText.skip_bigrams("This is a test", i_max_dist=4, i_min_dist=2, stop_words=None, remove_punct=None)
        assert result == [('This', 'a'), ('This', 'test'), ('is', 'test')]


class TextSkipGrams():
    def test_blank(self):
        a_expected = []
        assert a_expected == FwText.skip_grams([])
    
    def test_unigram(self):
        a_expected = [('test',)]
        assert a_expected == FwText.skip_grams('test')
    
    def test_bigram(self):
        a_expected = [('test', 'skip'), ('test',), ('skip',)]
        assert a_expected == FwText.skip_grams(['test', 'skip'])
    
    def test_bigram(self):
        a_expected = [
            ('test', 'skip', 'generator'),
            ('test', 'skip'), ('skip', 'generator'),
            ('test',), ('skip',), ('generator',),
            ('test', 'generator')
        ]
        assert a_expected == FwText.skip_grams(['test', 'skip', 'generator'])
    
    def test_normal(self):
        a_expected = [
            ('test', 'skip', 'generator', 'function', 'out'),
            ('test', 'skip', 'generator', 'function'), ('skip', 'generator', 'function', 'out'),
            ('test', 'skip', 'generator'), ('skip', 'generator', 'function'), ('generator', 'function', 'out'),
            ('test', 'skip'), ('skip', 'generator'), ('generator', 'function'), ('function', 'out'),
            ('test',), ('skip',), ('generator',), ('function',), ('out',),
            ('test', 'generator', 'function', 'out'),
            ('test', 'skip', 'function', 'out'), ('test', 'skip', 'generator', 'out'),
            ('test', 'function', 'out'), ('test', 'skip', 'out'),
            ('test', 'generator', 'function'), ('test', 'skip', 'function'),
            ('skip', 'function', 'out'), ('skip', 'generator', 'out'),
            ('test', 'out'), ('test', 'function'), ('skip', 'out'),
            ('test', 'generator'), ('skip', 'function'), ('generator', 'out')
        ]
        assert a_expected == FwText.skip_grams(['test', 'skip', 'generator', 'function', 'out'])

class TextCharGrams():
    def test_blank(self):
        assert [] == char_ngrams('')
        assert [] == char_ngrams([])
    
    def test_word(self):
        a_expected = [
            ('t', 'e', 's', 't'),
            ('t', 'e', 's'), ('e', 's', 't'),
            ('t', 'e'), ('e', 's'), ('s', 't'),
            ('t',), ('e',), ('s',), ('t',)]
        assert a_expected == char_ngrams('test')
    
    def test_indexes(self):
        a_expected = [(1, 2, 3), (2, 3, 4), (1, 2), (2, 3), (3, 4)]
        assert a_expected == char_ngrams([1, 2, 3, 4], 3, 2)


class TestTextListToNGramList():
    def test_default_settings(self):
        result = FwText.text_list_to_ngram_list([
            "Sparta is full of Spartans",
            "This is Sparta"
        ])
        assert result == [
            ["sparta full spartans", "sparta full", "full spartans",
            "sparta", "full", "spartans"],
            ["sparta"]
        ]

    def test_max_size(self):
        result = FwText.text_list_to_ngram_list(
            ["Sparta is full of Spartans", "This is Sparta!"],
            i_max_size=2)
        assert result == [
            ["sparta full", "full spartans", "sparta", "full", "spartans"],
            ["sparta"]
        ]

    def test_min_size(self):
        result = FwText.text_list_to_ngram_list(
            ["Sparta is full of Spartans", "This is Sparta!"],
            i_min_size=2)
        assert result == [
            ["sparta full spartans", "sparta full", "full spartans"],
            []
        ]

    def test_do_not_remove_stopwords(self):
        result = FwText.text_list_to_ngram_list(
            ["Sparta is full of Spartans", "This is Sparta!"],
            stop_words=None)
        assert result == [
            ["sparta is full of spartans", "sparta is full of",
            "is full of spartans", "sparta is full", "is full of",
            "full of spartans", "sparta is", "is full", "full of",
            "of spartans", "sparta", "is", "full", "of", "spartans"],
            ["this is sparta", "this is", "is sparta", "this", "is",
            "sparta"]
        ]

    def test_remove_punctuation(self):
        result = FwText.text_list_to_ngram_list(
            ["This is Sparta!"], remove_punctuation=False)
        assert result == [["sparta!"]]

    def test_case_sensitive(self):
        result = FwText.text_list_to_ngram_list(
            ["This is Sparta"], b_case_insensitive=False)
        assert result == [["Sparta"]]

    def test_return_tuples(self):
        result = FwText.text_list_to_ngram_list(
            ["This is Sparta"], b_return_tuples=True)
        assert result == [[("sparta",)]]



class TestNGramFreqCoverageInList():
    def test_default_behaviour(self):
        freq, coverage, max_count = FwText.ngram_freq_coverage_in_list([
            ["this", "is", "Sparta"],
            ["that", "is", "Sparta"],
            ["this", "is", "this", "good"]
        ])
        # verify frequency results
        assert freq["this"] == 3
        assert freq["is"] == 3
        assert freq["Sparta"] == 2
        assert freq["that"] == 1
        assert freq["good"] == 1
        # verify coverage results
        assert coverage["this"] == 2
        assert coverage["is"] == 3
        assert coverage["Sparta"] == 2
        assert coverage["that"] == 1
        assert coverage["good"] == 1
        # verify max_count result
        assert max_count["this"] == 2
        assert max_count["is"] == 1
        assert max_count["Sparta"] == 1
        assert max_count["that"] == 1
        assert max_count["good"] == 1

    def test_single_string_not_wrapped_as_list(self):
        with pytest.raises(TypeError) as error_info:
            result = FwText.ngram_freq_coverage_in_list(["This", "is","Sparta"])
            assert error_info.__str__() == "Expected list, got str"

    def test_single_string_wrapped_as_list(self):
        freq, coverage, max_count = FwText.ngram_freq_coverage_in_list([
            ["This", "is", "Sparta"]
        ])
        assert sum(freq.values()) == 3
        assert sum(coverage.values()) == 3
        assert sum(max_count.values()) == 3

    def test_case_sensitivity(self):
        freq, coverage, max_count = FwText.ngram_freq_coverage_in_list([
            ["This", "and", "this"]
        ])
        assert len(freq.keys()) == len(coverage.keys()) == len(max_count.keys()) == 3

class TestNGramFreqInList():
    def test_default_behaviour(self):
        result = FwText.ngram_freq_in_list([
            ["this", "is", "sparta"],
            ["this", "is", "not", "sparta"]
        ])
        assert result["this"] == 2
        assert result["is"] == 2
        assert result["not"] == 1
        assert result["sparta"] == 2


class TestTupleNGramFreqInList():
    def test_default_behaviour(self):
        result = FwText.tuple_ngram_freq_in_list([
            [("this",), ("is",), ("sparta",)]
        ])
        assert result[("this",)] == 1
        assert result[("is",)] == 1
        assert result[("sparta",)] == 1

    def test_fail_if_not_tuples(self):
        with pytest.raises(TypeError) as error_info:
            result = FwText.tuple_ngram_freq_in_list([
                ["This", "is", "Sparta"]
            ])
            expected_error = "Expected tuple, got str"
            assert error_info.__str__() == expected_error


class TestNgramCoverageInList():
    def test_default_behavior(self):
        result = FwText.ngram_coverage_in_list([
            ["this", "is", "Sparta"],
            ["this", "is", "also", "Sparta"]
        ])
        assert result["this"] == 2
        assert result["is"] == 2
        assert result["also"] == 1
        assert result["Sparta"] == 2

    def test_case_sensitivity(self):
        result = FwText.ngram_coverage_in_list([
            ["This", "and", "this"]
        ])
        assert len(result.keys()) == 3


class TestTupleNGramCoverageInList():
    def test_default_behavior(self):
        result = FwText.tuple_ngram_coverage_in_list([
            [("this",), ("is",), ("sparta",)],
            [("that",), ("is",), ("sparta",)]
        ])
        assert result[("this",)] == 1
        assert result[("is",)] == 2
        assert result[("sparta",)] == 2
        assert result[("that",)] == 1

    def test_case_sensitivity(self):
        result = FwText.tuple_ngram_coverage_in_list([
            [("This",), ("and",), ("this",)]
        ])
        assert len(result.keys()) == 3

    def test_fail_if_not_tuples(self):
        with pytest.raises(TypeError) as error_info:
            result = FwText.tuple_ngram_coverage_in_list([
                ["This", "is", "Sparta"]
            ])
            expected_error = "Expected tuple, got str"
            assert error_info.__str__() == expected_error

    
class TestNgramTable():
    def test_default_settings(self):
        result = FwText.ngram_table(["This is Sparta", "That is Sparta"])
        assert result == [(0, ("Sparta",), 2, 2, 0, 1)]

    def test_do_not_remove_stopwords(self):
        result = FwText.ngram_table(
            ["This is Sparta", "That is Sparta"],
            stop_words = None
        )
        assert len(result) == 9

    def test_max_ngram_size(self):
        result = FwText.ngram_table(
            ["This is Sparta", "That is Sparta"],
            stop_words = None, i_max_size=2
        )
        assert len(result) == 7

    def test_min_ngram_size(self):
        result = FwText.ngram_table(
            ["This is Sparta", "That is Sparta"],
            stop_words = None, i_min_size=2
        )
        assert len(result) == 5

    def test_do_not_remove_punctuation(self):
        result = FwText.ngram_table(["This is Sparta!"],
            remove_punctuation=False)
        assert result == [(0, ("Sparta!",), 1, 1, 0, 1)]

    def test_case_sensitive(self):
        result = FwText.ngram_table(["Sparta sparta"],
            b_case_insensitive=False)
        assert len(result) == 3 # "Sparta", "sparta", "Sparta sparta"


class TestNGramCount():
    ngrams = {
        ("test", ): 1, ("tests", ): 1,
        ("ngramtest", ): 1, ("ngramtests", ): 1,
        ("ngram-test", ): 1, ("ngram-tests", ): 1,
        ("ngram_test", ): 1, ("ngram_tests", ): 1,
        ("ngram", "test"): 1, ("ngram", "tests"): 1,
    }

    def test_blank(self):
        result = FwText.get_ngram_count(tuple([]), self.ngrams)
        assert result == 0

    def test_singular(self):
        result = FwText.get_ngram_count(('test', ), self.ngrams)
        assert result == 2

    def test_plural(self):
        result = FwText.get_ngram_count(('tests', ), self.ngrams)
        assert result == 2

    def test_compound(self):
        result = FwText.get_ngram_count(('ngramtest', ), self.ngrams)
        assert result == 8

    def test_bigram(self):
        result = FwText.get_ngram_count(('ngram', 'test'), self.ngrams)
        assert result == 8


class TestCombinePluralSingularStems():
    ngrams = {
        ("test", ): 1, ("tests", ): 1,
        ("ngramtest", ): 1, ("ngramtests", ): 1,
        ("ngram-test", ): 1, ("ngram-tests", ): 1,
        ("ngram_test", ): 1, ("ngram_tests", ): 1,
        ("ngram", "test"): 1, ("ngram", "tests"): 1,
    }
    
    def test_blank(self):
        result = FwText.combine_plural_singular_stems({})
        assert result == {}

    def test_combine(self):
        result = FwText.combine_plural_singular_stems(self.ngrams)
        print(result)
        assert result[('test', )] == 2
        assert result[('tests', )] == 2
        assert result[('ngramtest', )] == 2
        assert result[('ngramtests', )] == 2
        assert result[('ngram-test', )] == 2
        assert result[('ngram-tests', )] == 2
        assert result[('ngram_test', )] == 2
        assert result[('ngram_tests', )] == 2
        assert result[('ngram', 'test')] == 2
        assert result[('ngram', 'tests')] == 2


class TestIsCompoundWord():
    ngrams = {
        ("singular", ): 1,
        ("compoundword", ): 1,
        ("compound", "word"): 1,
        ("compound", ): 1,
        ("word"): 1,
        ("asits"): 1,
        ("as"): 1,
        ("its"): 1,
    }
    
    def test_blank(self):
        assert FwText.is_compound_word('', self.ngrams) == False

    def test_plain(self):
        result = FwText.is_compound_word('singular', self.ngrams)
        assert result == False

    def test_compound(self):
        result = FwText.is_compound_word('compoundword', self.ngrams)
        assert result == True

    def test_short(self):
        result = FwText.is_compound_word('asits', self.ngrams, i_start=3)
        assert result == False

    def test_short_override(self):
        result = FwText.is_compound_word('asits', self.ngrams, i_start=1)
        assert result == True


class TestCompoundWordInNgrams():
    ngrams = {
        ("singular", ): 1,
        ("compoundword", ): 1,
        ("compound", "word"): 1,
        ("compound", ): 1,
        ("word", ): 1,
        ("asits", ): 1,
        ("as", ): 1,
        ("its", ): 1,
    }
    
    def test_blank_text(self):
        assert not FwText.compound_word_in_ngrams("", self.ngrams)
    
    def test_blank_dict(self):
        assert not FwText.compound_word_in_ngrams("compoundword", {})
    
    def test_compound(self):
        assert ("compound", "word") == FwText.compound_word_in_ngrams("compoundword", self.ngrams)


class TestGetCompounds():
    ngrams = {
        ("singular", ): 1,
        ("compoundword", ): 1,
        ("compound", "word"): 1,
        ("compound", ): 1,
        ("word", ): 1,
        ("asits", ): 1,
        ("as", ): 1,
        ("its", ): 1,
    }

    def test_blank(self):
        assert FwText.get_compounds({}, e_ngrams=self.ngrams) == {}

    def test_compounds(self):
        result = FwText.get_compounds(self.ngrams)
        assert result == {
            ("compoundword", ): ("compound", "word"),
        }


class TestFreqDictToLower():
    def test_default_settings(self):
        # Use FwText.FwText because this function is not exposed
        # in the external FwText
        result = FwText.text.freq_dict_to_lower({"This":1, "is":1, "SPARTA":1})
        # Not sure if this is the intended behavior
        assert len(result) == 3
        assert result["this"] == 1
        assert result["is"] == 1
        assert result["sparta"] == 1


class TestInsertPhrase():
    text = ["Test", "this", "Function", "out"]
    phrase = ("new", "Insert", "function", "thing")
    
    def test_blank_text(self):
        result = FwText.insert_phrase([], self.phrase)
        assert self.phrase == list(result)
    
    def test_blank_text(self):
        result = FwText.insert_phrase(self.text[:], tuple())
        assert self.text == result
    
    def test_normal(self):
        expected = ['Test', 'this', 'new', 'Insert', 'Function', 'thing', 'out']
        result = FwText.insert_phrase(self.text[:], self.phrase)
        assert expected == result
    
    def test_append(self):
        phrase = ['add', 'at', 'end']
        expected = ["Test", "this", "Function", "out"] + phrase
        result = FwText.insert_phrase(self.text[:], tuple(phrase))
        assert expected == result
    
    def test_prepend(self):
        phrase = ['add', 'at', 'start']
        expected = phrase + ["Test", "this", "Function", "out"]
        result = FwText.insert_phrase(self.text[:], tuple(phrase), b_prepend=True)
        assert expected == result


class TestUniqueNgramsToPhrase():
    ngrams = [
        ('henley', 'silver', 'hoodie'),
        ('henley', 'hoodie'),
        ('hoodie', 'mens'),
        ('extra', 'large'),
        ("flat", "silver"),
        ('hoodie',),]
    
    def test_blank(self):
        result = FwText.unique_ngrams_to_phrase([])
        assert not result
    
    def test_normal(self):
        expected = ['henley', 'flat', 'silver', 'hoodie', 'mens', 'extra', 'large',]
        result = FwText.unique_ngrams_to_phrase(self.ngrams[:])
        assert expected == result
    
    def test_prepend(self):
        expected = ['extra', 'large', 'henley', 'flat', 'silver', 'hoodie', 'mens',]
        result = FwText.unique_ngrams_to_phrase(self.ngrams[:], b_prepend=True)
        assert expected == result
    
    def test_no_merge(self):
        expected = ['henley', 'silver', 'hoodie', 'mens', 'extra', 'large', 'flat']
        result = FwText.unique_ngrams_to_phrase(self.ngrams[:], b_merge=False)
        assert expected == result

    def test_no_merge_prepend(self):
        expected = ['flat', 'extra', 'large', 'mens', 'henley', 'silver', 'hoodie']
        result = FwText.unique_ngrams_to_phrase(self.ngrams[:], b_merge=False, b_prepend=True)
        assert expected == result
