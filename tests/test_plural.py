﻿'''
Test cases for FwText/plural.pyx
'''
from __future__ import absolute_import
import pyximport; pyximport.install()
import string
import re
import pytest
import FwText


class TestAllPluralSingular():
    # The capitalization of the results is inconsisten with singularize
    def test_blank(self):
        assert not FwText.all_plural_singular(set())
    
    def test_singular(self):
        c_words = FwText.all_plural_singular({
            "singular", "word", "test"
        })
        assert "singulars" in c_words
        assert "words" in c_words
        assert "tests" in c_words

    def test_plural(self):
        c_words = FwText.all_plural_singular({
            "plurals", "words", "tests"
        })
        assert "plural" in c_words
        assert "word" in c_words
        assert "test" in c_words


class TestPluralize():
    # The capitalization of the results is inconsisten with singularize
    def test_blank(self):
        assert FwText.pluralize("") is None
        
    def test_this_is_sparta(self):
        assert FwText.pluralize("this") == "these"
        assert FwText.pluralize("is") == "are"
        assert FwText.pluralize("Sparta") == "Spartas"

    def test_already_plural(self):
        assert FwText.pluralize("pieces") == "pieces"
        assert FwText.pluralize("spartans") == "spartans"


class TestSingularize():
    # The capitalization of the results is inconsistent with pluralize
    def test_blank(self):
        assert FwText.singularize("") is None
        
    def test_these_are_spartas(self):
        assert FwText.singularize("these") == "this"
        assert FwText.singularize("are") == "is"
        assert FwText.singularize("spartas") == "sparta"

    def test_already_singular(self):
        assert FwText.singularize("piece") == "piece"
        assert FwText.singularize("sparta") == "sparta"
        
        # already singularized
        # assert FwText.singularize("this") == "this"
        # assert FwText.singularize("is") == "is"


class TestPluralizeTuple():
    def test_blank(self):
        assert FwText.pluralize_tuple(tuple([])) == tuple([])

    def test_last_word_default(self):
        result = FwText.pluralize_tuple(("this", "test"))
        assert result == ("this", "tests")

    def test_all_words(self):
        result = FwText.pluralize_tuple(("this", "test"), b_last_word_only=False)
        assert result == ("these", "tests")


class TestSingularizeTuple():
    def test_blank(self):
        assert FwText.singularize_tuple(tuple([])) == tuple([])

    def test_last_word_default(self):
        result = FwText.singularize_tuple(("these", "tests"))
        assert result == ("these", "test")

    def test_all_words(self):
        result = FwText.singularize_tuple(("these", "tests"), b_last_word_only=False)
        assert result == ("this", "test")
