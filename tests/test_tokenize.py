﻿'''
Test cases for FwText/tokenize.pyx
'''
from __future__ import absolute_import
import pyximport; pyximport.install()
import string
import re
import pytest
import FwText


class TestTitleTokenize():
    def test_blank(self):
        result = FwText.title_tokenize("")
        assert result == []
        
    def test_default(self):
        result = FwText.title_tokenize("This is a test Title.")
        assert result == ["This", "is", "a", "test", "Title"]
        
    def test_lower(self):
        result = FwText.title_tokenize("This is a test Title.",
                                       return_lower=True)
        assert result == ["this", "is", "a", "test", "title"]
        
    def test_default_stop_words(self):
        result = FwText.title_tokenize("This is a test Title.",
                                       stop_words='english')
        assert result == ["test", "Title"]
        
    def test_custom_stop_words(self):
        result = FwText.title_tokenize("This is A test Title.",
                                       stop_words=('is', 'a'))
        assert result == ["This", "test", "Title"]
        
    def test_phrases_underscore(self):
        result = FwText.title_tokenize("This is a test Title.",
                                       phrases=['test_title'])
        assert result == ["This", "is", "a", "test_Title"]
        
    def test_phrases_underscore_and_tuple(self):
        result = FwText.title_tokenize("This is a test Title.",
                                       phrases=['test_title'],
                                       return_tuples=True)
        assert result == ["This", "is", "a", ("test", "Title")]
        
    def test_phrases_tuple(self):
        result = FwText.title_tokenize("This is a test Title.",
                                       phrases=[('test', 'title')],
                                       use_underscore=False,
                                       return_tuples=True)
        assert result == ["This", "is", "a", ("test", "Title")]


class TestDescriptionTokenize():
    def test_blank(self):
        result = FwText.description_tokenize("")
        assert result == []
        
    def test_default(self):
        result = FwText.description_tokenize("This is a test Description.")
        assert result == [["This", "is", "a", "test", "Description"]]
        
    def test_lower(self):
        result = FwText.description_tokenize("This is a test Description.",
                                       return_lower=True)
        assert result == [["this", "is", "a", "test", "description"]]
        
    def test_default_stop_words(self):
        result = FwText.description_tokenize("This is a test Description.",
                                       stop_words='english')
        assert result == [["test", "Description"]]
        
    def test_custom_stop_words(self):
        result = FwText.description_tokenize("This is A test Description.",
                                       stop_words=('is', 'a'))
        assert result == [["This", "test", "Description"]]
        
    def test_phrases_underscore(self):
        result = FwText.description_tokenize("This is a test Description.",
                                       phrases=['test_description'])
        assert result == [["This", "is", "a", "test_Description"]]
        
    def test_phrases_underscore_and_tuple(self):
        result = FwText.description_tokenize("This is a test Description.",
                                       phrases=['test_description'],
                                       return_tuples=True)
        assert result == [["This", "is", "a", ("test", "Description")]]
        
    def test_phrases_tuple(self):
        result = FwText.description_tokenize("This is a test Description.",
                                       phrases=[('test', 'description')],
                                       use_underscore=False,
                                       return_tuples=True)
        assert result == [["This", "is", "a", ("test", "Description")]]
        
    def test_sentences_default(self):
        result = FwText.description_tokenize("This is a test Description. It has two sentences.",
                                       return_sentences=True)
        assert result == [["This", "is", "a", "test", "Description"],
                          ["It", "has", "two", "sentences"]]
        
    def test_sentences_joined(self):
        result = FwText.description_tokenize("This is a test Description. It has two sentences.",
                                       return_sentences=False)
        assert result == ["This", "is", "a", "test", "Description",
                          "It", "has", "two", "sentences"]


class TestWordTokenize():
    def test_default_remove_english_stopwords(self):
        result = FwText.word_tokenize("This is Sparta")
        assert result == ["Sparta"]

    def test_do_not_remove_stopwords(self):
        result = FwText.word_tokenize("This is Sparta", stop_words=None)
        assert result == ["This", "is", "Sparta"]

    def test_remove_custom_stopwords(self):
        result = FwText.word_tokenize("This is Sparta", stop_words=["sparta"])
        assert result == ["This", "is"]

    def test_default_remove_punctuation(self):
        result = FwText.word_tokenize("Tom | Jerry!")
        assert result == ["Tom", "Jerry"]

    def test_do_not_remove_punctuation(self):
        result = FwText.word_tokenize("Tom & Jerry!", remove_punct=False)
        assert result == ["Tom", "&", "Jerry!"]

    def test_custom_vocab(self):
        result = FwText.word_tokenize("We're only interested in keeping this word",
            vocabulary=["this", "word"], stop_words=None)
        assert result == ["this", "word"]

    def test_ampersand(self):
        result = FwText.word_tokenize("I like A&W Root Beer & French Fries",
            stop_words=None, convert_ampersand=True)
        assert result == ["I", "like", "A&W", "Root", "Beer", "and", "French", "Fries"]


class TestSentenceTokenize():
    c_tms = {'yahoo', ('walkabout',)}
    c_vocab = {'space', 'will'}
    
    def test_blank(self):
        s_test = ""
        a_result = []
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_period(self):
        s_test = 'This is an easy test. It has two sentences.'
        a_result = ['This is an easy test.', 'It has two sentences.']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_double_space(self):
        s_test = 'This is an easy test.  It has double spacing.'
        a_result = ['This is an easy test.', 'It has double spacing.']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_double_space_no_period(self):
        s_test = 'This is a simple test  It has double spacing but missing periods'
        a_result = ['This is a simple test', 'It has double spacing but missing periods']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_titles(self):
        s_test = 'Book about Mr. Jekyll and Dr. Hyde. This is hard.'
        a_result = ['Book about Mr. Jekyll and Dr. Hyde.', 'This is hard.']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_known_trademark(self):
        s_test = 'Yahoo! is an old search engine'
        a_result = ['Yahoo! is an old search engine']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_unknown_trademark(self):
        s_test = 'Quizzit? is a game.'
        a_result = ['Quizzit? is a game.']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_case_split_trademark(self):
        s_test = 'Test of walkAbout trademark split.'
        a_result = ['Test of walkAbout trademark split.']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_unmarked_list(self):
        s_test = 'This is a description\nShaped like an unmarked list'
        a_result = ['This is a description', 'Shaped like an unmarked list']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_bullet_list(self):
        s_test = '• Feature 1 • feature 2'
        a_result = ['• Feature 1', '• feature 2']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_star_list(self):
        s_test = 'ingredients: * Maintaining cholesterol balance * Providing antioxidants * Assisting heart function'
        a_result = ['ingredients:', '* Maintaining cholesterol balance', '* Providing antioxidants', '* Assisting heart function']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_star_asterisk(self):
        s_test = 'With free case* and selected colors *with purchase)'
        a_result = ['With free case* and selected colors', '*with purchase)']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_ordered_list(self):
        s_test = '1. Element one 2. Element two 33. another element'
        a_result = ['1. Element one', '2. Element two 33.', 'another element']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_missing_space(self):
        s_test = 'This has a missing spaceWill it split anyway?'
        a_result = ['This has a missing space', 'Will it split anyway?']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_quoted_sentence(self):
        s_test = '"This is just like him I said." Then I left.'
        a_result = ['"This is just like him I said."', 'Then I left.']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_end_dimension_quote(self):
        s_test = 'This sentence ends with a dimension. 15"x9"'
        a_result = ['This sentence ends with a dimension. 15"x9"']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_decimal(self):
        s_test = 'This is a 2.0 liter engine.'
        a_result = ['This is a 2.0 liter engine.']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_acronym(self):
        s_test = 'L.I.T. are designed with a limited number of protein and carbohydrate sources.'
        a_result = ['L.I.T. are designed with a limited number of protein and carbohydrate sources.']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_possessive_abbreviation(self):
        s_test = "Pet Gear, Inc.'s strollers are a great way to take your pet with you on a long walk."
        a_result = ["Pet Gear, Inc.'s strollers are a great way to take your pet with you on a long walk."]
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_end_uom(self):
        s_test = 'For X-Large dogs weighing 60-90 lbs. Interactive tug and toss squeak toy Easy-to-use tug handle.'
        a_result = ['For X-Large dogs weighing 60-90 lbs.', 'Interactive tug and toss squeak toy Easy-to-use tug handle.']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_mid_ellipses(self):
        s_test = "This isn't funny...but I laughed anyway."
        a_result = ["This isn't funny...but I laughed anyway."]
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)
    
    def test_end_ellipses(self):
        s_test = 'What a parser... Just the fastest'
        a_result = ['What a parser...', 'Just the fastest']
        assert a_result == FwText.sentence_tokenize(s_test, self.c_vocab, self.c_tms)


class TestSplitStrip():
    def test_blank(self):
        result = FwText.split_strip("")
        assert result == []
        
    def test_default_settings(self):
        result = FwText.split_strip("This  Example")
        assert result == ["This", "Example"]

    def test_multiple_spaces(self):
        result = FwText.split_strip(" This  Example ")
        assert result == ["This", "Example"]

    def test_custom_delimiter(self):
        result = FwText.split_strip("This : Example", s_delimiter=":")
        assert result == ["This", "Example"]

    def test_limit(self):
        result = FwText.split_strip("This | Example | Split", "|", 1)
        assert result == ["This", "Example | Split"]
    
    def test_limit_right(self):
        result = FwText.split_strip("This | Example | Split", "|", 1, b_from_right=True)
        assert result == ["This | Example", "Split"]


class TestWordSplit():
    def test_empty(self):
        a_words = []
        FwText.split_words(a_words, FwText.split_camelcase)
        assert a_words == []
        FwText.split_words(a_words, FwText.split_number_uom)
        assert a_words == []
        FwText.split_words(a_words, FwText.split_edge_punctuation)
        assert a_words == []
        FwText.split_words(a_words, FwText.split_edge_quotes)
        assert a_words == []
    
    def test_camel_case_words(self):
        a_words = ['CamelCase', 'for', 'TestExampleSplit']
        FwText.split_words(a_words, FwText.split_camelcase)
        assert a_words == ['Camel', 'Case', 'for', 'Test', 'Example', 'Split']
    
    def test_number_uom(self):
        a_words = ['Cardboard', 'Box', '10inch', 'x', '10in.', 'x', '12-in']
        FwText.split_words(a_words, FwText.split_number_uom)
        assert a_words == ['Cardboard', 'Box', '10', 'inch', 'x', '10', 'in.', 'x', '12', 'in']
    
    def test_edge_punct(self):
        a_words = ["-test'/", '"this;', "-out."]
        FwText.split_words(a_words, FwText.split_edge_punctuation)
        assert a_words == ['-', "test'", '/', '"this', ';', '-', 'out', '.']
    
    def test_edge_quotes(self):
        a_words = ["'test'", "this'", "'out", '"and"', '"split', 'quotes"', "36'", "'72"]
        FwText.split_words(a_words, FwText.split_edge_quotes)
        assert a_words == ["'", 'test', "'", 'this', "'", "'", 'out', '"',
                           'and', '"', '"', 'split', 'quotes', '"', "36'", "'", '72']


class TestSplitCamelCase():
    def test_empty(self):
        result = FwText.split_camelcase('')
        assert result == tuple()
        
    def test_two_words(self):
        result = FwText.split_camelcase('PyCon')
        assert result == ('Py', 'Con')
        
    def test_three_words(self):
        result = FwText.split_camelcase('PyConNews')
        assert result == ('Py', 'Con', 'News')
        
    def test_too_short_first(self):
        result = FwText.split_camelcase('PCon')
        assert result == tuple()
        
    def test_too_short_last(self):
        result = FwText.split_camelcase('PyC')
        assert result == tuple()
        

class TestSplitNumberUOM():
    def test_empty(self):
        result = FwText.split_number_uom('')
        assert result == tuple()
        
    def test_non_matches(self):
        result = FwText.split_number_uom('none')
        assert result == tuple()
        result = FwText.split_number_uom('mile1')
        assert result == tuple()
        
    def test_matches(self):
        result = FwText.split_number_uom('1In')
        assert result == ('1', 'In')
        result = FwText.split_number_uom('1in.')
        assert result == ('1', 'in.')
        result = FwText.split_number_uom('1inch')
        assert result == ('1', 'inch')
        result = FwText.split_number_uom('2inches')
        assert result == ('2', 'inches')
        result = FwText.split_number_uom('1 in')
        assert result == ('1', 'in')
        result = FwText.split_number_uom('1-in.')
        assert result == ('1', 'in.')
        result = FwText.split_number_uom('1__inch')
        assert result == ('1', 'inch')
        result = FwText.split_number_uom('2.inches')
        assert result == ('2', 'inches')
        result = FwText.split_number_uom('1.5_inch')
        assert result == ('1.5', 'inch')
        result = FwText.split_number_uom('1-1/2inch')
        assert result == ('1-1/2', 'inch')
        result = FwText.split_number_uom('2 3/4 inch')
        assert result == ('2 3/4', 'inch')
        result = FwText.split_number_uom('3 - 1/4 inch')
        assert result == ('3 - 1/4', 'inch')


class TestSplitEdgePunctuation():
    def test_empty(self):
        assert FwText.split_edge_quotes("") == ""
    
    def test_plain_quotes(self):
        assert FwText.split_edge_quotes("'") == "'"
        assert FwText.split_edge_quotes("''") == "''"
        assert FwText.split_edge_quotes('"') == '"'
        assert FwText.split_edge_quotes('""') == '""'
        
    def test_plain_quotes(self):
        assert FwText.split_edge_quotes("'") == "'"
        assert FwText.split_edge_quotes("''") == "''"
        
    def test_words(self):
        assert FwText.split_edge_quotes('"test') == ['"', 'test']
        assert FwText.split_edge_quotes('test"') == ['test', '"']
        assert FwText.split_edge_quotes('"test"') == ['"', 'test', '"']
        assert FwText.split_edge_quotes("'test") == ["'", 'test']
        assert FwText.split_edge_quotes("test'") == ['test', "'"]
        assert FwText.split_edge_quotes("'test'") == ["'", 'test', "'"]
        
    def test_letters(self):
        assert FwText.split_edge_quotes('"a') == ['"', 'a']
        assert FwText.split_edge_quotes('a"') == ['a', '"']
        assert FwText.split_edge_quotes('"a"') == ['"', 'a', '"']
        assert FwText.split_edge_quotes("'b") == ["'", 'b']
        assert FwText.split_edge_quotes("b'") == ['b', "'"]
        assert FwText.split_edge_quotes("'b'") == ["'", 'b', "'"]
        
    def test_numbers(self):
        assert FwText.split_edge_quotes('"1') == ['"', '1']
        assert FwText.split_edge_quotes('1"') == '1"'
        assert FwText.split_edge_quotes('"1"') == ['"', '1', '"']
        assert FwText.split_edge_quotes("'0") == ["'", '0']
        assert FwText.split_edge_quotes("0'") == "0'"
        assert FwText.split_edge_quotes("'0'") == ["'", '0', "'"]


class TestSplitOnEdgePunctuation():
    def test_default_settings(self):
        result = FwText.split_on_edge_punctuation([
            "test/", ",this-", "out"
        ])
        assert result == ["test", "/", ",", "this", "-","out"]

    def test_do_not_split_if_not_edge(self):
        result = FwText.split_on_edge_punctuation([
            "back-end", "front-end"
        ])
        assert result == ["back-end", "front-end"]


class TestJoinOxford():
    def test_blank(self):
        a_test = []
        result = FwText.join_oxford(a_test)
        assert result == ''
    
    def test_one_word(self):
        a_test = ['test']
        result = FwText.join_oxford(a_test)
        assert result == 'test'
    
    def test_two_words(self):
        a_test = ['test', 'this']
        result = FwText.join_oxford(a_test)
        assert result == 'test, and this'
    
    def test_three_words(self):
        a_test = ['test', 'this', 'function']
        result = FwText.join_oxford(a_test)
        assert result == 'test, this, and function'
    
    
class TestJoinSentence():
    def test_blank(self):
        a_test = []
        result = FwText.join_sentence(a_test)
        assert result == ''
    
    def test_one_punct(self):
        a_test = ['-']
        result = FwText.join_sentence(a_test)
        assert result == '-'
    
    def test_sentence(self):
        a_test = ['This', 'is', 'a', '100', '%', 'test', '-', 'sentence', 'w', '/', 'slashes.']
        result = FwText.join_sentence(a_test)
        assert result == 'This is a 100% test-sentence w/slashes.'
