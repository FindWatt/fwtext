﻿'''
Test cases for FwText/search.pyx
'''
from __future__ import absolute_import
import pyximport; pyximport.install()
import string
import re
import pytest
import FwText


class TestWordIndexes():
    def test_blank_text(self):
        s_text = ""
        a_text = ['This', 'is', 'a', '100', 'test', 'sentence', 'w', 'w', 'slashes']
        result = FwText.word_indexes(a_text, s_text)
        assert result == []
    
    def test_blank_word_list(self):
        s_text = "This is a a 100% test - sentence w / slashes."
        a_text = []
        result = FwText.word_indexes(a_text, s_text)
        assert result == []
    
    def test_normal(self):
        s_text = "This is a a 100% test - sentence w / slashes."
        a_text = ['This', 'is', 'a', '100', 'test', 'sentence', 'w', 'w', 'slashes']
        result = FwText.word_indexes(a_text, s_text)
        assert result == [(0, 4), (5, 7), (8, 9), (12, 15), (17, 21), (24, 32), (33, 34), None, (37, 44)]
    
    def test_no_spaces(self):
        s_text = "Thisisaa100%test-sentencew/slashes."
        a_text = ['This', 'is', 'a', '100', 'test', 'sentence', 'w', 'w', 'slashes']
        result = FwText.word_indexes(a_text, s_text)
        assert result == [(0, 4), (4, 6), (6, 7), (8, 11), (12, 16), (17, 25), (25, 26), None, (27, 34)]
    
    def test_case(self):
        s_text = "this IS a A 100% tEsT - Sentence W / Slashes."
        a_text = ['This', 'is', 'a', '100', 'test', 'sentence', 'w', 'w', 'slashes']
        result = FwText.word_indexes(a_text, s_text, b_case_insensitive=True)
        assert result == [(0, 4), (5, 7), (8, 9), (12, 15), (17, 21), (24, 32), (33, 34), None, (37, 44)]


class TestPosContainedInSpans():
    def test_none_pos(self):
        result = FwText.pos_contained_in_spans(
            None,
            [(1, 5), (10, 15)])
        assert result == False
    
    def test_empty_pos(self):
        result = FwText.pos_contained_in_spans(
            tuple(),
            [(1, 5), (10, 15)])
        assert result == False

    def test_empty_spans(self):
        result = FwText.pos_contained_in_spans(
            (5, 10),
            [])
        assert result == False
    
    def test_no_overlap(self):
        result = FwText.pos_contained_in_spans(
            (5, 10),
            [(1, 5), (10, 15)])
        assert result == False
    
    def test_partial_overlaps(self):
        result = FwText.pos_contained_in_spans(
            (3, 12),
            [(1, 5), (10, 15)])
        assert result == False
    
    def test_contained(self):
        result = FwText.pos_contained_in_spans(
            (1, 5),
            [(1, 5), (10, 15)])
        assert result == True


class TestPosOverlapsSpans():
    def test_none_pos(self):
        result = FwText.pos_overlaps_in_spans(
            None,
            [(1, 5), (10, 15)])
        assert result == False
        
    def test_none_spans(self):
        result = FwText.pos_overlaps_in_spans(
            (1, 5),
            None)
        assert result == False
    
    def test_empty_spans(self):
        result = FwText.pos_overlaps_in_spans(
            (1, 5),
            [])
        assert result == False

    def test_no_overlap(self):
        result = FwText.pos_overlaps_in_spans(
            (5, 10),
            [(0, 5), (10, 15)])
        assert result == False

    def test_start_overlap(self):
        result = FwText.pos_overlaps_in_spans(
            (4, 10),
            [(0, 5), (10, 15)])
        assert result == True

    def test_end_overlap(self):
        result = FwText.pos_overlaps_in_spans(
            (5, 11),
            [(0, 5), (10, 15)])
        assert result == True

    def test_contained(self):
        result = FwText.pos_overlaps_in_spans(
            (1, 3),
            [(0, 5), (10, 15)])
        assert result == True


class TestMergeOverlappedSpans():
    def test_none_pos(self):
        result = FwText.merge_overlapped_spans(None)
        assert not result

    def test_list(self):
        a_spans = [(0, 5), (10, 25), (0, 2), (9, 11), (5, 8), (12, 15), (4, 6)]
        result = FwText.merge_overlapped_spans(a_spans)
        assert result == [(0, 8), (9, 25)]


class TestMergAadjacentSpans():
    def test_none_pos(self):
        result = FwText.merge_adjacent_spans('', None)
        assert not result
    
    def test_empty(self):
        result = FwText.merge_adjacent_spans('', [])
        assert not result
    
    def test_single(self):
        result = FwText.merge_adjacent_spans('-test-', [(1, 5)])
        assert [(1, 5)] == result
    
    def test_merge(self):
        result = FwText.merge_adjacent_spans(r'test"' + r"\`~-_/\:@+this", [(0, 4), (15, 19)])
        assert [(0, 19)] == result
    
    def test_not_merged(self):
        result = FwText.merge_adjacent_spans('testathis', [(0, 4), (5, 9)])
        assert [(0, 4), (5, 9)] == result
    
    def test_too_far(self):
        result = FwText.merge_adjacent_spans('test---this', [(0, 4), (7, 11)], 2)
        assert [(0, 4), (7, 11)] == result
    
    def test_just_close_enough(self):
        result = FwText.merge_adjacent_spans('test---this', [(0, 4), (7, 11)], 3)
        assert [(0, 11)] == result
    
    def test_allowed(self):
        result = FwText.merge_adjacent_spans('test?this', [(0, 4), (5, 9)], c_allowed_chars=set("?"))
        assert [(0, 9)] == result
    
    def test_not_allowed(self):
        result = FwText.merge_adjacent_spans('test-this', [(0, 4), (5, 9)], c_not_allowed_chars=set("-"))
        assert [(0, 4), (5, 9)] == result


class TestWordInText():
    def test_default_settings(self):
        result = FwText.word_in_text("This is Sparta", "Sparta")
        assert result == (7, 14)

    def test_plural_and_singular(self):
        result = FwText.word_in_text("These are Spartans", "Spartan")
        assert result == (9, 18)
        result = FwText.word_in_text("This is a Spartan", "Spartans")
        assert result == (9, 17)

    def test_position_function(self):
        # Testing each position function independently
        # in their respective classes.
        pass


class TestWordInStr():
    def test_word_in_str(self):
        result = FwText.word_instr("This is Sparta", "Sparta")
        assert result == (7, 14)

    def test_case_sensitivity(self):
        result = FwText.word_instr("This is Sparta", "sparta")
        assert result is None

    def test_word_boundaries_importance(self):
        result = FwText.word_instr("This is a Spartan", "Sparta")
        assert result is None


class TestFindCharSequence():
    def test_default_settings(self):
        result = FwText.find_char_sequence("This is Sparta", "Sparta")
        assert result == (8, 14)

    def test_word_boundaries_not_important(self):
        result = FwText.find_char_sequence("These are Spartans", "Sparta")
        assert result == (10, 16)

    def test_ignore_some_punctuation(self):
        result = FwText.find_char_sequence("This is Sparta", "Spa__r__ta")
        assert result == (8, 14)
        result = FwText.find_char_sequence("This is Spa__r__ta", "Sparta")
        assert result == (8, 18)


class TestFindWordSequence():
    def test_default_settings(self):
        result = FwText.find_word_sequence("This is Sparta", "Sparta")
        assert result == (8, 14)

    def test_ignore_some_punctuation(self):
        result = FwText.find_word_sequence("This is-Sparta", "is Sparta")
        assert result == (5, 14)
        result = FwText.find_word_sequence("This is Sparta", "is-Sparta")
        assert result == (5, 14)
        result = FwText.find_word_sequence("This is'Sparta", "is Sparta")
        assert result == (5, 14)
        result = FwText.find_word_sequence("This is Sparta", "is'Sparta")
        assert result == (5, 14)

    def test_try_plural_singular(self):
        # Not sure if this feature if working
        pass


class TestWordInSet():
    c_words = {"this", "is" "a", "test"}
    def test_empty_set(self):
        assert not FwText.word_in_set(set(), "test")
    def test_empty_word(self):
        c_words = {"test"}
        assert not FwText.word_in_set(c_words, "")
    def test_exact(self):
        c_words = {"test"}
        assert FwText.word_in_set(c_words, "test")
    def test_singular(self):
        c_words = {"test"}
        assert FwText.word_in_set(c_words, "tests")
    def test_plural(self):
        c_words = {"tests"}
        assert FwText.word_in_set(c_words, "test")
    def test_possessive(self):
        c_words = {"test's"}
        assert FwText.word_in_set(c_words, "test")
    def test_plural_possessive(self):
        c_words = {"tests'"}
        assert FwText.word_in_set(c_words, "test")
    def test_not_possessive(self):
        c_words = {"test"}
        assert FwText.word_in_set(c_words, "test's")
    def test_not_possessive_plural(self):
        c_words = {"tests"}
        assert FwText.word_in_set(c_words, "test's")
    def test_not_plural_possessive(self):
        c_words = {"tests"}
        assert FwText.word_in_set(c_words, "tests'")
    def test_partial_long_enough(self):
        c_words = {"testing"}
        assert FwText.word_in_set(c_words, "test", i_partial_minimum=4)
    def test_partial_too_short(self):
        c_words = {"test"}
        assert not FwText.word_in_set(c_words, "tes", i_partial_minimum=4)


class TestGetTokenSpans():
    def test_empty_text(self):
        result = FwText.get_token_spans("", ['test',])
        assert result == [None]
    def test_empty_tokens(self):
        result = FwText.get_token_spans("test", [])
        assert result == []
    
    def test_sub_word(self):
        result = FwText.get_token_spans("Herfindal", ['find'])
        assert result == [(3, 7)]
    
    def test_word(self):
        result = FwText.get_token_spans("Deep Herfindal Analysis", ['Herfindal'])
        assert result == [(5, 14)]
    
    def test_ampersand(self):
        result = FwText.get_token_spans("Salt & Pepper", ['Salt', 'and', 'Pepper'])
        assert result == [(0, 4), (5, 6), (7, 13)]
        
    def test_delayed(self):
        result = FwText.get_token_spans("Deep Herfindal Analysis", ['Analysis'], i_max_missing=2)
        assert result == [None]
        result = FwText.get_token_spans("Deep Herfindal Analysis", ['Analysis'], i_max_missing=2, i_start=15)
        assert result == [(15, 23)]
    
    def test_accent(self):
        result = FwText.get_token_spans(
            "EqualizerŽ Telescopic Adjustable Length Long Cutting",
            ["EqualizerZ", "Telescopic", "Adjustable", "Length", "Long", "Cutting"],
            i_max_missing=5)
        assert result == [None, (11, 21), (22, 32), (33, 39), (40, 44), (45, 52)]
    
    def test_accent_against_clean(self):
        result = FwText.get_token_spans(
            "EqualizerŽ Telescopic Adjustable Length Long Cutting",
            ["EqualizerZ", "Telescopic", "Adjustable", "Length", "Long", "Cutting"],
            b_accent_insensitive=True)
        assert result == [(0, 10), (11, 21), (22, 32), (33, 39), (40, 44), (45, 52)]


class TestGetConsecutiveTokenSpans():
    def test_empty_text(self):
        result = FwText.get_consecutive_spans("", [['test'], ['this']])
        assert result == [[None], [None]]
    def test_empty_tokens(self):
        result = FwText.get_consecutive_spans("test this", [])
        assert result == []
    def test_normal(self):
        result = FwText.get_consecutive_spans("test this", [['test'], ['this']])
        assert result == [[(0, 4)], [(5, 9)]]


class TestGetPhrasePosition():
    def test_default_settings(self):
        result = FwText.get_phrase_position("This is Sparta", "is Sparta")
        assert result == (1, 3)

    def test_case_sensitivity(self):
        result = FwText.get_phrase_position(
            "This is Sparta", "IS SPARTA",
            b_case_insensitive=True
        )
        assert result == (1, 3)

    def test_and_ampersand(self):
        result = FwText.get_phrase_position(
            "Salt and Pepper", "Salt & Pepper",
            b_punctuation_insensitive=True,
        )
        assert result == (0, 3)
        result = FwText.get_phrase_position(
            "Salt & Pepper", "Salt and Pepper",
            b_punctuation_insensitive=True,
        )
        assert result == (0, 3)

    def test_punctuation_insensitive(self):
        result = FwText.get_phrase_position(
            "This is Sparta!", "Sparta",
            b_punctuation_insensitive=True
        )
        assert result == (2, 3)

    def test_skipgram_too_short(self):
        s_text = "Grizzly Joint Aid Mini Pellet Hip & Joint for Dogs"
        s_phrase = "joint pellet"
        result = FwText.get_phrase_position(
            s_text, s_phrase,
            b_case_insensitive=True,
            b_punctuation_insensitive=True,
            skipgram_length=1,
        )
        assert not result

    def test_skipgram(self):
        s_text = "Grizzly Joint Aid Mini Pellet Hip & Joint for Dogs"
        s_phrase = "joint pellet"
        result = FwText.get_phrase_position(
            s_text, s_phrase,
            b_case_insensitive=True,
            b_punctuation_insensitive=True,
            skipgram_length=2,
        )
        assert result == (1, 5)

    def test_from_beginning(self):
        # Not sure if this feature is working properly
        result = FwText.get_phrase_position(
            "This is Sparta! But not all Sparta", "Sparta",
            b_from_beginning=False)
        assert result == (6, 7)
    
    def test_extra_punctuation(self):
        result = FwText.get_phrase_position(
            ['pto', 'clutch', 'for', 'great', 'dane', 'gda10017', 'd38155',
             'and', '5219-4', 'with', 'wire', 'repair', 'kit'],
             ["repair", "kit", "!!"],
            b_punctuation_insensitive=True,
            b_case_insensitive=True,
        )
        assert result == (11, 13)


class TestGetAllPhrasePositions():
    def test_missing(self):
        result = FwText.get_all_phrase_positions("This is a test sentence with no phrase", "missing phrase")
        assert not result
        
    def test_default_settings(self):
        result = FwText.get_all_phrase_positions("This is Sparta", "is Sparta")
        assert result == [(1, 3)]

    def test_case_insensitivity(self):
        result = FwText.get_all_phrase_positions(
            "This is Sparta", "IS SPARTA",
            b_case_insensitive=True
        )
        assert result == [(1, 3)]

    def test_punctuation_insensitive(self):
        result = FwText.get_all_phrase_positions(
            "This is Sparta!", "Sparta",
            b_punctuation_insensitive=True
        )
        assert result == [(2, 3)]

    def test_first_word(self):
        result = FwText.get_all_phrase_positions(
            "Testing last word !", "Testing",
            b_punctuation_insensitive=True
        )
        assert result == [(0, 1)]

    def test_last_word(self):
        result = FwText.get_all_phrase_positions(
            "Testing last word !!", "word !!",
            b_punctuation_insensitive=True
        )
        assert result == [(2, 4)]
    
    def test_just_punct(self):
        result = FwText.get_all_phrase_positions(
            ["Testing", "last", "word", "!!"], ["!!"],
            b_punctuation_insensitive=False
        )
        assert result == [(3, 4)]


class TestGetProximities():
    source = [(8, 10), (11, 13), (56, 58), (143, 145)]
    target = [(6, 7), (13, 14), (55, 56), (174, 175), (145, 146)]
    def test_empty_target(self):
        result = FwText.get_proximities([], self.source)
        assert result == []
    
    def test_empty_source(self):
        result = FwText.get_proximities(self.target, [])
        assert result == [99999, 99999, 99999, 99999, 99999]
        
    def test_regular(self):
        result = FwText.get_proximities(self.target, self.source)
        assert result == [-2, 1, -1, 1, 30]


class TestProximity():
    source = [(8, 10), (11, 13), (56, 58), (143, 145)]
    target = [(6, 7), (13, 14), (55, 56), (174, 175), (145, 146)]
    def test_empty_target(self):
        result = FwText.proximity([], self.source)
        assert result == 99999
    
    def test_empty_source(self):
        result = FwText.proximity(self.target, [])
        assert result == 99999
        
    def test_regular(self):
        result = FwText.proximity(self.target, self.source)
        assert result == 1


class TestNearestProximity():
    source = [(8, 10), (11, 13), (56, 58), (143, 145)]
    target = [(6, 7), (13, 14), (55, 56), (174, 175), (145, 146)]
    def test_empty_target(self):
        result = FwText.get_nearest_proximity([], self.source)
        assert result is None
    
    def test_empty_source(self):
        result = FwText.get_nearest_proximity(self.target, [])
        assert result is None
        
    def test_regular(self):
        result = FwText.get_nearest_proximity(self.target, self.source)
        assert result == (1, 1)


class TestSentenceProximity():
    anchor = ("anchor", "phrase")
    sentence = FwText.title_tokenize(
        "This has two tests of the anchor phrase with another pluralized/singularized anchor phrases test",
        stop_words=False)
    
    def test_empty_target(self):
        result = FwText.get_sentence_proximity(("test",), tuple(), self.sentence)
        assert result == []
    
    def test_missing_target(self):
        result = FwText.get_sentence_proximity(("test",), ("not", "present"), self.sentence)
        assert result == []
    
    def test_empty_search(self):
        result = FwText.get_sentence_proximity(tuple(), self.anchor, self.sentence)
        assert result == []
        
    def test_missing_search(self):
        result = FwText.get_sentence_proximity(("not", "present"), self.anchor, self.sentence)
        assert result == []
        
    def test_normal_search(self):
        result = FwText.get_sentence_proximity(("test",), self.anchor, self.sentence)
        assert result == [-3, 1]


class TestTokensBetween():
    text = ['this', 'is', 'a', 'test', 'of', 'tokens', 'between']
    def test_empty(self):
        results = FwText.get_tokens_between([], (1, 2), (3, 4))
        assert not results
        
    def test_overlapped(self):
        results = FwText.get_tokens_between(self.text, (1, 3), (2, 4))
        assert not results
        
    def test_adjacent(self):
        results = FwText.get_tokens_between(self.text, (1, 2), (2, 4))
        assert not results
        
    def test_span1_first(self):
        results = FwText.get_tokens_between(self.text, (1, 2), (5, 6))
        assert results == ['a', 'test', 'of']
        
    def test_span2_first(self):
        results = FwText.get_tokens_between(self.text, (5, 6), (1, 2))
        assert results == ['a', 'test', 'of']


class TestAreTwoSubstringsConsecutive():
    def test_ordered_case_sensitive_match(self):
        result = FwText.are_two_substrings_consecutive(
            "This is Sparta",
            ["This is", "Sparta"]
        )
        assert result is True

    def test_fail_unordered_match(self):
        result = FwText.are_two_substrings_consecutive(
            "This is Sparta",
            ["Sparta", "This is"]
        )
        assert result is False

    def test_case_insensitive_match(self):
        result = FwText.are_two_substrings_consecutive(
            "This is Sparta",
            ["this is", "sparta"]
        )
        assert result is True

    def test_case_sensitive_match(self):
        result = FwText.are_two_substrings_consecutive(
            "this is sparta", ["THIS IS", "SPARTA"],
            case_sensitive = True
        )

    def test_separated_multiple_spaces(self):
        result = FwText.are_two_substrings_consecutive(
            "This is                 Sparta",
            ["This is", "Sparta"]
        )
        assert result is True

    def test_separated_by_punctuation(self):
        result = FwText.are_two_substrings_consecutive(
            "This is + Sparta",
            ["This is", "Sparta"]
        )
        assert result is True

    def test_separated_by_word(self):
        result = FwText.are_two_substrings_consecutive(
            "This is not Sparta",
            ["This is", "Sparta"]
        )
        assert result is False

    def test_more_than_two_substrings(self):
        #with pytest.raises(Exception) as error_info:
        #    result = FwText.are_two_substrings_consecutive(
        #        "This is not Sparta",
        #        ["This", "is not", "Sparta"]
        #    )
        #assert "This function only takes 2 substrings" in error_info.__str__()
        try:
            result = FwText.are_two_substrings_consecutive(
                "This is not Sparta",
                ["This", "is not", "Sparta"]
            )
            assert False
        except:
            #"This function only takes 2 substrings", so it should fail
            pass


class TestAreConsecutiveOrdered():
    def test_default_settings(self):
        result = FwText.are_consecutive_ordered(
            "This is Sparta", ["This", "is", "Sparta"])
        assert result is True

    def test_case_sensitive(self):
        result = FwText.are_consecutive_ordered(
            "THIS IS SPARTA", ["this", "is", "sparta"],
            case_sensitive=True
        )
        assert result is False

    def test_unordered(self):
        result = FwText.are_consecutive_ordered(
            "This is Sparta", ["Sparta", "is", "this"]
        )
        assert result is False


class TestAreSubstringsConsecutive():
    def test_default_settings(self):
        result = FwText.are_substrings_consecutive(
            "This is Sparta", ["this", "is", "sparta"])
        assert result is True

    def test_unordered_substrings(self):
        result = FwText.are_substrings_consecutive(
            "This is Sparta", ["Sparta", "is", "this"])
        assert result is True

    def test_case_sensitive(self):
        result = FwText.are_substrings_consecutive(
            "THIS IS SPARTA", ["this", "is", "sparta"],
            case_sensitive = True)
        assert result is False

    def test_separated_multiple_spaces(self):
        result = FwText.are_substrings_consecutive(
            "This is        Sparta", ["This", "is", "Sparta"]
        )
        assert result is True

    def test_separated_by_punctuation(self):
        result = FwText.are_substrings_consecutive(
            "This+is+sparta", ["This", "is", "Sparta"]
        )
        assert result is True
