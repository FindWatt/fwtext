﻿'''
Test cases for FwText/test_dimension_patterns.pyx
'''
import pytest
#from FwText import dimension_patterns
from FwText import dimensions

#dim_tok = dimension_patterns.DimensionsTokenizer()
dim_tok = dimensions.Dimensionizer()

class TestTokenizeDimensions():
    # Including regex level tests would be the optimal way to go
    # http://stackoverflow.com/questions/488601/how-do-you-unit-test-regular-expressions
    # For now I'll include tests to check that the module is not broken
    def test_default_behavior(self):
        result = dim_tok.tokenize_dimensions(
            "jars of pickles, packs of 6 jars. Only high quality 3 cm pickles."
        )
        exp_result = "jars of pickles, packs_of_6 jars. Only high quality 3_cm pickles."
        assert result[0] == exp_result
        assert isinstance(result[1], list)

        assert result[1][0].pattern_type == "distance_2"
        assert result[1][0].string_match == "3 cm"
        assert result[1][0].position_start == 52
        assert result[1][0].position_end == 56
        
        assert result[1][1].pattern_type == "pack_0"
        assert result[1][1].string_match == "packs of 6"
        assert result[1][1].position_start == 17
        assert result[1][1].position_end == 27

class TestCompilePatterns():
    def test_compiles_elements_in_list(self):
        compiled = dim_tok._compile_patterns(
            ["pattern1", "pattern2", "pattern3"]
        )
        assert len(compiled) == 3

    def test_returns_patternslist_object(self):
        compiled = dim_tok._compile_patterns(
            ["pattern1", "pattern2", "pattern3"]
        )
        assert isinstance(compiled, list)

    def test_ignores_case(self):
        compiled = dim_tok._compile_patterns(["sparta"])
        assert compiled[0].search("Sparta")


class TestTokenizePatterns():
    def test_finds_patterns(self):
        doc, matches = dim_tok._tokenize_patterns(
            "this is Sparta", "test", dim_tok._compile_patterns(["this is"])
        )
        assert len(matches) == 1
        assert matches[0].position_start == 0
        assert matches[0].position_end == 7

    def test_returns_tokenizationresult_object(self):
        doc, matches = dim_tok._tokenize_patterns(
            "this is Sparta", "test", dim_tok._compile_patterns(["this is"])
        )
        assert all(
            isinstance(match, dimensions.TokenizationResult)
            for match in matches
        )

    def test_returns_modified_document(self):
        doc, matches = dim_tok._tokenize_patterns(
            "this is Sparta", "test", dim_tok._compile_patterns(["this is"])
        )
        assert doc == "this_is Sparta"



class TestInchesPatterns():
    def test_distance_patterns_multi_dimensional_between_imperial(self):
        pattern = dim_tok.patterns['distance'][0]
        
        # 3D inches between
        assert pattern.search("10inch-12inch-90inch")
        assert pattern.search("10inches-12inches-90inches")
        assert pattern.search('10"-12"-90"')
        assert pattern.search("10in.-12in.-90in.")
        assert pattern.search("10in. - 12in. - 90in.")
        assert pattern.search("1/2inches-1/4inches-1/6inches")
        assert pattern.search('1/2in. - 1/4in. - 1/6in.')
        

        # 3D feet between
        assert pattern.search("2feet-3feet-4feet")
        assert pattern.search("2feet - 3feet - 4feet")
        assert pattern.search("2feet/3feet/4feet")
        assert pattern.search("2feetx3feetx4feet")
        assert pattern.search("2feetX3feetX4feet")
        assert pattern.search("2feet~3feet~4feet")
        assert pattern.search("2FEET-3FEET-4FEET")
        assert pattern.search("2ft-3ft-4ft")
        assert pattern.search("2ft - 3ft - 4ft")
        assert pattern.search("2ft/3ft/4ft")
        assert pattern.search("2ftx3ftx4ft")
        assert pattern.search("2ftX3ftX4ft")
        assert pattern.search("2ft~3ft~4ft")
        assert pattern.search("2ft.~3ft.~4ft.")
        assert pattern.search("2FT-3FT-4FT")
        assert pattern.search("2'-3'-4'")
        assert pattern.search("10/12ft.~20/12ft.~13/12ft.")
        assert pattern.search("10/12'~20/12'~13/12'")
        assert pattern.search("2feet-3feet-4feet")
        assert pattern.search("2feet - 3feet - 4feet")
        assert pattern.search("2feetx3feetx4feet")
        assert pattern.search("2feetX3feetX4feet")
        assert pattern.search("2feet~3feet~4feet")
        assert pattern.search("2FEET-3FEET-4FEET")
        assert pattern.search("2ft-3ft-4ft")
        assert pattern.search("2ft - 3ft - 4ft")
        assert pattern.search("2ftx3ftx4ft")
        assert pattern.search("2ftX3ftX4ft")
        assert pattern.search("2ft~3ft~4ft")
        assert pattern.search("2ft.~3ft.~4ft.")
        assert pattern.search("2FT-3FT-4FT")
        assert pattern.search("2'-3'-4'")
        
        
        # 2D inches between
        assert pattern.search("10inch-12inch")
        assert pattern.search("10inches-12inches")
        assert pattern.search('10"-12"')
        assert pattern.search("10in.-12in.")
        assert pattern.search("10in. - 12in.")
        assert pattern.search("1/2inches-1/4inches")
        assert pattern.search('1/2in. - 1/4in.')
        
        
        # 2D feet between
        assert pattern.search("2feet-3feet")
        assert pattern.search("2feet - 3feet")
        assert pattern.search("2feet/3feet")
        assert pattern.search("2feetx3feet")
        assert pattern.search("2feetX3feet")
        assert pattern.search("2feet~3feet")
        assert pattern.search("2FEET-3FEET")
        assert pattern.search("2ft-3ft")
        assert pattern.search("2ft - 3ft")
        assert pattern.search("2ft/3ft")
        assert pattern.search("2ftx3ft")
        assert pattern.search("2ftX3ft")
        assert pattern.search("2ft~3ft")
        assert pattern.search("2ft.~3ft.")
        assert pattern.search("2FT-3FT")
        assert pattern.search("2'-3'")
        assert pattern.search("20/12feet-13/12feet")
        assert pattern.search("20/12ft-13/12ft")
        assert pattern.search("20/12ft.~13/12ft.")
        assert pattern.search("20/12'~13/12'")
        assert pattern.search("2feet-3feet")
        assert pattern.search("2feet - 3feet")
        assert pattern.search("2feetx3feet")
        assert pattern.search("2feetX3feet")
        assert pattern.search("2feet~3feet")
        assert pattern.search("2FEET-3FEET")
    
    def test_distance_patterns_imperial(self):
        pattern = dim_tok.patterns['distance'][2]
        
        # 3D inches
        assert pattern.search("10-12-90 inch")
        assert pattern.search("10/12/90 inch")
        assert pattern.search("10X12X90 inch")
        assert pattern.search("10x12x90 inch")
        assert pattern.search("10~12~90 inch")
        assert pattern.search("100-120-900 inch")
        assert pattern.search("1.10-1.12-9.90 inch")
        assert pattern.search("10-12-90 inches")
        assert pattern.search("10-12-90inch")
        assert pattern.search("10 - 12 - 90 inch")
        assert pattern.search("10 - 12 - 90 (inch)")
        assert pattern.search("10 - 12 - 90 (inches)")
        
        assert pattern.search("10-12-90in")
        assert pattern.search("10/12/90in")
        assert pattern.search("10X12X90in")
        assert pattern.search("10x12x90in")
        assert pattern.search("10~12~90in")
        assert pattern.search("100-120-900in")
        assert pattern.search("1.10-1.12-9.90in")
        assert pattern.search("10-12-90 in")
        assert pattern.search("10-12-90in.")
        assert pattern.search('10-12-90"')
        assert pattern.search("10 - 12 - 90in")
        assert pattern.search("10-12-90(in)")

        assert pattern.search("1/2-1/4-1/6inch")
        assert pattern.search("1/2X1/4X1/6inch")
        assert pattern.search("1/2x1/4x1/6inch")
        assert pattern.search("1/2~1/4~1/6inch")
        assert pattern.search("1/2 - 1/4 - 1/6inch")
        assert pattern.search("1 1/2 - 1 1/4 - 1 1/6inch")
        assert pattern.search("1-1/2 - 1-1/4 - 1-1/6inch")
        assert pattern.search("11 1/2 x 11 1/4 x 12 1/6 inch")
        assert pattern.search("1/2-1/4-1/6(inch)")
        assert pattern.search("1/2-1/4-1/6(inches)")

        assert pattern.search('1/2-1/4-1/6in')
        assert pattern.search('1/2-1/4-1/6in.')
        assert pattern.search('1/2-1/4-1/6"')
        assert pattern.search('1/2x1/4x1/6in')
        assert pattern.search('1/2X1/4X1/6in')
        assert pattern.search('1/2~1/4~1/6in')
        assert pattern.search('1/2 - 1/4 - 1/6in')
        assert pattern.search('2 1/2 - 2 1/4 - 3 1/6in')
        assert pattern.search('2-1/2 - 2-1/4 - 3-1/6in')
        assert pattern.search('2 1/2 x 3 1/4 x 2 1/6 in')
        assert pattern.search('1/2-1/4-1/6(in)')
        
        # 3D feet
        assert pattern.search("1-2-3feet")
        assert pattern.search("10-12-90feet")
        assert pattern.search("10.12-12.12-90.12feet")
        assert pattern.search("1-1.5-1.8foot")
        assert pattern.search("1x2x3 feet")
        assert pattern.search("1-2-3(feet)")

        assert pattern.search("1-2-3ft")
        assert pattern.search("10-12-90ft")
        assert pattern.search("10.12-12.12-90.12ft")
        assert pattern.search("1-1.5-1.8'")
        assert pattern.search("1x2x3 ft")
        assert pattern.search("1-2-3(ft)")

        assert pattern.search("1/2-2/3-3/4feet")
        assert pattern.search("10/12-20/12-13/12feet")
        assert pattern.search("2 10/12 - 2 20/12 - 2 13/12feet")
        assert pattern.search("10/12feet-20/12feet-13/12feet")
        assert pattern.search("10/12 - 20/12 - 13/12feet")
        assert pattern.search("10/12x20/12x13/12feet")
        assert pattern.search("10/12X20/12X13/12feet")
        assert pattern.search("10/12~20/12~13/12feet")
        assert pattern.search("10/12~20/12~13/12FEET")
        assert pattern.search("10/12~20/12~13/12foot")
        assert pattern.search("10/12~20/12~13/12 feet")
        assert pattern.search("10/12~20/12~13/12(feet)")

        assert pattern.search("1/2-2/3-3/4ft")
        assert pattern.search("10/12-20/12-13/12ft")
        assert pattern.search("2 10/12 - 2 20/12 - 2 13/12ft")
        assert pattern.search("10/12ft-20/12ft-13/12ft")
        assert pattern.search("10/12 - 20/12 - 13/12ft")
        assert pattern.search("10/12x20/12x13/12ft")
        assert pattern.search("10/12X20/12X13/12ft")
        assert pattern.search("10/12~20/12~13/12ft")
        assert pattern.search("10/12~20/12~13/12FT")
        assert pattern.search("10/12~20/12~13/12 ft")
        assert pattern.search("10/12~20/12~13/12(ft)")

        assert pattern.search("1-2-3feet")
        assert pattern.search("10-12-90feet")
        assert pattern.search("1-2-3foot")
        assert pattern.search("1x2x3 feet")
        assert pattern.search("1-2-3(feet)")

        assert pattern.search("1-2-3ft")
        assert pattern.search("10-12-90ft")
        assert pattern.search("10-12-90ft")
        assert pattern.search("1-2-3'")
        assert pattern.search("1x2x3 ft")
        assert pattern.search("1-2-3(ft)")
        
        
        # 2D inches
        assert pattern.search("10-12 inch")
        assert pattern.search("10/12 inch")
        assert pattern.search("10X12 inch")
        assert pattern.search("10x12 inch")
        assert pattern.search("10~12 inch")
        assert pattern.search("100-120 inch")
        assert pattern.search("1.10-1.12 inch")
        assert pattern.search("10-12 inches")
        assert pattern.search("10-12-inch")
        assert pattern.search("10 - 12 inch")
        assert pattern.search("10 - 12 (inch)")
        assert pattern.search("10 - 12 (inches)")

        assert pattern.search("10-12in")
        assert pattern.search("10/12in")
        assert pattern.search("10X12in")
        assert pattern.search("10x12in")
        assert pattern.search("10~12in")
        assert pattern.search("100-120in")
        assert pattern.search("1.10-1.12in")
        assert pattern.search("10-12 in")
        assert pattern.search("10-12in.")
        assert pattern.search('10-12"')
        assert pattern.search("10 - 12in")
        assert pattern.search("10-12(in)")

        assert pattern.search("1/2-1/4inch")
        assert pattern.search("1/2X1/4inch")
        assert pattern.search("1/2x1/4inch")
        assert pattern.search("1/2~1/4inch")
        assert pattern.search("1/2 - 1/4inch")
        assert pattern.search("1 1/2 - 1 1/4inch")
        assert pattern.search("1-1/2 - 1-1/4inch")
        assert pattern.search("11 1/2 x 11 1/4inch")
        assert pattern.search("1/2-1/4(inch)")
        assert pattern.search("1/2-1/4(inches)")

        assert pattern.search('1/2-1/4in')
        assert pattern.search('1/2-1/4in.')
        assert pattern.search('1/2-1/4"')
        assert pattern.search('1/2x1/4in')
        assert pattern.search('1/2X1/4in')
        assert pattern.search('1/2~1/4in')
        assert pattern.search('1/2 - 1/4in')
        assert pattern.search('2 1/2 - 2 1/4in')
        assert pattern.search('2-1/2 - 2-1/4in')
        assert pattern.search('2 1/2 x 3 1/4in')
        assert pattern.search('1/2-1/4(in)')
        
        
        # 2D feet
        assert pattern.search("1-2feet")
        assert pattern.search("10-12feet")
        assert pattern.search("10.12-12.12feet")
        assert pattern.search("1-1.5foot")
        assert pattern.search("1x2 feet")
        assert pattern.search("1-2(feet)")

        assert pattern.search("1-2ft")
        assert pattern.search("10-12ft")
        assert pattern.search("10.12-12.12ft")
        assert pattern.search("1-1.5'")
        assert pattern.search("1x2 ft")
        assert pattern.search("1-2(ft)")

        assert pattern.search("2/3-3/4feet")
        assert pattern.search("20/12-13/12feet")
        assert pattern.search("2 20/12 - 2 13/12feet")
        assert pattern.search("20/12 - 13/12feet")
        assert pattern.search("20/12x13/12feet")
        assert pattern.search("20/12X13/12feet")
        assert pattern.search("20/12~13/12feet")
        assert pattern.search("20/12~13/12FEET")
        assert pattern.search("20/12~13/12foot")
        assert pattern.search("20/12~13/12 feet")
        assert pattern.search("20/12~13/12(feet)")

        assert pattern.search("2/3-3/4ft")
        assert pattern.search("20/12-13/12ft")
        assert pattern.search("2 20/12 - 2 13/12ft")
        assert pattern.search("20/12 - 13/12ft")
        assert pattern.search("20/12x13/12ft")
        assert pattern.search("20/12X13/12ft")
        assert pattern.search("20/12~13/12ft")
        assert pattern.search("20/12~13/12FT")
        assert pattern.search("20/12~13/12 ft")
        assert pattern.search("20/12~13/12(ft)")

        assert pattern.search("1-2feet")
        assert pattern.search("10-12feet")
        assert pattern.search("1-1foot")
        assert pattern.search("1x2 feet")
        assert pattern.search("1-2(feet)")

        assert pattern.search("1-2ft")
        assert pattern.search("10-12ft")
        assert pattern.search("2ft.-3ft.")
        assert pattern.search("2ft - 3ft")
        assert pattern.search("2ftx3ft")
        assert pattern.search("2ftX3ft")
        assert pattern.search("2ft~3ft")
        assert pattern.search("2FT-3FT")
        assert pattern.search("1-1ft.")
        assert pattern.search("1x2 ft")
        assert pattern.search("1-2(ft)")
        
        
        # 1D inches
        assert pattern.search("1.12 inch")
        assert pattern.search("1.12inch")
        assert pattern.search("1.12-inch")
        assert pattern.search("12.2 inches")
        assert pattern.search("12.2-inch")
        assert pattern.search("12.2inch")
        assert pattern.search("12.2(inch)")
        assert pattern.search("12.2 (inches)")

        assert not pattern.search("1.0 in configuration")
        assert pattern.search("1.10in")
        assert pattern.search("1.10 in")
        assert pattern.search("1.10-in")
        assert pattern.search("1.10in.")
        assert pattern.search('10.5"')
        assert pattern.search("10.4(in)")

        assert pattern.search("1 1/2inch")
        assert pattern.search("1-1/2 inch")
        assert pattern.search("11 1/2-inch")
        assert pattern.search("1 1/2inches")
        assert pattern.search("1-1/2 inches")
        assert pattern.search("11 1/2-inches")
        assert pattern.search("4 1/2(inch)")
        assert pattern.search("2 1/2(inches)")

        assert not pattern.search("1 1/2 in example")
        assert pattern.search("1 1/2in")
        assert pattern.search("1-1/2 in")
        assert pattern.search("11 1/2-in")
        assert pattern.search("1 1/2in.")
        assert pattern.search("1-1/2 in.")
        assert pattern.search("11 1/2-in.")
        assert pattern.search("4 1/2(in)")
    
        assert pattern.search("1/2inch")
        assert pattern.search("1/2 inch")
        assert pattern.search("1/2-inch")
        assert pattern.search("1/2inches")
        assert pattern.search("1/2 inches")
        assert pattern.search("1/2-inches")
        assert pattern.search("1/2(inch)")
        assert pattern.search("1/2(inches)")

        assert not pattern.search("1/2 in profile")
        assert pattern.search("1/2in")
        assert pattern.search("1/2 in")
        assert pattern.search("1/2-in")
        assert pattern.search("1/2in.")
        assert pattern.search("1/2 in.")
        assert pattern.search("1/2-in.")
        assert pattern.search("1/2(in)")

        assert pattern.search("1inch")
        assert pattern.search("1 inch")
        assert pattern.search("1-inch")
        assert pattern.search("1inches")
        assert pattern.search("1 inches")
        assert pattern.search("1-inches")
        assert pattern.search("1(inch)")
        assert pattern.search("1(inches)")

        assert not pattern.search("1 in a million")
        assert pattern.search("1in")
        assert pattern.search("1 in")
        assert pattern.search("1-in")
        assert pattern.search("1in.")
        assert pattern.search("1 in.")
        assert pattern.search("1-in.")
        assert pattern.search("1(in)")

        # 1D feet
        assert pattern.search("2.1feet")
        assert pattern.search("12.12feet")
        assert pattern.search("3.1feet")
        assert pattern.search("3.3FEET")
        assert pattern.search("1.9-foot")
        assert pattern.search("1.4-2.2(feet)")

        assert pattern.search("2.1ft")
        assert pattern.search("12.12ft")
        assert pattern.search("3.1ft.")
        assert pattern.search("3.3FT")
        assert pattern.search("2.2 ft")
        assert pattern.search("2.2-ft")
        assert pattern.search("2.2(ft)")

        assert pattern.search("1 2/3feet")
        assert pattern.search("1-1/2feet")
        assert pattern.search("1  2/3feet")
        assert pattern.search("3 1/3FEET")
        assert pattern.search("1 4/9-foot")
        assert pattern.search("1 1/2(feet)")

        assert pattern.search("1 2/3ft")
        assert pattern.search("1-1/2ft")
        assert pattern.search("1  2/3ft")
        assert pattern.search("3 1/3FT")
        assert pattern.search("1 4/9ft.")
        assert pattern.search("1 1/2(ft)")

        assert pattern.search("2/3feet")
        assert pattern.search("2/3 feet")
        assert pattern.search("2/3-feet")
        assert pattern.search("1/3FEET")
        assert pattern.search("4/9foot")
        assert pattern.search("1/2(feet)")

        assert pattern.search("2/3ft")
        assert pattern.search("2/3 ft")
        assert pattern.search("2/3-ft")
        assert pattern.search("1/3FT")
        assert pattern.search("4/9ft.")
        assert pattern.search("1/2(ft)")

        assert pattern.search("3feet")
        assert pattern.search("3 feet")
        assert pattern.search("3-feet")
        assert pattern.search("3FEET")
        assert pattern.search("1foot")
        assert pattern.search("2(feet)")

        assert pattern.search("3ft")
        assert pattern.search("3 ft")
        assert pattern.search("3-ft")
        assert pattern.search("3FT")
        assert pattern.search("1ft.")
        assert pattern.search("2(ft)")


class TestCurrentPatterns():
    def test_amps_pattern(self):
        pattern = dim_tok.patterns['current'][0]
        assert pattern.search("2-2a")
        assert pattern.search("10-12a")
        assert pattern.search("10.5-12.5a")
        assert pattern.search("10/12a")
        assert pattern.search("10~12a")
        assert pattern.search("10~12 a")
        assert pattern.search("10~12-a")
        assert pattern.search("10~12-amps")
        assert pattern.search("10~12-a.")

        assert pattern.search("12.5a")
        assert pattern.search("12.55a")
        assert pattern.search("12.5 a")
        assert pattern.search("12.5-a")
        assert pattern.search("12.5-amps")
        assert pattern.search("12.5-a.")

        assert pattern.search("12a")
        assert pattern.search("12 a")
        assert pattern.search("12-a")
        assert pattern.search("12-amps")
        assert pattern.search("12-a.")

    def test_voltages_pattern(self):
        pattern = dim_tok.patterns['current'][1]
        assert pattern.search("2-2volt")
        assert pattern.search("12.10-12.10volt")
        assert pattern.search("2~2volt")
        assert pattern.search("2/2volt")
        assert pattern.search("2-2 volt")
        assert pattern.search("2~2-volt")
        assert pattern.search("2-2 voltage")
        assert pattern.search("2-2 volts")
        assert pattern.search("2-2 volt.")

        assert pattern.search("1.1volt")
        assert pattern.search("12.10volt")
        assert pattern.search("10.12 volt")
        assert pattern.search("10.12-volt")
        assert pattern.search("10.12 voltage")
        assert pattern.search("10.12 volts")
        assert pattern.search("10.12 volt.")

        assert pattern.search("1volt")
        assert pattern.search("10volt")
        assert pattern.search("12 volt")
        assert pattern.search("12-volt")
        assert pattern.search("12 voltage")
        assert pattern.search("12 volts")
        assert pattern.search("12 volt.")

    def test_wattages_pattern(self):
        pattern = dim_tok.patterns['current'][2]
        assert pattern.search("2-2w")
        assert pattern.search("10-12w")
        assert pattern.search("10.12-10.12w")
        assert pattern.search("10/12w")
        assert pattern.search("10~12w")
        assert pattern.search("10-12 w")
        assert pattern.search("10~12-w")
        assert pattern.search("10-12watt")
        assert pattern.search("10-12wattage")
        assert pattern.search("10-12watts")
        # this one doesn't look right
        assert pattern.search("10-12wattages")

        assert pattern.search("2.1w")
        assert pattern.search("10.12w")
        assert pattern.search("10.12 w")
        assert pattern.search("10.12-w")
        assert pattern.search("10.12watt")
        assert pattern.search("10.12wattage")
        assert pattern.search("10.12watts")
        # this one doesn't look right
        assert pattern.search("10.12wattages")

        assert pattern.search("2w")
        assert pattern.search("12w")
        assert pattern.search("12 w")
        assert pattern.search("12-w")
        assert pattern.search("12watt")
        assert pattern.search("12wattage")
        assert pattern.search("12watts")
        # this one doesn't look right
        assert pattern.search("12wattages")

    def test_amps_pattern(self):
        pattern = dim_tok.patterns['current'][3]
        assert pattern.search("2-2mAh")
        assert pattern.search("10-12mAh")
        assert pattern.search("10.5-12.5mAh")
        assert pattern.search("10/12mAh")
        assert pattern.search("10~12mAh")
        assert pattern.search("10~12 mAh")
        assert pattern.search("10~12-mAh")
        assert pattern.search("10~12-mAh.")
        assert pattern.search("10~12-MAH")
    
        assert pattern.search("2,2-2,2mAh")
        assert pattern.search("2,200-2,200mAh")
        assert pattern.search("20,200,200-20,200,200mAh")
        assert pattern.search("2,200/2,200mAh")
        assert pattern.search("2,200~2,200mAh")
        assert pattern.search("2,200-2,200 mAh")
        assert pattern.search("2,200~2,200-mAh")
        assert pattern.search("2,200-2,200mAh.")
        assert pattern.search("2,200-2,200MAH")

        assert pattern.search("2.5mAh")
        assert pattern.search("12.5mAh")
        assert pattern.search("12.52mAh")
        assert pattern.search("12.5 mAh")
        assert pattern.search("12.5-mAh")
        assert pattern.search("12.5-mAh.")
        assert pattern.search("12.5-MAH")

        assert pattern.search("2,2mAh")
        assert pattern.search("2,200mAh")
        assert pattern.search("20,200,200mAh")
        assert pattern.search("2,200 mAh")
        assert pattern.search("2,200-mAh")
        assert pattern.search("2,200mAh.")
        assert pattern.search("2,200MAH")

class TestGaugePatterns():
    def test_gauge_pattern(self):
        # Could use optional spaces.
        pattern = dim_tok.patterns['gauge'][0]
        assert pattern.search("2-2awg")
        assert pattern.search("10-12awg")
        assert pattern.search("10.12-10.12awg")
        assert pattern.search("10/12awg")
        assert pattern.search("10~12awg")
        assert pattern.search("10-12 awg")
        assert pattern.search("10~12-awg")

        assert pattern.search("2.2awg")
        assert pattern.search("1.12awg")
        assert pattern.search("10.12awg")
        assert pattern.search("10.12 awg")
        assert pattern.search("10.12-awg")

        assert pattern.search("2awg")
        assert pattern.search("12awg")
        assert pattern.search("12 awg")
        assert pattern.search("12-awg")

        assert pattern.search("2-2gauge")
        assert pattern.search("10-12gauge")
        assert pattern.search("10.12-10.12gauge")
        assert pattern.search("10/12gauge")
        assert pattern.search("10~12gauge")
        assert pattern.search("10-12 gauge")
        assert pattern.search("10~12-gauge")

        assert pattern.search("2.2gauge")
        assert pattern.search("1.12gauge")
        assert pattern.search("10.12gauge")
        assert pattern.search("10.12 gauge")
        assert pattern.search("10.12-gauge")

        assert pattern.search("2gauge")
        assert pattern.search("12gauge")
        assert pattern.search("12 gauge")
        assert pattern.search("12-gauge")
        assert pattern.search("2GAUGE")


    
class TestHertzPatterns():
    def test_hertz_patterns(self):
        pattern = dim_tok.patterns['hertz'][0]
        assert pattern.search("8hertz")
        assert pattern.search("80hertz")
        assert pattern.search("8 hertz")
        assert pattern.search("8-hertz")
        assert pattern.search("8HERTZ")

        assert pattern.search("8hz")
        assert pattern.search("80hz")
        assert pattern.search("8 hz")
        assert pattern.search("8-hz")
        assert pattern.search("8thz")
        assert pattern.search("8ghz")
        assert pattern.search("8mhz")
        assert pattern.search("8khz")
        assert pattern.search("8HZ")


class TestLumensPatterns():
    def test_lumens_patterns(self):
        # Optional spaces could be included
        pattern = dim_tok.patterns['lumens'][0]
        assert pattern.search("1-2candelas")
        assert pattern.search("1.1-2.2candelas")
        assert pattern.search("1,000.20-2,000.20candelas")
        assert pattern.search("1~2candela")
        assert pattern.search("1-2 candela")
        assert pattern.search("1~2-candela")

        assert pattern.search("1-2cd")
        assert pattern.search("1.1-2.2cds")
        assert pattern.search("1,000.20-2,000.20cds")
        assert pattern.search("1~2cd")
        assert pattern.search("1-2 cds")
        assert pattern.search("1~2-cds")

        assert pattern.search(".2nanocandela")
        assert pattern.search("2microcandela")
        assert pattern.search("2.2kilocandela")
        assert pattern.search("2,000.20megacandelas")
        assert pattern.search("1-2 candelas")
        assert pattern.search("1~2-candela")

        assert pattern.search("2ncd")
        assert pattern.search("2.2ccd")
        assert pattern.search("2,000.20kcd")
        assert pattern.search("1-2 mcd")
        assert pattern.search("1~2-gcd")
    
    def test_lumens_patterns(self):
        # Optional spaces could be included
        pattern = dim_tok.patterns['lumens'][1]
        assert pattern.search("1-2lumens")
        assert pattern.search("1.1-2.2lumens")
        assert pattern.search("1,000.20-2,000.20lumens")
        assert pattern.search("1~2lumens")
        assert pattern.search("1-2 lumens")
        assert pattern.search("1~2-lumens")

        assert pattern.search("1-2lms")
        assert pattern.search("1.1-2.2lms")
        assert pattern.search("1,000.20-2,000.20lms")
        assert pattern.search("1~2lms")
        assert pattern.search("1-2 lms")
        assert pattern.search("1~2-lms")

        assert pattern.search("2lumens")
        assert pattern.search("2.2lumens")
        assert pattern.search("2,000.20lumens")
        assert pattern.search("1-2 lumens")
        assert pattern.search("1~2-lumens")

        assert pattern.search("2lms")
        assert pattern.search("2.2lms")
        assert pattern.search("2,000.20lms")
        assert pattern.search("1-2 lms")
        assert pattern.search("1~2-lms")


class TestNumberOfOutletsPatterns():
    # Trailing "s" could be made optional in these patterns
    # And they could all be merged into a single pattern
    def test_number_of_outlets_patterns(self):
        pattern = dim_tok.patterns['number of outlets'][0]
        assert pattern.search("2outlets")
        assert pattern.search("20outlets")
        assert pattern.search("2 outlets")
        assert pattern.search("2-outlets")
        assert pattern.search("2OUTLETS")
        assert pattern.search("2gangs")
        assert pattern.search("20gangs")
        assert pattern.search("2 gangs")
        assert pattern.search("2-gangs")
        assert pattern.search("2GANGS")
        assert pattern.search("2ports")
        assert pattern.search("20ports")
        assert pattern.search("2 ports")
        assert pattern.search("2-ports")
        assert pattern.search("2PORTS")
        assert pattern.search("2bays")
        assert pattern.search("20bays")
        assert pattern.search("2 bays")
        assert pattern.search("2-bays")
        assert pattern.search("2BAYS")


class TestPackPatterns():
    def test_pack_pattern(self):
        pattern = dim_tok.patterns['pack'][0]
        assert pattern.search("2pcs/pack")
        assert pattern.search("20pcs/pack")
        assert pattern.search("2 pcs/pack")
        assert pattern.search("2-pcs/pack")
        assert pattern.search("2pcs./pack")
        assert pattern.search("2  pcs/pack")
        assert pattern.search("(2pcs/pack)")

        assert pattern.search("2pc")
        assert pattern.search("20pc")
        assert pattern.search("2 pc")
        assert pattern.search("2-pc")
        assert pattern.search("2 pc")
        assert pattern.search("(2pc)")
        assert pattern.search("2pcs")
        assert pattern.search("2pc.")
        assert pattern.search("2pcs.")
        assert pattern.search("2pack")
        assert pattern.search("2pk")
        assert pattern.search("2piece")
        assert pattern.search("2can")

        assert pattern.search("twopack")
        assert pattern.search("two pack")
        assert pattern.search("two  pack")
        assert pattern.search("two-pack")
        assert pattern.search("twopacks")
        assert pattern.search("(twopack)")
        assert pattern.search("threepack")
        assert pattern.search("fourpack")
        assert pattern.search("sixpack")
        assert pattern.search("eightpack")
        assert pattern.search("twelvepack")

        assert pattern.search("pack of 2")
        assert pattern.search("pack of 20")
        assert pattern.search("packs of 2")
        assert pattern.search("set of 2")
        assert pattern.search("sets of 2")
        assert pattern.search("pack-of-2")
        assert pattern.search("PACK OF 2")
        
        assert pattern.search("pack of two")
        assert pattern.search("packs of two")
        assert pattern.search("set of two")
        assert pattern.search("sets of two")
        assert pattern.search("pack-of-two")
        assert pattern.search("pack of three")
        assert pattern.search("pack of four")
        assert pattern.search("pack of six")
        assert pattern.search("pack of eight")
        assert pattern.search("pack of twelve")
        assert pattern.search("PACK OF TWO")
        
    
    def test_pack_pattern_double(self):
        pattern = dim_tok.patterns['pack'][1]
        assert pattern.search("2piece bag")
        assert pattern.search("20pieces bag")
        assert pattern.search("2 piece bag")
        assert pattern.search("2-piece bag")
        assert pattern.search("2piece/bag")
        assert pattern.search("2pieces bag")
        assert pattern.search("(2piece bag)")

        assert pattern.search("2pc bag")
        assert pattern.search("20pc bag")
        assert pattern.search("2 pc bag")
        assert pattern.search("2-pc bag")
        assert pattern.search("2pc/bag")
        assert pattern.search("2pcs bag")
        assert pattern.search("2pcs. bag")
        assert pattern.search("(2pc bag)")


class TestCountPatterns():
    def test_count_pattern(self):
        pattern = dim_tok.patterns['count'][0]
        assert pattern.search("2count")
        assert pattern.search("2cnt")
        assert pattern.search("3 pair")
        assert pattern.search("2gross")
        assert pattern.search("4-dzn")
        assert pattern.search("4  doz")
        assert pattern.search("5ea")
        assert pattern.search("3qty.")
        
    def test_count_pattern(self):
        pattern = dim_tok.patterns['count'][1]
        assert pattern.search("2capsule")
        assert pattern.search("2tab")
        assert pattern.search("3 roll")
        assert pattern.search("4-ply")
        assert pattern.search("4  sht")
        assert pattern.search("3rm")


class TestBytesPatterns():
    def test_bytes_patterns_1st(self):
        pattern = dim_tok.patterns['memory'][0]
        assert pattern.search("2b")
        assert pattern.search("200b")
        assert pattern.search("2.2b")
        assert pattern.search("2,000.05b")
        assert pattern.search("2-b")
        assert pattern.search("2 b")
        assert pattern.search("2-2b")
        assert pattern.search("2,000-4,000b")
        assert pattern.search("2.2-4.4b")
        assert pattern.search("1,024 2,048b")
        assert pattern.search("2byte")
        assert pattern.search("2bytes")
        assert pattern.search("2kb")
        assert pattern.search("2mb")
        assert pattern.search("2gb")
        assert pattern.search("2tb")
        assert pattern.search("2tbs")
        assert pattern.search("2TBS")



class TestWeightPatterns():
    def test_weight_patterns_metric(self):
        pattern = dim_tok.patterns['weight'][0]
        assert pattern.search("2g")
        assert pattern.search("20g")
        assert pattern.search("2.2g")
        assert pattern.search("2,000.15g")
        assert pattern.search("2-g")
        assert pattern.search("2 g")
        assert pattern.search("2-4g")
        assert pattern.search("2,000-4,000g")
        assert pattern.search("2,000.15-4,000.25g")
        assert pattern.search("4kg")
        assert pattern.search("4mg")
        assert pattern.search("4gs")
        assert pattern.search("4kgs")
        
        assert pattern.search("2gram")
        assert pattern.search("20gram")
        assert pattern.search("2.2gram")
        assert pattern.search("2,000.15gram")
        assert pattern.search("2-gram")
        assert pattern.search("2 gram")
        assert pattern.search("2-4gram")
        assert pattern.search("2,000-4,000gram")
        assert pattern.search("2,000.15-4,000.25gram")
        assert pattern.search("4kilogram")
        assert pattern.search("4milligram")
        assert pattern.search("4grams")
        assert pattern.search("4kilograms")
        assert pattern.search("4milligrams")
    
    def test_weight_patterns_ton(self):
        pattern = dim_tok.patterns['weight'][1]
        assert pattern.search("2t")
        assert pattern.search("20t")
        assert pattern.search("2.2ton")
        assert pattern.search("2,000.15ton")
        assert pattern.search("2-tonne")
        assert pattern.search("2 t.")
        assert pattern.search("2-4t.")
        
        
    def test_weight_patterns_imperial(self):
        pattern = dim_tok.patterns['weight'][2]
        assert pattern.search("2lb")
        assert pattern.search("20lb")
        assert pattern.search("2.2lb")
        assert pattern.search("2,000.15lb")
        assert pattern.search("2-lb")
        assert pattern.search("2 lb")
        assert pattern.search("2-4lb")
        assert pattern.search("2,000-4,000lb")
        assert pattern.search("2,000.15-4,000.25lb")
        assert pattern.search("4oz")
        assert pattern.search("4pound")
        assert pattern.search("4ounce")
        assert pattern.search("4lbs")
        assert pattern.search("4pounds")
        assert pattern.search("4ounces")


class TestCaratPatterns():
    def test_carat_patterns(self):
        pattern = dim_tok.patterns['carat'][0]
        assert pattern.search("1 1/2carat")
        assert pattern.search("1-1/2carats")
        
        assert pattern.search("1/2carat")
        assert pattern.search("1-2 carats")
        assert pattern.search("1.5~2 c.")
        assert pattern.search("1.5~2-ct")
        assert pattern.search("1.5~2.5-ctw")
        
        assert pattern.search("1.5 cwt")
        assert pattern.search(".5-tw")
        assert pattern.search("0.5 cttw.")
        assert pattern.search("0.5 dtw.")
        assert pattern.search("0.5 dw.")
        assert pattern.search("0.5 tdw")
        
        assert pattern.search("12 karat")
        assert pattern.search("18-karat")
        assert pattern.search("24karat")
        assert pattern.search("12 karats")
        assert pattern.search("18-karats")
        assert pattern.search("24karats")
